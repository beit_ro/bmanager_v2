var socketPort = 6301;
var expressPort = 80;
var apiUrl = 'http://cue.beit.ro:443/api';
var appUrl = 'http://cue.beit.ro/';


var redis = {
    host: 'com.beit.ro',
    port: 7777
};

var module;
if (module == undefined) {
} else {
    module.exports = {
        expressPort: expressPort,
        socketPort: socketPort,
        redis: redis,
        apiUrl: apiUrl,
        appUrl: appUrl
    };
}
