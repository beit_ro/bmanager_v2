import { NgModule } from '@angular/core';

import {PrivateLayoutModule} from './private/private-layout.module';
import {PublicLayoutModule} from './public/public-layout.module';
import {ContentModule} from './components/content/content.module';
import {FooterModule} from './components/footer/footer.module';
import {NavbarModule} from './components/navbar/navbar.module';
import {QuickPanelModule} from './components/quick-panel/quick-panel.module';



@NgModule({
    imports: [

        // App modules
        PublicLayoutModule,
        PrivateLayoutModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,




    ],
    exports: [
        PublicLayoutModule,
        PrivateLayoutModule,

    ],
    providers: [
]
})
export class LayoutModule
{
}
