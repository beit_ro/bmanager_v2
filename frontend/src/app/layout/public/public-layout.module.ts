import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {FuseNavigationModule, FuseSidebarModule} from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import {PublicLayoutComponent} from "./public-layout.component";
import {ContentModule} from '../components/content/content.module';
import {FooterModule} from '../components/footer/footer.module';
import {NavbarModule} from "../components/navbar/navbar.module";
import {ToolbarModule} from "../components/toolbar/toolbar.module";
import {QuickPanelModule} from "../components/quick-panel/quick-panel.module";
import {PublicModule} from "../../pages/public/public.module";



@NgModule({
    declarations: [
        PublicLayoutComponent
    ],
    imports: [
        RouterModule,
        FuseSharedModule,
        FuseSidebarModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        ToolbarModule,
        QuickPanelModule,
        PublicModule,
    ],
    exports     : [
        PublicLayoutComponent
    ]
})
export class PublicLayoutModule
{
}
