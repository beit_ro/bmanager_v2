import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSidebarModule, FuseNavigationModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';


import {PrivateLayoutComponent} from './private-layout.component';
import {ContentModule} from '../components/content/content.module';
import {FooterModule} from '../components/footer/footer.module';
import {NavbarModule} from '../components/navbar/navbar.module';
import {ToolbarModule} from '../components/toolbar/toolbar.module';
import {QuickPanelModule} from '../components/quick-panel/quick-panel.module';


@NgModule({
    declarations: [
        PrivateLayoutComponent
    ],
    imports: [
        RouterModule,
        FuseSharedModule,
        FuseSidebarModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        ToolbarModule,
        FuseNavigationModule,
        QuickPanelModule,
    ],
    exports     : [
        PrivateLayoutComponent
    ],

    providers     : [
    ]
})
export class PrivateLayoutModule
{
}
