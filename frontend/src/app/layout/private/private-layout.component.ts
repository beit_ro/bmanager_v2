import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import {SharedNavigationService} from "../../shared/_services/sharednavigation.service";
import {FuseNavigationService} from "../../../@fuse/components/navigation/navigation.service";
import {FuseNavigation} from "../../../@fuse/types";

@Component({
    selector     : 'private-layout',
    templateUrl  : './private-layout.component.html',
    styleUrls    : ['./private-layout.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PrivateLayoutComponent implements OnInit, OnDestroy
{
    fuseConfig: any;
   // navigation: any;
    chatUsers: boolean;
    user: any;
    navigation: FuseNavigation[];


    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param _sharedNavigationService
     * @param _fuseNavigationService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _sharedNavigationService: SharedNavigationService,
        private _fuseNavigationService: FuseNavigationService,


    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._sharedNavigationService.onItemUpdate
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(navigation => {

                //console.log('gett here to nav');
                this._fuseNavigationService.unregister('main');
                this.getNavigation();
            });

        this.chatUsers = false;
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
    }


    getNavigation() {

        const queryParams = {
            relationships: 'parent',
            orderby: 'position',
            order: 'asc'

        };

        return this._sharedNavigationService
            .list(queryParams)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (data) => {
                    this.navigation = data;
                    // Register the navigation to the service
                    this._fuseNavigationService.register('main', this.navigation);
                    console.log('nav',this.navigation)
                    // Set the main navigation as our current navigation
                    this._fuseNavigationService.setCurrentNavigation('main');

                }
            );
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
