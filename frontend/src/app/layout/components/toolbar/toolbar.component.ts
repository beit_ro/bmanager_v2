import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../shared/_services/authentication.service';
import {TokenStorage} from '../../../shared/_services/token-storage.service';
import {Socket} from 'ngx-socket-io';
import {DefaultComponent} from '../../../shared/_components/default.component';
import {environment} from '../../../../environments/environment';
import {SharedUserService} from "../../../shared/_services/shareduser.service";
import {DomSanitizer} from "@angular/platform-browser";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";
import {MatDialog} from "@angular/material/dialog";
import {Overlay} from "@angular/cdk/overlay";
import {DataStorageService} from "../../../shared/_services/data-storage.service";

@Component({
    selector     : 'toolbar',
    templateUrl  : './toolbar.component.html',
    styleUrls    : ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent extends DefaultComponent implements OnInit, OnDestroy
{
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];
    user: any;
    location: any;
    endpoint: string;
    isLoggedIn = false;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private authService: AuthenticationService,
        private tokenStorage: TokenStorage,
        private socketService: Socket,
        private userService: SharedUserService,
        private sanitizer: DomSanitizer,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        private dataStorageService: DataStorageService,


        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        this.navigation = navigation;


    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

        this.location = this.dataStorageService.getData('address');

        //console.log('location', this.location.formatted_address)


        // Set the selected language from default languages
        this.checkIfLoggedIn();
    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void
    {
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void
    {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }

    checkIfLoggedIn() {
        //console.log('check login step2');
        return this.authService
            .isAuthorized()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.isLoggedIn = data;
                    if (this.isLoggedIn) {
                        this.getUser();
                        this.tokenStorage.sendTokenToSocketIo();

                    }
                }
            );
    }

    getUser() {
        return this.userService
            .getMe()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.user = data;
                    this.user.avatar = this.sanitizer.bypassSecurityTrustResourceUrl(this.user.avatar);
                    this.isLoggedIn = true;
                }
            );
    }


    onLogOut() {
        return this.authService
            .logout()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe();
    }





}
