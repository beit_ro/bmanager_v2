import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-header-image',
  templateUrl: './header-image.component.html',
  styleUrls: ['./header-image.component.scss']
})
export class HeaderImageComponent implements OnInit {
  @Input('backgroundImage') backgroundImage;
  @Input('bgImageAnimate') bgImageAnimate;
  @Input('contentOffsetToTop') contentOffsetToTop;
  @Input('contentMinHeight') contentMinHeight;
  @Input('title') title;
  @Input('desc') desc;
  @Input('isHomePage') isHomePage = false;
  public bgImage;
  public MinHeight;
  constructor( private sanitizer:DomSanitizer) {
  }

  ngOnInit() {

    if(this.contentMinHeight)
      this.MinHeight = this.contentMinHeight + 'px;';
    if(this.backgroundImage) 
      this.bgImage = this.sanitizer.bypassSecurityTrustStyle('url('+this.backgroundImage +')'); 
  }

  ngOnDestroy(){    
    this.bgImage = false;
    this.contentOffsetToTop = false;
  }

}
