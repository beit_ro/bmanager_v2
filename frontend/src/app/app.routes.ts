import {RouterModule, Routes} from '@angular/router';
import {ProtectedGuard, PublicGuard} from 'ngx-auth';
import {publicRoutes} from './pages/public/public.routes';
import {PublicLayoutComponent} from './layout/public/public-layout.component';
import {authenticationRoutes} from './pages/authentication/authentication.routes';
import {PrivateLayoutComponent} from './layout/private/private-layout.component';
import {privateRoutes} from './pages/private/private.routes';


const approutes: Routes = [


    //Public routes
    {
        path: 'public',
        component: PublicLayoutComponent,
        canActivate: [PublicGuard],
        children: [
            // @ts-ignore
            {path: '', children: [...publicRoutes]}
        ]
    },

    // App routes

    {
        path: 'private',
        component: PrivateLayoutComponent,
        canActivate: [ProtectedGuard],
        children: [
            // @ts-ignore
            {path: '', children: [...privateRoutes]},
        ]
    },

    // Authentification routes
    {
        path: 'auth',
        component: PublicLayoutComponent,
        children: [
            // @ts-ignore
            {path: '', children: [...authenticationRoutes]}
        ]
    },

    {
        path: '**', redirectTo: '/auth/login'
    }
];

export const appRoutes = RouterModule.forRoot(approutes);
