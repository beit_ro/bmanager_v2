import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';
import {appRoutes} from './app.routes';
import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import {SocketIoConfig, SocketIoModule} from 'ngx-socket-io';
import {environment} from '../environments/environment';
import {PublicModule} from './pages/public/public.module';
import {AuthenticationModule} from './pages/authentication/authentication.module';
import {PrivateModule} from './pages/private/private.module';
import {SharedModule} from './shared/shared.module';
import {ToasterModule} from "angular2-toaster";
import {MatInputModule} from "@angular/material/input";
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { AgmCoreModule } from '@agm/core';
const socketIoConfig: SocketIoConfig = { url: environment.socket, options: {} };



@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LayoutModule,
        appRoutes,
        PublicModule,
        PrivateModule,
        SharedModule,
        TranslateModule.forRoot(),
        AuthenticationModule,
        MatInputModule,

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,


        SocketIoModule.forRoot(socketIoConfig),
        ToasterModule.forRoot(),
        MatGoogleMapsAutocompleteModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB77OC0QNHhmHrPBtWXKLZfMSN-HvKX-ZE',
            libraries: ["places"]
        })

    ],
    bootstrap   : [
        AppComponent
    ]
})
export class AppModule
{
}
