export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'MAINDASHBOARD': 'Dashboard',
            'ANALITICSDASHBOARD'        : {
                'TITLE': 'Analitics'
            },
            'PROJECTDASHBOARD'        : {
                'TITLE': 'PROJECT'
            },
            'SETTINGS': 'Settings',
            'LANGUAGES'        : {
                'TITLE': 'Languages'
            },
            'NAVIGATION'        : {
                'TITLE': 'Navigation Menu'
            },


        },

    }
};
