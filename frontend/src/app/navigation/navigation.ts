// @ts-ignore
import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        "id": 1,
        "title": "Dashboard",
        "type": "group",
        "icon": null,
        "translate": null,
        "url": null,
        "parent_id": null,
        "created_at": "2019-12-23 00:37:38",
        "updated_at": "2019-12-23 00:37:38",
        "deleted_at": null,
        "children": [
            {
                "id": 3,
                "title": "Project",
                "type": "item",
                "icon": "settings",
                "translate": null,
                "url": "/private/project",
                "parent_id": 1,
                "created_at": "2019-12-23 00:37:38",
                "updated_at": "2019-12-23 00:37:38",
                "deleted_at": null
            }
        ]
    },
    {
        "id": 2,
        "title": "Settings",
        "type": "group",
        "icon": "settings",
        "translate": null,
        "url": null,
        "parent_id": null,
        "created_at": "2019-12-23 00:37:38",
        "updated_at": "2019-12-23 00:37:38",
        "deleted_at": null,
        "children": [
            {
                "id": 4,
                "title": "Permissions",
                "type": "item",
                "icon": "lock",
                "translate": null,
                "url": "/private/settings/permissions",
                "parent_id": 2,
                "created_at": "2019-12-23 00:37:38",
                "updated_at": "2019-12-23 00:37:38",
                "deleted_at": null
            },
            {
                "id": 5,
                "title": "Groups",
                "type": "item",
                "icon": "menu",
                "translate": null,
                "url": "/private/settings/groups",
                "parent_id": 2,
                "created_at": "2019-12-23 00:37:38",
                "updated_at": "2019-12-23 00:37:38",
                "deleted_at": null
            },
            {
                "id": 6,
                "title": "Users",
                "type": "item",
                "icon": "group",
                "translate": null,
                "url": "/private/settings/users",
                "parent_id": 2,
                "created_at": "2019-12-23 00:37:38",
                "updated_at": "2019-12-23 00:37:38",
                "deleted_at": null
            }
        ]
    }
];
