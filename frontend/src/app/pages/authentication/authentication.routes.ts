import {Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {RegisterComponent} from "./register/register.component";
import {ResetPasswordComponent} from './reset-password/reset-password.component';


export const authenticationRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'login',
                component: LoginComponent
            },
            {
                path: 'forgot-password',
                component: ForgotPasswordComponent
            },
            {
                path: 'register',
                component: RegisterComponent
            },
            {
                path: 'reset/:token',
                component: ResetPasswordComponent
            }

        ]
    }
];
