import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {AuthenticationService} from '../../../shared/_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../shared/_services/shared-routes.service';
import {DefaultComponent} from '../../../shared/_components/default.component';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    animations: fuseAnimations
})
export class ForgotPasswordComponent extends DefaultComponent implements OnInit, OnDestroy {
    forgotPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param authenticationService
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService) {
        super(router, activatedRoute, sharedRoutesService);
    }


    /**
     * On init
     */
    ngOnInit(): void {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }



    reset(form: HTMLFormElement) {
        const queryParams = form.value;
        this.authenticationService
            .forgotPassword(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.router.navigate(['/auth/login']);
                }
            );
    }


}
