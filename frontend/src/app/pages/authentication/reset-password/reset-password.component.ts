import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {AuthenticationService} from '../../../shared/_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../shared/_services/shared-routes.service';
import {DefaultComponent} from '../../../shared/_components/default.component';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    animations: fuseAnimations
})
export class ResetPasswordComponent extends DefaultComponent implements OnInit, OnDestroy {
    resetPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param authenticationService
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService) {
        super(router, activatedRoute, sharedRoutesService);
    }

    token: any;



    /**
     * On init
     */
    ngOnInit(): void {

        this.token = this.activatedRoute.snapshot.paramMap.get('token');
        this.resetPasswordForm = this._formBuilder.group({
            new_password: ['', Validators.required],
            new_password_confirmation: ['', [Validators.required]],
        });

    }



    reset(form: HTMLFormElement) {
        const queryParams = form.value;
        queryParams.token = this.token
        return this.authenticationService
            .changePassword(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((data) => {

                this.router.navigate(['/auth/login']);
            },(error: any ) => {

            });
    }


}
