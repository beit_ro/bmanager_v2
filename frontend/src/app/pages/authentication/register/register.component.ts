import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '../../../../@fuse/animations';
import {environment} from '../../../../environments/environment';
import {FuseConfigService} from '../../../../@fuse/services/config.service';
import {AuthenticationService} from "../../../shared/_services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from '../../../shared/_components/default.component';
import {SharedRoutesService} from '../../../shared/_services/shared-routes.service';

import {UnsecuredOrganizationsService} from './unsecured-organizations.service';


@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: fuseAnimations
})
export class RegisterComponent extends DefaultComponent implements OnInit, OnDestroy {
    registerForm: FormGroup;
    user: any = {};
    organizations: any;
    project: any = environment.project;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private unsecuredorganizationsService: UnsecuredOrganizationsService,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.getOrganizations();
        this.registerForm = this._formBuilder.group({
            name: ['', Validators.required],
            organization_id: [1, Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            password_confirmation: ['', [Validators.required]],
        });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
    }

    register(form: HTMLFormElement) {

        const queryParams = form.value;
        //console.log('pp',queryParams);
        //return queryParams;
        this.authenticationService
            .register(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.router.navigate(['/auth/login']);
                }
            );
    }

    getOrganizations() {
        const queryParams = {
        };

        return this.unsecuredorganizationsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.organizations = data;
                    console.log('aa',data)

                }
            );
    }


}
