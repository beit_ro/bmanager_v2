import {NgModule} from '@angular/core';


import {RouterModule} from "@angular/router";
import {
    MatButtonModule, MatCheckbox, MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule, MatInputModule,
} from "@angular/material";
import {
    AuthModule,
    AUTH_SERVICE,
    PUBLIC_FALLBACK_PAGE_URI,
    PROTECTED_FALLBACK_PAGE_URI
} from 'ngx-auth';

import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from './register/register.component';
import {AuthentificationComponent} from './authentification.component';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {DataStorageService} from '../../shared/_services/data-storage.service';
import {TokenStorage} from '../../shared/_services/token-storage.service';
import {AuthenticationService} from '../../shared/_services/authentication.service';
import {ToasterService} from 'angular2-toaster';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatDialogModule} from "@angular/material/dialog";
import {MatToolbarModule} from "@angular/material/toolbar";
import {UnsecuredOrganizationsService} from './register/unsecured-organizations.service';
import {NgSelectModule} from '@ng-select/ng-select';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {ResetPasswordComponent} from './reset-password/reset-password.component';


@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        AuthentificationComponent,
        ResetPasswordComponent

    ],
    imports: [

        AuthModule,
        RouterModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatExpansionModule,
        MatDialogModule,
        MatToolbarModule,
        MatCheckboxModule,
        FuseSharedModule,
        NgSelectModule,
        MatOptionModule,
        MatSelectModule
    ],
    exports: [
        RegisterComponent,
        AuthentificationComponent,
        ResetPasswordComponent

    ],
    entryComponents: [
    ],
    providers: [
        DataStorageService,
        TokenStorage,
        ToasterService,
        AuthenticationService,
        UnsecuredOrganizationsService,
        {provide: PROTECTED_FALLBACK_PAGE_URI, useValue: '/private/dashboard'}, // redirect if already logged in
        {provide: PUBLIC_FALLBACK_PAGE_URI, useValue: '/auth/login'}, // redirect if not logged in
        {provide: AUTH_SERVICE, useClass: AuthenticationService}
    ]
})
export class AuthenticationModule {

}
