import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '../../../../@fuse/animations';
import {DefaultComponent} from '../../../shared/_components/default.component';
import {ToasterService} from 'angular2-toaster';
import {AuthenticationService} from '../../../shared/_services/authentication.service';
import {environment} from '../../../../environments/environment';
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class LoginComponent extends DefaultComponent implements OnInit {
    loginForm: FormGroup;
    user: any = {};
    project: any = environment.project;

    constructor(
        private _formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private toasterService: ToasterService,

        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    ngOnInit() {
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });



    }



    login(form: HTMLFormElement) {
        const queryParams = form.value;
        this.authenticationService
            .login(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                data => {
                    //main route after login
                    this.router.navigate(['/private/dashboard']);
                    this.toasterService.pop('success', 'Logged in successfully!');
                }
            );
    }


}
