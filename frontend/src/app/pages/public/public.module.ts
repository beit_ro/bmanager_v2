import {NgModule} from '@angular/core';
import {ComingSoonComponent} from "./coming-soon/coming-soon.component";
import {Error404Component} from "./errors/404/error-404.component";
import {Error500Component} from "./errors/500/error-500.component";
import {FaqComponent} from "./faq/faq.component";
import {
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule, MatToolbarModule, MatListModule, MatSelectModule
} from "@angular/material";
import {RouterModule} from "@angular/router";
import { KnowledgeBaseService } from './knowledge-base/knowledge-base.service';
import {ForgotPasswordComponent} from '../authentication/forgot-password/forgot-password.component';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {FuseCountdownModule, FuseWidgetModule} from '../../../@fuse/components';
import {KnowledgeBaseArticleComponent} from "./knowledge-base/dialogs/article/article.component";
import {MaintenanceComponent} from "./maintenance/maintenance.component";
import {KnowledgeBaseComponent} from "./knowledge-base/knowledge-base.component";
import {MatCardModule} from "@angular/material/card";
import {HomeComponent} from "./home/home.component";
import {AuthenticationModule} from "../authentication/authentication.module";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {ColorPickerModule} from "ngx-color-picker";
import {MatChipsModule} from "@angular/material/chips";
import {MatRippleModule} from "@angular/material/core";
import {MatSortModule} from "@angular/material/sort";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatStepperModule} from "@angular/material/stepper";
import {MatDividerModule} from "@angular/material/divider";
import {MatTableModule} from "@angular/material/table";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatRadioModule} from "@angular/material/radio";
import {MatMenuModule} from "@angular/material/menu";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatTabsModule} from "@angular/material/tabs";
import {MatGoogleMapsAutocompleteModule} from "@angular-material-extensions/google-maps-autocomplete";
import {DataStorageService} from "../../shared/_services/data-storage.service";
import { FuseSidebarModule } from '@fuse/components/sidebar/sidebar.module';
import {HeaderImageComponent} from "../../layout/components/header-image/header-image.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatGridListModule} from "@angular/material/grid-list";
import {FaqService} from "./faq/faq.service";
import {TermsComponent} from './terms/terms.component';


@NgModule({
    imports: [

        // material components
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatExpansionModule,
        MatListModule,
        MatToolbarModule,
        BrowserAnimationsModule,

        // fuse components
        FuseSharedModule,
        FuseCountdownModule,
        FuseSidebarModule,

        // other components
        RouterModule,

        // other Modules
        MatPaginatorModule,
        MatStepperModule,
        AuthenticationModule,
        FuseWidgetModule,
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatDividerModule,
        MatTableModule,
        MatDatepickerModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatTooltipModule,
        ColorPickerModule,
        MatSidenavModule,
        MatRadioModule,
        MatCardModule,
        MatCheckboxModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        MatChipsModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatGoogleMapsAutocompleteModule,
        MatProgressSpinnerModule,
        MatGridListModule,


    ],
    entryComponents: [
    ],

    declarations: [

        HeaderImageComponent,
        ComingSoonComponent,
        Error404Component,
        Error500Component,
        FaqComponent,
        ForgotPasswordComponent,
        KnowledgeBaseArticleComponent,
        KnowledgeBaseComponent,
        MaintenanceComponent,
        TermsComponent,
        HomeComponent,




    ],
    exports: [

        ComingSoonComponent,
        Error404Component,
        Error500Component,
        FaqComponent,
        ForgotPasswordComponent,
        TermsComponent

    ],
    providers: [
        KnowledgeBaseService,
        DataStorageService,
        FaqService




    ]

})
export class PublicModule {
}

