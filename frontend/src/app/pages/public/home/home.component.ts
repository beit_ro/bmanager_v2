import {Component, ElementRef, NgZone, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {DomSanitizer} from "@angular/platform-browser";
import {Appearance} from "@angular-material-extensions/google-maps-autocomplete";
import PlaceResult = google.maps.places.PlaceResult;
import {DataStorageService} from "../../../shared/_services/data-storage.service";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class HomeComponent extends DefaultComponent implements OnInit {

    public bgImage;
    public backgroundImage;
    public MinHeight;

    public appearance = Appearance;
    public zoom: number;
    public latitude: number;
    public longitude: number;
    public value;
    public selectedAddress: PlaceResult;


    constructor(
        private sanitizer: DomSanitizer,
        private dataStorage: DataStorageService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit() {


        this.backgroundImage = '/assets/images/others/homepage.jpg';
        this.MinHeight = '100px;';
        this.bgImage = this.sanitizer.bypassSecurityTrustStyle('url(' + this.backgroundImage + ')');

        this.checkSession();


    }


    onAutocompleteSelected(result: PlaceResult) {
        this.dataStorage.setData('address', result);
        //console.log('onAutocompleteSelected: ', result);
    }


    onChange(result: PlaceResult) {
    }

    onLocationSelected(location: Location) {
        console.log('onLocationSelected: ', location);
        // @ts-ignore
        this.latitude = location.latitude;
        // @ts-ignore
        this.longitude = location.longitude;
        this.dataStorage.setData('location', location);


    }




    showInfo()
    {

        this.router.navigate(['/public/suppliers']);

    }


    checkSession(){
        if(this.dataStorage.getData('uid')){
            console.log(this.dataStorage.getData('uid'));
        }
        else
        {
            let session = this.makeid(64);
            this.dataStorage.setData('uid', session);
        }

    }

     makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
         // @ts-ignore
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            // @ts-ignore
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

}
