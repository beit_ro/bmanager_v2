import {Routes} from "@angular/router";
import {ComingSoonComponent} from "./coming-soon/coming-soon.component";
import {Error404Component} from "./errors/404/error-404.component";
import {Error500Component} from "./errors/500/error-500.component";
import {FaqComponent} from "./faq/faq.component";
import {KnowledgeBaseComponent} from "./knowledge-base/knowledge-base.component";
import {KnowledgeBaseService} from "./knowledge-base/knowledge-base.service";
import {MaintenanceComponent} from "./maintenance/maintenance.component";
import {HomeComponent} from "./home/home.component";
import {TermsComponent} from "./terms/terms.component";


export const publicRoutes: Routes = [
    {
        path: 'comingsoon',
        component: ComingSoonComponent
    },
    {
        path: '404',
        component: Error404Component
    },

    {
        path: '500',
        component: Error500Component
    },

    {
        path     : 'faq',
        component: FaqComponent

    },
    {
        path     : 'knowledge-base',
        component: KnowledgeBaseComponent,
        resolve  : {
            knowledgeBase: KnowledgeBaseService
        }
    },
    {
        path     : 'maintenance',
        component: MaintenanceComponent
    },
    {
        path     : 'home',
        component: HomeComponent
    },
    {
        path     : 'terms',
        component:  TermsComponent
    }

];
