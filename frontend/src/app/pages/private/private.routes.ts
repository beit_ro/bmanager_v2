import {Routes} from "@angular/router";
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {GroupsComponent} from './groups/groups.component';
import {UsersComponent} from "./users/users.component";
import {PermissionsComponent} from "./permissions/permissions.component";
import {NavigationsComponent} from "./navigation/navigations.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {NotificationsComponent} from "./notifications/notifications.component";
import {SettingsComponent} from "./settings/settings.component";
import {CitiesComponent} from './cities/cities.component';
import {FilesComponent} from './files/files.component';
import {OrganizationsComponent} from './organizations/organizations.component';
import {ProjectsComponent} from './projects/projects.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {ExpensesComponent} from './expenses/expenses.component';

export const privateRoutes: Routes = [
    {
        path: 'user-profile',
        component: UserProfileComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'files',
        component: FilesComponent
    },
    {
        path: 'invoices',
        component: InvoicesComponent
    },
    {
        path: 'expenses',
        component: ExpensesComponent
    },
    {
        path: 'notifications',
        component: NotificationsComponent
    },
    {
        path: 'projects',
        component: ProjectsComponent
    },

    {
        path: 'settings',
        children: [
            {
                path: 'groups',
                component: GroupsComponent
            }
        ]
    },

    {
        path: 'settings',
        children: [
            {
                path: 'users',
                component: UsersComponent
            }
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'permissions',
                component: PermissionsComponent
            }
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'navigations',
                component: NavigationsComponent
            }
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'settings',
                component: SettingsComponent
            }
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'cities',
                component: CitiesComponent
            }
        ]
    },
    {
        path: 'settings',
        children: [
            {
                path: 'organizations',
                component: OrganizationsComponent
            }
        ]
    }

];
