import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Overlay} from '@angular/cdk/overlay';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {ManageOrganizationComponent} from '../manage-organization/manage-organization.component';
import {OrganizationsService} from "../organizations.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'organizations-list',
    templateUrl: './organizations-list.component.html',
    styleUrls: ['./organizations-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class OrganizationsListComponent extends DefaultComponent implements OnInit, OnDestroy {

    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    organizations: any = [];
    dataSource: {};
    filters: any = {};

    displayedColumnsOrganizations: string[] = ['organisation_name','address','vat_no','date_inclusion','director_name','iban','action'];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;


    /**
     * Constructor
     *
     * @param {OrganizationsService} organizationsService
     * @param {MatDialog} _matDialog
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private organizationsService: OrganizationsService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit() {


        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 5
        }


        this.getOrganizations();


        this.organizationsService.onItemUpdate
            .subscribe(organization => {
                this.getOrganizations();
            });


        this.organizationsService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getOrganizations();
            });


    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getOrganizations();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit tenant
     *
     * @param tenant
     */



    getOrganizations() {

        const queryParams = {
            ...this.filters,
            type: 'public',
            relationships:'owner'
        };

        return this.organizationsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.organizations = data;
                    this.dataSource = new MatTableDataSource(this.organizations.data);

                }
            );
    }


    manageOrganization(organizationId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManageOrganizationComponent, {
            panelClass: 'organization-dialog',
            minWidth: '40vw',
            data: {
                organizationId: organizationId ? organizationId : null,
                title: 'Edit organization'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getOrganizations();
            });
    }

    deleteOrganization(organizationId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: organizationId
                };
                return this.organizationsService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getOrganizations();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


}
