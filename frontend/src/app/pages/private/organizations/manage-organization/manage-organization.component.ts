import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {OrganizationsService} from "../organizations.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-organization',
    templateUrl: './manage-organization.component.html',
    styleUrls: ['./manage-organization.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageOrganizationComponent extends DefaultComponent implements OnInit {

    organization: any = {};
    users: any = [];
    organizationId: number;
    organizationParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageOrganizationComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private organizationsService: OrganizationsService,
        public _matDialog: MatDialog,
        private organizationService: OrganizationsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.organizationId = this._data.organizationId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Organization';
        }
        if (this.organizationId) {
            this.getOrganization(this.organizationId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getOrganization(organizationId): Subscription {
        const queryParams = {
            id: organizationId
        };

        return this.organizationsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.organization = data;
                }
            );

    }




    saveOrganization(): void {

        const queryParams: any = {
            ...this.organization
        };

        const service = this.organization.id ?
            this.organizationsService.update(queryParams) : this.organizationsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.organization = data;
                    this.organizationsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
