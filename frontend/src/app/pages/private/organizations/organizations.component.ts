import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ManageOrganizationComponent} from './manage-organization/manage-organization.component';
import {OrganizationsService} from "./organizations.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";


@Component({
    selector     : 'organizations',
    templateUrl  : './organizations.component.html',
    styleUrls    : ['./organizations.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class OrganizationsComponent extends DefaultComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    searchInput: FormControl;

    /**
     *
     * @param organizationsService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     */
    constructor(
        private organizationsService: OrganizationsService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
         router: Router,
         activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        console.log('rights',this.rights)

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.organizationsService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New organization
     */
    newOrganization(): void
    {
        this.dialogRef = this._matDialog.open(ManageOrganizationComponent, {
            panelClass: 'organization-dialog',
            minWidth: '40vw',
            data      : {
                title: 'New organization'
            }
        });
    }

}
