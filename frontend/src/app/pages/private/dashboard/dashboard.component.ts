import {Component, Injectable, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

import {fuseAnimations} from '@fuse/animations';

import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {takeUntil} from "rxjs/operators";
import {Overlay} from "@angular/cdk/overlay";
import {MatDialog} from "@angular/material/dialog";
import {DefaultComponent} from '../../../shared/_components/default.component';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../shared/_services/shared-routes.service';
import {DashboardService} from './dashboard.service';

@Component({
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class DashboardComponent extends DefaultComponent implements OnInit {
    projects: any[];


    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    dateNow = Date.now();
    stats: any;


    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param _matDialog
     * @param overlay
     * @param dashService
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        private dashService: DashboardService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

    }


    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
       this.getStats();
    }

    getStats(): Subscription {
        const queryParams = {};

        return this.dashService
            .getstats(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.stats = data;
                    //console.log('stats', data);

                }

            );

    }




}

