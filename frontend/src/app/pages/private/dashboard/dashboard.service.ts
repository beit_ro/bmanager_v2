import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../shared/_services/abstract/default.service";

export class DashboardService extends DefaultService {


    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('dashboard');


    }


    getstats(queryParams) {
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
            .set('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');

        const httpOptions = {
            headers: new HttpHeaders({
                'Access-Control-Allow-Origin': '*'
            })
        };
        let result = this.http.get(this.endpoint + this.resourceLink + '/stats', httpOptions);
        return result;
    }


}
