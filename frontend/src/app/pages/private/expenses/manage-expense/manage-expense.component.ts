import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {ExpensesService} from "../expenses.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {ExpensesTypService} from '../expense_type.service';


@Component({
    selector: 'manage-expense',
    templateUrl: './manage-expense.component.html',
    styleUrls: ['./manage-expense.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageExpenseComponent extends DefaultComponent implements OnInit {

    expense: any = {};
    users: any = [];
    expenses_type: any = [];
    expenseId: number;
    expenseParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    private breakpoint: number;

    constructor(
        public matDialogRef: MatDialogRef<ManageExpenseComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private expensesService: ExpensesService,
        public _matDialog: MatDialog,
        private expenseservice: ExpensesService,
        private expensestypeservice: ExpensesTypService,

        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.breakpoint = (window.innerWidth <= 400) ? 1 : 2;
        this.getExpensesType();
        this.expenseId = this._data.expenseId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Expense';
        }
        if (this.expenseId) {
            this.getExpense(this.expenseId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getExpense(expenseId): Subscription {
        const queryParams = {
            id: expenseId
        };

        return this.expensesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.expense = data;
                }
            );

    }



    getExpensesType(): Subscription {
        const queryParams = {
            paginated:false
        };

        return this.expensestypeservice
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.expenses_type = data;
                }
            );

    }




    saveExpense(): void {

        const queryParams: any = {
            ...this.expense
        };

        const service = this.expense.id ?
            this.expensesService.update(queryParams) : this.expensesService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.expense = data;
                    this.expensesService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


    onResize(event) {
        this.breakpoint = (event.target.innerWidth <= 400) ? 1 : 6;
    }

}
