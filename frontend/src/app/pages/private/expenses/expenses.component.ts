import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ExpensesService} from "./expenses.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";
import {ManageExpenseComponent} from './manage-expense/manage-expense.component';


@Component({
    selector     : 'expenses',
    templateUrl  : './expenses.component.html',
    styleUrls    : ['./expenses.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ExpensesComponent extends DefaultComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    searchInput: FormControl;

    /**
     *
     * @param expensesService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private expensesService: ExpensesService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
         router: Router,
         activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
       // console.log('rights',this.rights)

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.expensesService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New expense
     */
    newExpense(): void
    {
        this.dialogRef = this._matDialog.open(ManageExpenseComponent, {
            panelClass: 'expense-dialog',
            minWidth: '80vw',
            //minHeight: '80vh',
            data      : {
                title: 'New expense'
            }
        });
    }

}
