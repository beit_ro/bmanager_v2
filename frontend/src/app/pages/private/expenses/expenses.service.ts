import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../shared/_services/abstract/default.service";

export class ExpensesService extends DefaultService {

    link: any;
    filters: any = {};

    onItemUpdate: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('expenses');
        this.onItemUpdate = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();

    }

}
