import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {ProjectsService} from "../projects.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-project',
    templateUrl: './manage-project.component.html',
    styleUrls: ['./manage-project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageProjectComponent extends DefaultComponent implements OnInit {

    project: any = {};
    users: any = [];
    projectId: number;
    projectParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageProjectComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private projectsService: ProjectsService,
        public _matDialog: MatDialog,
        private projectService: ProjectsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.projectId = this._data.projectId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Project';
        }
        if (this.projectId) {
            this.getProject(this.projectId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getProject(projectId): Subscription {
        const queryParams = {
            id: projectId
        };

        return this.projectsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.project = data;
                }
            );

    }




    saveProject(): void {

        const queryParams: any = {
            ...this.project
        };

        const service = this.project.id ?
            this.projectsService.update(queryParams) : this.projectsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.project = data;
                    this.projectsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
