import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {CitiesService} from "../cities.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-city',
    templateUrl: './manage-city.component.html',
    styleUrls: ['./manage-city.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageCityComponent extends DefaultComponent implements OnInit {

    city: any = {};
    users: any = [];
    cityId: number;
    cityParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageCityComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private citiesService: CitiesService,
        public _matDialog: MatDialog,
        private citieservice: CitiesService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.cityId = this._data.cityId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New City';
        }
        if (this.cityId) {
            this.getCity(this.cityId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getCity(cityId): Subscription {
        const queryParams = {
            id: cityId
        };

        return this.citiesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.city = data;
                }
            );

    }




    saveCity(): void {

        const queryParams: any = {
            ...this.city
        };

        const service = this.city.id ?
            this.citiesService.update(queryParams) : this.citiesService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.city = data;
                    this.citiesService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
