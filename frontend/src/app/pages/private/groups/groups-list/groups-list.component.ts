import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Overlay} from '@angular/cdk/overlay';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {GroupsService} from "../groups.service";
import {ManageGroupComponent} from "../manage-group/manage-group.component";
import {GroupRightComponent} from "../group-right/group-right.component";


@Component({
    selector: 'groups-list',
    templateUrl: './groups-list.component.html',
    styleUrls: ['./groups-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class GroupsListComponent extends DefaultComponent implements OnInit, OnDestroy {

    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    groups: any = [];
    dataSource: {};
    filters: any = {};

    displayedColumnsGroups: string[] = ['id', 'name', 'code', 'observations', 'action'];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;


    /**
     * Constructor
     *
     * @param {GroupsService} groupsService
     * @param {MatDialog} _matDialog
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private groupsService: GroupsService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit() {


        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 5
        }


        this.getGroups();


        this.groupsService.onItemUpdate
            .subscribe(user => {
                this.getGroups();
            });


        this.groupsService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getGroups();
            });


    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getGroups();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit tenant
     *
     * @param tenant
     */



    getGroups() {

        const queryParams = {
            ...this.filters
        };

        return this.groupsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.groups = data;
                    this.dataSource = new MatTableDataSource(this.groups.data);

                }
            );
    }



    manageGroupRights(groupId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(GroupRightComponent, {
            panelClass: 'manage-group-right',
            minWidth: '40vw',
            data: {
                groupId: groupId ? groupId : null,
                title: 'Edit rights'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getGroups();
            });
    }



    manageGroup(groupId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManageGroupComponent, {
            panelClass: 'group-dialog',
            minWidth: '40vw',
            data: {
                groupId: groupId ? groupId : null,
                title: 'Edit group'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getGroups();
            });
    }

    deleteGroup(groupId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: groupId
                };
                return this.groupsService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getGroups();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


}
