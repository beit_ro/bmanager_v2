import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {SharedUserService} from "../../../../shared/_services/shareduser.service";
import {GroupsService} from "../groups.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-group',
    templateUrl: './manage-group.component.html',
    styleUrls: ['./manage-group.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageGroupComponent extends DefaultComponent implements OnInit {

    group: any = {};
    users: any = [];
    groupId: number;
    groupUsers: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    constructor(
        public matDialogRef: MatDialogRef<ManageGroupComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private groupsService: GroupsService,
        public _matDialog: MatDialog,
        private usersService: SharedUserService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    ngOnInit(): void {
        this.groupId = this._data.groupId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Group';
        }

        if (this.groupId) {
            this.getGroup(this.groupId);
        } else {
            this.getUsers();
        }

    }


    close(): void {
        this.matDialogRef.close();
    }

    getUsers(): Subscription {
        const queryParams = {
            paginated: false,
            relationships: 'details'
        };

        return this.usersService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.users = data;
                    for (let i in this.group.user_groups) {
                        this.groupUsers.push(this.group.user_groups[i].user_id);
                    }
                    this.groupUsers = [...this.groupUsers];
                }
            );

    }


    getGroup(groupId): Subscription {
        const queryParams = {
            id: groupId,
            relationships: 'user_groups.user'
        };

        return this.groupsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.group = data;
                    this.getUsers();
                }
            );

    }

    saveGroup(): void {

        // @ts-ignore
        let data = JSON.parse(JSON.stringify(this.group));
        data.user_groups = [];
        this.groupUsers.forEach((item) => {
            data.user_groups.push({
                user_id: item
            });
        });
        const queryParams: any = {
            ...data
        };

        const service = this.group.id ?
            this.groupsService.update(queryParams) : this.groupsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.group = data;
                    this.groupsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
