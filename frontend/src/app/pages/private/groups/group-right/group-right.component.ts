import {Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {fuseAnimations} from "../../../../../@fuse/animations";
import {CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {PermissionsService} from "../../permissions/permissions.service";
import {takeUntil} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {GroupRightsService} from "./group-rights.service";
import {SharedUserService} from "../../../../shared/_services/shareduser.service";






@Component({
    selector     : 'group-right',
    templateUrl  : './group-right.component.html',
    styleUrls    : ['./group-right.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class GroupRightComponent extends DefaultComponent  implements OnInit
{

    action: string;
    rights: any = [];
    groupId: number;
    groupRights: any = [];
    groupForm: FormGroup;
    dialogTitle: string;
    saveData: any ={};

    /**
     * Constructor
     *
     * @param matDialogRef
     * @param _data
     * @param permissionsService
     * @param groupRightsService
     * @param router
     * @param activatedRoute
     * @param _formBuilder
     * @param sharedRoutesService
     */
    constructor(
        public matDialogRef: MatDialogRef<GroupRightComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private permissionsService: PermissionsService,
        private groupRightsService: GroupRightsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
    }
    ngOnInit(): void {

        this.groupId = this._data.groupId;
        this.getExistingRights();


    }


    addRights(event: CdkDragDrop<string[]>) {

        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {

            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            var moveditem= JSON.parse(JSON.stringify(event.container.data[event.currentIndex]));

            this.saveData.right_id = moveditem.id;
            this.saveData.group_id = this.groupId;

            const queryParams = {
            ...this.saveData
            };
            return this.groupRightsService
                .create(queryParams)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe((data) => {

                });

        }



    }


    removeRights(event: CdkDragDrop<string[]>) {

        if (event.previousContainer === event.container) {
           moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {

            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
                var moveditem = JSON.parse(JSON.stringify(event.container.data[event.currentIndex]));

                const queryParams = {
                    right_id: moveditem.id,
                    group_id:this.groupId
                };
                return this.groupRightsService
                    .removeRight(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {

                    });

        }

    }


    getUniquePermissions() {

        const queryParams = {
            group_id : this.groupId
        };

        return this.permissionsService
            .uniqueList(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.rights = data;

                }
            );
    }



    getExistingRights() {
        const queryParams = {
            pagination: false,
            id : this.groupId
        };

        this.groupRightsService
            .getRights(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (right: any) => {
                    this.groupRights = right;
                    this.getUniquePermissions();
                });



    }

}
