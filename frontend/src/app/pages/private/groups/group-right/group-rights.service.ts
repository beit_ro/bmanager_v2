import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../../shared/_services/abstract/default.service";

export class GroupRightsService extends DefaultService {

    link: any;
    filters: any = {};

    onGroupUpdate: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;


    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('group_rights');
        this.onGroupUpdate = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();

    }


    getRights(queryParams) {
        return this.http.get(this.endpoint + 'groups/' + queryParams.id + '/rights');
    }


    removeRight(queryParams): Observable<Object> {
        const httpParams = new HttpParams({fromObject: queryParams});
        return this.http.get(this.endpoint +  'rights/removeRight', {params: httpParams});    }
}
