import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {GroupsService} from "./groups.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";
import {ManageGroupComponent} from "./manage-group/manage-group.component";


@Component({
    selector     : 'groups',
    templateUrl  : './groups.component.html',
    styleUrls    : ['./groups.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class GroupsComponent extends DefaultComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    searchInput: FormControl;

    /**
     *
     * @param groupsService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private groupsService: GroupsService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
         router: Router,
         activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        console.log('rights',this.rights)

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.groupsService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New group
     */
    newGroup(): void
    {
        this.dialogRef = this._matDialog.open(ManageGroupComponent, {
            panelClass: 'group-dialog',
            minWidth: '40vw',
            data      : {
                title: 'New group'
            }
        });
    }

}
