import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatToolbarModule,
    MatListModule,
    MatStepperModule,
    MatDividerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatRadioModule,
    MatSidenavModule,
    MatCardModule, MatCheckboxModule, MatMenuModule, MatTabsModule, MatSelectModule, MatAutocompleteModule
} from "@angular/material";
import {RouterModule} from "@angular/router";
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {
    FuseConfirmDialogModule,
    FuseCountdownModule,
    FuseProgressBarModule,
    FuseSidebarModule,
    FuseThemeOptionsModule, FuseWidgetModule
} from '../../../@fuse/components';

import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {GroupsComponent} from './groups/groups.component';
import {GroupsListComponent} from './groups/groups-list/groups-list.component';
import {AuthenticationModule} from '../authentication/authentication.module';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {ColorPickerModule} from 'ngx-color-picker';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {NgSelectModule} from '@ng-select/ng-select';
import {UserListComponent} from "./users/user-list/user-list.component";
import {ManageUserComponent} from "./users/manage-user/manage-user.component";
import {UsersComponent} from "./users/users.component";
import {UsersService} from "./users/users.service";
import {NgSelectFormFieldControlDirective} from "../../shared/_directives/ng-select.directive";
import {GroupsService} from "./groups/groups.service";
import {PermissionsComponent} from "./permissions/permissions.component";
import {PermissionListComponent} from "./permissions/permissions-list/permissions-list.component";
import {ManagePermissionComponent} from "./permissions/manage-permission/manage-permission.component";
import {PermissionsService} from "./permissions/permissions.service";
import {ClassesService} from "./permissions/classes.service";
import {NavigationsComponent} from "./navigation/navigations.component";
import {ManageNavigationComponent} from "./navigation/manage-navigation/manage-navigation.component";
import {NavigationsListComponent} from "./navigation/navigations-list/navigations-list.component";
import {NavigationsService} from "./navigation/navigations.service";
import {UserRightComponent} from "./users/user-right/user-right.component";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {MatChipsModule} from "@angular/material/chips";
import {MatRippleModule} from "@angular/material/core";
import {MatSortModule} from "@angular/material/sort";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {SharedRoutesService} from "../../shared/_services/shared-routes.service";
import {ManageRoutesComponent} from "./navigation/manage-routes/manage-routes.component";
import {RoutesService} from "./navigation/manage-routes/routes.service";
import {UserRightsService} from "./users/user-right/user-rights.service";
import {ManageGroupComponent} from "./groups/manage-group/manage-group.component";
import {GroupRightComponent} from "./groups/group-right/group-right.component";
import {GroupRightsService} from "./groups/group-right/group-rights.service";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {DashboardService} from "./dashboard/dashboard.service";
import {NotificationsComponent} from "./notifications/notifications.component";
import {ManageNotificationComponent} from "./notifications/manage-notification/manage-notification.component";
import {NotificationsListComponent} from "./notifications/notifications-list/notifications-list.component";
import {NotificationsService} from "./notifications/notifications.service";
import {AgmCoreModule} from "@agm/core";
import {AgmJsMarkerClustererModule} from "@agm/js-marker-clusterer";
import {MatGoogleMapsAutocompleteModule} from "@angular-material-extensions/google-maps-autocomplete";
import { SettingsComponent } from 'app/pages/private/settings/settings.component';
import {ManageSettingComponent} from "./settings/manage-category/manage-setting.component";
import { SettingsListComponent } from 'app/pages/private/settings/settings-list/settings-list.component';
import { SettingsService } from 'app/pages/private/settings/settings.service';
import {CitiesComponent} from './cities/cities.component';
import {ManageCityComponent} from './cities/manage-citiy/manage-city.component';
import { CitiesListComponent } from 'app/pages/private/cities/cities-list/cities-list.component';
import {CitiesService} from './cities/cities.service';
import {FilesComponent} from './files/files.component';
import {FileManagerMainSidebarComponent} from './files/sidebars/main/main.component';
import {FileManagerDetailsSidebarComponent} from './files/sidebars/details/details.component';
import {FilesService} from './files/files.service';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {ManageFolderComponent} from './files/manage-folder/manage-folder.component';
import {FoldersService} from './files/folders.service';
import {FileListComponent} from './files/file-list/file-list.component';
import {FolderListComponent} from './files/folder-list/folder-list.component';
import {ManageFileComponent} from './files/manage-file/manage-file.component';
import {OrganizationsService} from './organizations/organizations.service';
import {ManageOrganizationComponent} from './organizations/manage-organization/manage-organization.component';
import {OrganizationsComponent} from './organizations/organizations.component';
import {OrganizationsListComponent} from './organizations/organizations-list/organizations-list.component';
import {ProjectsComponent} from './projects/projects.component';
import {ProjectsListComponent} from './projects/projects-list/projects-list.component';
import {ManageProjectComponent} from './projects/manage-project/manage-project.component';
import {ProjectsService} from './projects/projects.service';
import {RenameFileComponent} from './files/rename-file/rename-file.component';
import {PreviewFileComponent} from './files/preview-file/preview-file.component';
import {ShareFileComponent} from './files/share-file/share-file.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {InvoicesListComponent} from './invoices/invoices-list/invoices-list.component';
import {ManageInvoiceComponent} from './invoices/manage-invoice/manage-invoice.component';
import {InvoicesService} from './invoices/invoices.service';
import {ExpensesComponent} from './expenses/expenses.component';
import {ExpensesListComponent} from './expenses/expenses-list/expenses-list.component';
import {ManageExpenseComponent} from './expenses/manage-expense/manage-expense.component';
import {ExpensesService} from './expenses/expenses.service';
import {ExpensesTypService} from './expenses/expense_type.service';
import {MatGridListModule} from '@angular/material/grid-list';




@NgModule({
    imports: [

        // material components
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatExpansionModule,
        MatListModule,
        MatToolbarModule,
        MatDialogModule,

        // fuse components
        FuseSharedModule,
        FuseCountdownModule,

        // other components
        RouterModule,

        // other Modules
        MatPaginatorModule,
        MatStepperModule,
        AuthenticationModule,
        FuseSharedModule,
        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatTableModule,
        MatDatepickerModule,
        MatDialogModule,
        MatInputModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        ColorPickerModule,
        MatSidenavModule,
        MatRadioModule,
        MatCardModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,

        // Fuse modules
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        FuseConfirmDialogModule,

        // App modules
        BrowserAnimationsModule,
        RouterModule,


        ChartsModule,
        NgxChartsModule,
        DragDropModule,

        FuseSharedModule,
        FuseWidgetModule,
        BrowserModule,
        CommonModule,

        MatAutocompleteModule,
        ReactiveFormsModule,
        NgxMatSelectSearchModule,
        NgSelectModule,
        FormsModule,
        AgmJsMarkerClustererModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB77OC0QNHhmHrPBtWXKLZfMSN-HvKX-ZE',
            libraries: ["places"]
        }),
        MatGoogleMapsAutocompleteModule,
        MatProgressBarModule,
        MatGridListModule,
    ],
    declarations: [

        // Groups
        GroupsComponent,
        ManageGroupComponent,
        GroupsListComponent,
        ManageRoutesComponent,
        GroupRightComponent,

        //User Profile
        UserProfileComponent,

        //Users
        UsersComponent,
        UserListComponent,
        ManageUserComponent,
        UserRightComponent,

        //Permissions
        PermissionsComponent,
        PermissionListComponent,
        ManagePermissionComponent,

        // Navigation
        NavigationsComponent,
        ManageNavigationComponent,
        NavigationsListComponent,


        // Info
        NotificationsComponent,
        ManageNotificationComponent,
        NotificationsListComponent,



        // Cities
        CitiesComponent,
        ManageCityComponent,
        CitiesListComponent,


        // Settings
        SettingsComponent,
        ManageSettingComponent,
        SettingsListComponent,

        //Dashboard
        DashboardComponent,


        // Files
        FilesComponent,
        FileListComponent,
        FolderListComponent,
        FileManagerMainSidebarComponent,
        FileManagerDetailsSidebarComponent,
        ManageFileComponent,
        RenameFileComponent,
        PreviewFileComponent,
        ShareFileComponent,

        ManageFolderComponent,


        //Organizations
        OrganizationsComponent,
        OrganizationsListComponent,
        ManageOrganizationComponent,

        //Projects
        ProjectsComponent,
        ProjectsListComponent,
        ManageProjectComponent,

        //Invoices
        InvoicesComponent,
        InvoicesListComponent,
        ManageInvoiceComponent,

        //Expenses
        ExpensesComponent,
        ExpensesListComponent,
        ManageExpenseComponent,







        NgSelectFormFieldControlDirective


    ],

    entryComponents: [

        ManageGroupComponent,
        ManageUserComponent,
        UserRightComponent,
        ManagePermissionComponent,
        ManageNavigationComponent,
        ManageNotificationComponent,
        ManageSettingComponent,
        ManageCityComponent,
        ManageRoutesComponent,
        GroupRightComponent,
        ManageFolderComponent,
        ManageFileComponent,
        RenameFileComponent,
        ManageOrganizationComponent,
        ManageProjectComponent,
        PreviewFileComponent,
        ShareFileComponent,
        ManageInvoiceComponent,
        ManageExpenseComponent





    ],

    exports: [



    ],
    providers: [


        GroupsService,
        NavigationsService,
        UsersService,
        PermissionsService,
        ClassesService,
        SharedRoutesService,
        RoutesService,
        UserRightsService,
        GroupRightsService,
        DashboardService,
        NotificationsService,
        SettingsService,
        CitiesService,
        FilesService,
        FoldersService,
        OrganizationsService,
        ProjectsService,
        InvoicesService,
        ExpensesService,
        ExpensesTypService


    ]

})

export class PrivateModule {
}

