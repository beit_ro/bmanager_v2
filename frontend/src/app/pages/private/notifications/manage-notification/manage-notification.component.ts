import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {NotificationsService} from "../notifications.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-notification',
    templateUrl: './manage-notification.component.html',
    styleUrls: ['./manage-notification.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageNotificationComponent extends DefaultComponent implements OnInit {

    notification: any = {};
    users: any = [];
    notificationId: number;
    notificationParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageNotificationComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private notificationsService: NotificationsService,
        public _matDialog: MatDialog,
        private notificationService: NotificationsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.notificationId = this._data.notificationId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Notification';
        }
        if (this.notificationId) {
            this.getNotification(this.notificationId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getNotification(notificationId): Subscription {
        const queryParams = {
            id: notificationId
        };

        return this.notificationsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.notification = data;
                }
            );

    }




    saveNotification(): void {

        const queryParams: any = {
            ...this.notification
        };

        const service = this.notification.id ?
            this.notificationsService.update(queryParams) : this.notificationsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.notification = data;
                    this.notificationsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
