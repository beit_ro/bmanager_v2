import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ManageNavigationComponent} from './manage-navigation/manage-navigation.component';
import {NavigationsService} from "./navigations.service";
import {ActivatedRoute, NavigationStart, Router} from "@angular/router";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";


@Component({
    selector     : 'navigations',
    templateUrl  : './navigations.component.html',
    styleUrls    : ['./navigations.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class NavigationsComponent extends DefaultComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    searchInput: FormControl;

    /**
     *
     * @param navigationsService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     */
    constructor(
        private navigationsService: NavigationsService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
         router: Router,
         activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

console.log('sssssss',this.rights);

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.navigationsService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New navigation
     */
    newNavigation(): void
    {
        this.dialogRef = this._matDialog.open(ManageNavigationComponent, {
            panelClass: 'navigation-dialog',
            minWidth: '40vw',
            data      : {
                title: 'New navigation'
            }
        });
    }

}
