import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Overlay} from '@angular/cdk/overlay';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {ManageNavigationComponent} from '../manage-navigation/manage-navigation.component';
import {NavigationsService} from "../navigations.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ManageRoutesComponent} from "../manage-routes/manage-routes.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'navigations-list',
    templateUrl: './navigations-list.component.html',
    styleUrls: ['./navigations-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class NavigationsListComponent extends DefaultComponent implements OnInit, OnDestroy {

    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    navigations: any = [];
    dataSource: {};
    filters: any = {};

    displayedColumnsNavigations: string[] = ['id', 'title', 'type', 'icon', 'url', 'parent', 'action'];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;




    /**
     * Constructor
     *
     * @param {StructuresService} navigationsService
     * @param {MatDialog} _matDialog
     * @param overlay
     */
    constructor(
        private navigationsService: NavigationsService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit() {


        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 5
        }


        this.getNavigations();


        this.navigationsService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getNavigations();
            });


    }



    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getNavigations();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit tenant
     *
     * @param tenant
     */



    getNavigations() {

        const queryParams = {
            ...this.filters,
            relationships: 'parent',
            orderby: 'position',
            order: 'asc'
        };

        return this.navigationsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.navigations = data;
                    this.dataSource = new MatTableDataSource(this.navigations.data);

                }
            );
    }


    manageNavigation(navigationId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManageNavigationComponent, {
            panelClass: 'navigation-dialog',
            minWidth: '40vw',
            data: {
                navigationId: navigationId ? navigationId : null,
                title: 'Edit navigation'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getNavigations();
            });
    }

    deleteNavigation(navigationId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: navigationId
                };
                return this.navigationsService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getNavigations();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


    manageRoutes(navigationId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManageRoutesComponent, {
            panelClass: 'route-dialog',
            minWidth: '40vw',
            data: {
                navigationId: navigationId ? navigationId : null,
                title: 'Edit route'
            },
            autoFocus: false,
            scrollStrategy

        });
        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
            });
    }



}
