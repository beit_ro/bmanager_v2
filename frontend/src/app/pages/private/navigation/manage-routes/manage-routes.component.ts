import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {RoutesService} from "./routes.service";
import {MatTableDataSource} from "@angular/material/table";


@Component({
    selector: 'manage-routes',
    templateUrl: './manage-routes.component.html',
    styleUrls: ['./manage-routes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageRoutesComponent extends DefaultComponent implements OnInit {

    route: any = {};
    routes: any = [];
    navigationId: number;
    dialogTitle: string;
    dataSource: {};
    displayedColumnsGroups: string[] = ['id', 'route', 'action'];


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    constructor(
        public matDialogRef: MatDialogRef<ManageRoutesComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        public _matDialog: MatDialog,
        private routeService: RoutesService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    ngOnInit(): void {
        this.dataSource = new MatTableDataSource();

        this.route.navigation_id = this._data.navigationId;
        this.navigationId = this._data.navigationId;


        if (this.navigationId) {
            this.getRoutes(this.navigationId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }

    getRoutes(navigationId): Subscription {
        const queryParams = {
            navigation_id: navigationId,
            paginated: false
        };

        return this.routeService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.routes = data;
                    this.dataSource = new MatTableDataSource(this.routes);
                    console.log('data',this.routes)

                }
            );

    }

    addRoute(): void {
        const queryParams: any = {
            ...this.route
        };

      this.routeService.create(queryParams)
          .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.getRoutes(this.navigationId);

                }
            );

    }



    deleteRoute(routeId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: routeId
                };
                return this.routeService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getRoutes(this.navigationId);
                    });

            }
            this.confirmDialogRef = null;
        });


    }


}
