import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {NavigationsService} from "../navigations.service";
import {FuseNavigationService} from "../../../../../@fuse/components/navigation/navigation.service";
import {SharedNavigationService} from "../../../../shared/_services/sharednavigation.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-navigation',
    templateUrl: './manage-navigation.component.html',
    styleUrls: ['./manage-navigation.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageNavigationComponent extends DefaultComponent implements OnInit {

    navigation: any = {};
    users: any = [];
    navigationId: number;
    navigationParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageNavigationComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private navigationsService: NavigationsService,
        public _matDialog: MatDialog,
        private _fuseNavigationService: FuseNavigationService,
        private sharedNavigationService: SharedNavigationService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    ngOnInit(): void {
        this.navigationId = this._data.navigationId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Navigation';
        }
        if (this.navigationId) {
            this.getNavigation(this.navigationId);
        } else {
            this.getParents();
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getNavigation(navigationId): Subscription {
        const queryParams = {
            id: navigationId
        };

        return this.navigationsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.navigation = data;
                    this.getParents();
                }
            );

    }

    getParents() {

        const queryParams = {
            relationships: 'parent',
            orderby: 'position',
            order: 'asc'

        };

        return this.navigationsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.navigationParents = data.data;
                }
            );
    }


    saveNavigation(): void {

        const queryParams: any = {
            ...this.navigation
        };

        const service = this.navigation.id ?
            this.navigationsService.update(queryParams) : this.navigationsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.navigation = data;
                    this.navigationsService.onItemUpdate.next(data)
                    this.sharedNavigationService.onItemUpdate.next(data)
                    this.close();
                }
            );

    }


}
