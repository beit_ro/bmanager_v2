import {
    Component,
    ElementRef,
    Inject,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
    ViewEncapsulation
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FilesService} from '../files.service';
import {HttpEventType} from '@angular/common/http';
import 'rxjs/add/operator/map'
import {Subscription} from 'rxjs';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';
import {environment} from '../../../../../environments/environment';


@Component({
    selector: 'rename-file',
    templateUrl: './rename-file.component.html',
    styleUrls: ['./rename-file.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RenameFileComponent extends DefaultComponent implements OnInit {


    project: any = environment.project;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
    files = [];
    file: any = {};
    folders: any = {};
    dialogTitle: string;
    folder: string;
    fileId: number;
    users: any;


    constructor(
        public matDialogRef: MatDialogRef<RenameFileComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        public _matDialog: MatDialog,
        private filesService: FilesService,
        private usersService: SharedUserService,
        public sanitizer: DomSanitizer,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.getUsers();
        this.fileId = this._data.fileId;
        this.dialogTitle = this._data.title;

        if (this.fileId) {
            console.log('fileid')
            this.getFile(this.fileId);
        }


    }


    close(): void {
        this.matDialogRef.close();
    }


    getUsers(): Subscription {
        const queryParams = {
            paginated: false,
            relationships: 'details'
        };

        return this.usersService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.users = data;
                    // for (let i in this.user_acceses) {
                    //     this.sharedUsers.push(this.group.user_groups[i].user_id);
                    // }
                    // this.groupUsers = [...this.groupUsers];
                }
            );

    }


    getFile(fileId): Subscription {
        const queryParams = {
            paginated: false,
            id: fileId,
            relationships: 'users.details'

        };

        return this.filesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.file = data;

                }
            );

    }


    saveFile(): void {

        // console.log('permission',this.permission);
        const queryParams: any = {


            ...this.file
        };

        const service = this.file.id ?
            this.filesService.update(queryParams) : this.filesService.create(queryParams);

        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.file = data;
                    this.filesService.onItemUpdate.next(data)
                    this.close();
                }
            );

    }


}
