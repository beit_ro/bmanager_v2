import {
    Component,
    ElementRef,
    Inject,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
    ViewEncapsulation
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FilesService} from '../files.service';
import {HttpEventType} from '@angular/common/http';
import 'rxjs/add/operator/map'
import {FoldersService} from '../folders.service';
import {Subscription} from 'rxjs';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';


@Component({
    selector: 'manage-folder',
    templateUrl: './manage-folder.component.html',
    styleUrls: ['./manage-folder.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageFolderComponent extends DefaultComponent implements OnInit {


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    folder: any = {};
    dialogTitle: any;
    private parent_id: any;
    users: any = [];
    private sharedUsers: any = [];



    constructor(
        public matDialogRef: MatDialogRef<ManageFolderComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        public _matDialog: MatDialog,
        private foldersService: FoldersService,
        private filesService: FilesService,
        private usersService: SharedUserService,
        public sanitizer: DomSanitizer,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.dialogTitle = this._data.title;
        if (!this._data.folder) {
            this.parent_id = 1;
        } else {
            this.parent_id = this._data.folder.id;
        }
        this.getUsers();

    }


    close(): void {
        this.matDialogRef.close();
    }


    saveFolder(): void {
        this.folder.type = 'folder';
        this.folder.icon = 'folder';
        this.folder.parent_id = this.parent_id;

        const queryParams: any = {
            ...this.folder
        };

        const service = this.folder.id ?
            this.foldersService.update(queryParams) : this.foldersService.create(queryParams);

        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.folder = data;
                    this.foldersService.onItemUpdate.next(data)
                    this.filesService.onItemUpdate.next(data)
                    this.close();
                }
            );

    }


    getUsers(): Subscription {
        const queryParams = {
            paginated: false,
            relationships: 'details'
        };

        return this.usersService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.users = data;
                    // for (let i in this.user_acceses) {
                    //     this.sharedUsers.push(this.group.user_groups[i].user_id);
                    // }
                    // this.groupUsers = [...this.groupUsers];
                }
            );

    }


}
