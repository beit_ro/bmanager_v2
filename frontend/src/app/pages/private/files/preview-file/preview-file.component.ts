import {
    Component,
    ElementRef,
    Inject,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
    ViewEncapsulation
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FilesService} from '../files.service';
import {HttpEventType} from '@angular/common/http';
import 'rxjs/add/operator/map'
import {Subscription} from 'rxjs';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';
import {environment} from '../../../../../environments/environment';


@Component({
    selector: 'preview-file',
    templateUrl: './preview-file.component.html',
    styleUrls: ['./preview-file.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PreviewFileComponent extends DefaultComponent implements OnInit {

    endpoint: any = environment.endpoint;
    project: any = environment.project;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
    files = [];
    file: any = {};
    folders: any = {};
    dialogTitle: string;
    folder: string;
    fileId: number;
    status_img: boolean = false;
    users: any;


    constructor(
        public matDialogRef: MatDialogRef<PreviewFileComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        public _matDialog: MatDialog,
        private filesService: FilesService,
        private usersService: SharedUserService,
        public sanitizer: DomSanitizer,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.file = this._data.file;
        if (this.file) {
            this.getFile(this.file.id);
        }


    }


    close(): void {
        this.matDialogRef.close();
    }


    getFile(fileId): Subscription {
        const queryParams = {
            paginated: false,
            id: fileId

        };

        return this.filesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.file = data;

                }
            );

    }


    status() {
        this.status_img = true;


    }
}
