import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FilesService} from '../../files.service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../../../shared/_services/shared-routes.service';
import {DefaultComponent} from '../../../../../shared/_components/default.component';


@Component({
    selector: 'file-manager-details-sidebar',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss'],
    animations: fuseAnimations
})
export class FileManagerDetailsSidebarComponent extends DefaultComponent implements OnInit, OnDestroy {
    @Input() selected: any;

    /**
     * Constructor
     *
     */
    constructor(
        private filesService: FilesService,
        private _matDialog: MatDialog,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

                console.log('ff', this.selected);

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }
}
