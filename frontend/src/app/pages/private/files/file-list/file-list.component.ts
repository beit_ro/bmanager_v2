import {Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {Observable, Subject} from 'rxjs';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FilesService} from '../files.service';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {Overlay} from '@angular/cdk/overlay';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../../shared/_services/shared-routes.service';
import {environment} from '../../../../../environments/environment';
import {RenameFileComponent} from '../rename-file/rename-file.component';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';
import {PreviewFileComponent} from '../preview-file/preview-file.component';

@Component({
    selector: 'file-list',
    templateUrl: './file-list.component.html',
    styleUrls: ['./file-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FileListComponent extends DefaultComponent implements OnInit, OnDestroy {
    @Input() selected: any;

    dialogContent: TemplateRef<any>;
    endpoint: any = environment.endpoint;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    files: any = [];
    user: any = {};
    displayedColumns = ['icon', 'name', 'extension', 'owner_id', 'fileSize', 'updated_at', 'detail-button', 'download', 'action'];
    filters: any = {};
    folder_id: number;
    // Private
    private _unsubscribeAll: Subject<any>;
    dataSource: {};
    private data_parent: any;

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param userService
     * @param filesService
     * @param _matDialog
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private userService: SharedUserService,
        private filesService: FilesService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {


        this.getUser();
        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 6
        }
        this.getFiles(this.selected);

        this.filesService.onItemUpdate
            .subscribe(file => {
                this.getFiles(this.selected);
            });

        this.filesService.onBreadcrumbSelection
            .subscribe(file => {
                this.getFiles(file);
            });


        this.filesService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getFiles(this.selected);
            });

    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getFiles(this.selected);
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    getFiles(selected) {

        const queryParams = {
            ...this.filters,
            //  type: '[!]folder',
            relationships: 'creator',
            parent_id: selected.id
        };

        return this.filesService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {

                    this.files = data;
                    this.dataSource = new MatTableDataSource(this.files.data);

                }
            );
    }


    getFile(fileId) {

        const queryParams = {
            id: fileId
        };

        return this.filesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {


                }
            );
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On select
     *
     * @param selected
     */
    onSelect(option): void {
        if (option.type == 'folder') {
            this.getFiles(option);
            this.filesService.onItemSelection.next(option);

            console.log('option', option)
        } else {
            //console.log('file')
        }

    }


    select(option): void {
        if (option.type == 'folder') {
            this.getFiles(option);
            this.filesService.onItemSelection.next(option);

            console.log('option', option)
        } else {
            console.log('file')
        }

    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    editFile(fileId) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(RenameFileComponent, {
            panelClass: 'file-dialog',
            minWidth: '40vw',
            data: {
                fileId: fileId ? fileId : null,
                title: 'Edit file'
            },
            autoFocus: false,
            scrollStrategy

        });


        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getFiles(this.selected);
            });
    }


    previewFile(file) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        if(file.type =='video' || file.type =='image' )
        {
            const dialogRef = this._matDialog.open(PreviewFileComponent, {
                panelClass: 'file-dialog',
                minWidth: '40vw',
                data: {
                    file: file ? file : null,

                },
                autoFocus: false,
                scrollStrategy

            });


            return dialogRef
                .afterClosed()
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe((result) => {
                    this.getFiles(this.selected);
                });
        }

    }


    deleteFile(fileId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: fileId
                };
                return this.filesService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getFiles(this.selected);
                    });

            }
            this.confirmDialogRef = null;
        });


    }


    downloadFile(selected): void {
        this.confirmDialogRef.componentInstance.confirmMessage = 'Compresion process has started. You will receive and email with the download link in a few minutes once it finishes ';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('download begin')

            }

        });
    }

    getUser() {
        return this.userService
            .getMe()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.user = data;
                    //console.log(this.user)

                }
            );
    }


}

