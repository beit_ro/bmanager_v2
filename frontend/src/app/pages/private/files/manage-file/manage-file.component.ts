import {
    Component,
    ElementRef,
    Inject,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
    ViewEncapsulation
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';
import {ActivatedRoute, Router} from "@angular/router";
import {FuseConfirmDialogComponent} from "../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {DomSanitizer} from "@angular/platform-browser";
import {FilesService} from '../files.service';
import {HttpEventType} from '@angular/common/http';
import 'rxjs/add/operator/map'
import {Subscription} from 'rxjs';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';
import {environment} from '../../../../../environments/environment';



@Component({
    selector: 'manage-file',
    templateUrl: './manage-file.component.html',
    styleUrls: ['./manage-file.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageFileComponent extends DefaultComponent implements OnInit {


    project: any = environment.project;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    @ViewChild("fileInput", {static: false}) fileInput: ElementRef;
    files  = [];
    file: any = {};
    fileId: number;

    folders: any = {};
    dialogTitle: string;
    folder: string;
    parent_id: number;
    users: any;
    selected_notification= 'none';
    selected_level = 'private';



    constructor(
        public matDialogRef: MatDialogRef<ManageFileComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        public _matDialog: MatDialog,
        private filesService: FilesService,
        private usersService: SharedUserService,

        public sanitizer: DomSanitizer,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {

        this.getUsers();
        this.dialogTitle = this._data.title;
        this.fileId = this._data.fileId;
        if (!this._data.folder) {
            this.parent_id = 1;
        } else {
            this.parent_id = this._data.folder.id;
        }


        if (this.fileId) {
            this.getFile(this.fileId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }





    callUploadService(file) {

        const formData = new FormData();
        formData.append('file', file.data);
        formData.append('parent_id', this._data.folder.id);
        formData.append('data', JSON.stringify(this.file));
        file.inProgress = true;
        this.filesService
            .upload(formData)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((event: any) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        file.progress = Math.round(event.loaded * 100 / event.total);
                        break;
                    case HttpEventType.Response:
                        return event;
                }



            })

    }


    private upload() {
        this.fileInput.nativeElement.value = '';
        this.files.forEach(file => {
            this.callUploadService(file);
        });
    }

    onClick() {
        this.files=[];
        const fileInput = this.fileInput.nativeElement;
        fileInput .onchange = () => {
            for (let index = 0; index < fileInput .files.length; index++)
            {
                const file = fileInput .files[index];
                this.files.push({ data: file, inProgress: false, progress: 0});
               // console.log('aaa',file);
            }
            this.upload();
        };
        fileInput.click();
    }



    getUsers(): Subscription {
        const queryParams = {
            paginated: false,
            relationships: 'details'
        };

        return this.usersService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.users = data;
                    // for (let i in this.user_acceses) {
                    //     this.sharedUsers.push(this.group.user_groups[i].user_id);
                    // }
                    // this.groupUsers = [...this.groupUsers];
                }
            );

    }
    finish(){
        this.filesService.onItemUpdate.next(event)
        this.close();
    }



    getFile(fileId): Subscription {
        const queryParams = {
            paginated: false,
            id: fileId,
            relationships: 'menus'

        };

        return this.filesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {

                    this.file = data;



                }
            );

    }




}
