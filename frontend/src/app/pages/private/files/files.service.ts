import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../shared/_services/abstract/default.service";

export class FilesService extends DefaultService {

    link: any;
    filters: any = {};

    onItemUpdate: BehaviorSubject<any>;
    onItemSelection: Subject<any>;
    onBreadcrumbSelection: Subject<any>;
    onSearchTextChanged: Subject<any>;
    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('files');
        this.onItemUpdate = new BehaviorSubject([]);
        this.onItemSelection = new Subject();
        this.onBreadcrumbSelection = new Subject();
        this.onSearchTextChanged = new Subject();

    }



}
