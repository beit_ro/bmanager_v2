import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FilesService} from '../files.service';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {Overlay} from '@angular/cdk/overlay';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../../shared/_services/shared-routes.service';

import {RenameFileComponent} from '../rename-file/rename-file.component';
import {environment} from '../../../../../environments/environment';
import {SharedUserService} from '../../../../shared/_services/shareduser.service';

@Component({
    selector: 'folder-list',
    templateUrl: './folder-list.component.html',
    styleUrls: ['./folder-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FolderListComponent extends DefaultComponent implements OnInit, OnDestroy {
    dialogContent: TemplateRef<any>;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    files: any;
    displayedColumns = ['icon', 'name', 'extension', 'owner_id', 'fileSize', 'updated_at', 'download', 'detail-button', 'action'];
    selected: any;
    filters: any = {};
    endpoint: any = environment.endpoint;
    data: any;
    user: any = {};


    // Private
    private _unsubscribeAll: Subject<any>;
    dataSource: {};

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param filesService
     * @param _matDialog
     * @param userService
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private filesService: FilesService,
        public _matDialog: MatDialog,
        private userService: SharedUserService,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.getUser();
        //this.getGmxCodeLists();
        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 6
        }
        this.getFiles();

        this.filesService.onItemUpdate
            .subscribe(file => {
                this.getFiles();
            });

        this.filesService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getFiles();
            });


    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getFiles();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    getFiles() {

        const queryParams = {
            ...this.filters,
            parent_id: 1,
            type: 'folder',
            relationships: 'creator'
        };

        return this.filesService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.files = data;
                    this.dataSource = new MatTableDataSource(this.files.data);

                }
            );
    }


    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On select
     *
     * @param selected
     */
    onSelect(selected): void {
        this.filesService.onItemSelection.next(selected);
        // console.log('select', selected)
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    /**
     * On select
     *
     * @param selected
     */
    downloadFile(selected): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });
        this.confirmDialogRef.componentInstance.confirmMessage = 'Compression process will start. You will receive ' +
            'and email with the download link in a few minutes once it finishes. Please confirm you want to proceed!';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.log('download begin')

            }

        });
    }


    deleteFile(fileId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: fileId
                };
                return this.filesService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getFiles();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


    select(selected): void {
        this.filesService.onItemSelection.next(selected);


    }



    editFolder(fileId) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(RenameFileComponent, {
            panelClass: 'file-dialog',
            minWidth: '40vw',
            data: {
                fileId: fileId ? fileId : null,
                title: 'Edit folder'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getFiles();
            });
    }



    getUser() {
        return this.userService
            .getMe()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.user = data;
                    //console.log(this.user)

                }
            );
    }

}

