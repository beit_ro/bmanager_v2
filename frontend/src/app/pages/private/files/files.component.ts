import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FormControl} from '@angular/forms';
import {DefaultComponent} from '../../../shared/_components/default.component';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedRoutesService} from '../../../shared/_services/shared-routes.service';
import {MatDialog} from '@angular/material/dialog';
import {FilesService} from './files.service';
import {ManageFileComponent} from './manage-file/manage-file.component';
import {ManageFolderComponent} from './manage-folder/manage-folder.component';


@Component({
    selector: 'files',
    templateUrl: './files.component.html',
    styleUrls: ['./files.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class FilesComponent extends DefaultComponent implements OnInit, OnDestroy {
    selected: boolean = false;
    pathArr: any = [];
    dialogRef: any;
    searchInput: FormControl;
    folder: any;
    data: any;

    /**
     * Constructor
     *
     * @param {FuseSidebarService} _fuseSidebarService
     * @param filesService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private _fuseSidebarService: FuseSidebarService,
        private filesService: FilesService,
        private _matDialog: MatDialog,

        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

// -----------------------------------------------------------------------------------------------------
// @ Lifecycle hooks
// -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {


        this.pathArr = [];
        this.filesService.onItemSelection
            .subscribe(folder => {
                console.log('folder',folder);
                if (!this.pathArr.includes(folder)) {
                    this.pathArr.push(folder);
                }

                console.log('nav', this.pathArr);
                if (folder.id) {
                    this.selected = true;
                    this.folder = folder;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy() {

    }

// -----------------------------------------------------------------------------------------------------
// @ Public methods
// -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    search(): void {
        this.filesService.onSearchTextChanged.next(this.searchInput.value);
    }


    /**
     * New file
     */
    newFile(): void {
        this.dialogRef = this._matDialog.open(ManageFileComponent, {
            panelClass: 'file-dialog',
            minWidth: '40%',
            minHeight: '70%',
            data: {
                folder: this.folder,
                title: 'New file'
            }
        });
    }

    /**
     * New file
     */
    newFolder(): void {
        this.dialogRef = this._matDialog.open(ManageFolderComponent, {
            panelClass: 'folder-dialog',
            minWidth: '40%',
            minHeight: '70%',
            data: {
                folder: this.folder,
                title: 'New Folder'
            }
        });
    }

    backClicked(i, folder) {
        if (this.pathArr.length > i) {
            this.pathArr.splice(i + 1, this.pathArr.length - i + 1);
            this.filesService.onBreadcrumbSelection.next(folder);
            this.folder = folder;
            console.log('back_option', folder)

        }


    }

    homeClicked() {

        this.pathArr = [];
        this.selected = false;
        console.log('aa',this.selected);

    }



}
