import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {PermissionsService} from "./permissions.service";
import {ManagePermissionComponent} from "./manage-permission/manage-permission.component";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";


@Component({
    selector: 'permissions',
    templateUrl: './permissions.component.html',
    styleUrls: ['./permissions.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class PermissionsComponent extends DefaultComponent implements OnInit, OnDestroy {
    dialogRef: any;
    searchInput: FormControl;



    /**
     *
     * @param permissionsService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     */
    constructor(
        private permissionsService: PermissionsService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.permissionsService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New permission
     */
    newPermission(): void {
        this.dialogRef = this._matDialog.open(ManagePermissionComponent, {
            panelClass: 'permission-dialog',
            minWidth: '40%',
            minHeight: '70%',
            data: {
                title: 'New permission'
            }
        });
    }

}
