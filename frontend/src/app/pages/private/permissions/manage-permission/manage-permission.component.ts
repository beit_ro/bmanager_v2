import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {FuseConfirmDialogComponent} from '../../../../../@fuse/components/confirm-dialog/confirm-dialog.component';
import {PermissionsService} from "../permissions.service";
import {ClassesService} from "../classes.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NavigationsService} from "../../navigation/navigations.service";
import {SharedNavigationService} from "../../../../shared/_services/sharednavigation.service";
import {RoutesService} from "../../navigation/manage-routes/routes.service";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-permission',
    templateUrl: './manage-permission.component.html',
    styleUrls: ['./manage-permission.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManagePermissionComponent extends DefaultComponent implements OnInit {

    permission: any = {};
    users: any = [];
    classes: any = [];
    routes: any = [];
    menus: any = [];
    permissionId: number;
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    constructor(
        public matDialogRef: MatDialogRef<ManagePermissionComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private permissionsService: PermissionsService,
        public _matDialog: MatDialog,
        private classesService: ClassesService,
        private routesService: RoutesService,
        private navigationsService: NavigationsService,
        private sharedNavigationService: SharedNavigationService,

        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    ngOnInit(): void {
        this.getClasses();
        this.permissionId = this._data.permissionId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Permission';
        }

        if (this.permissionId) {
            this.getPermission(this.permissionId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getClasses() {

        const queryParams = {
        };
        return this.classesService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.classes = data;
                    this.getMenus();
                }
            );
    }




    getMenus() {
        const queryParams = {
            paginated: false,
        };
        return this.navigationsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {

                    this.menus = data;

                   // console.log('aaa',this.menus);

                }
            );
    }





    getPermission(permissionId): Subscription {
        const queryParams = {
            paginated: false,
            id: permissionId,
            relationships: 'menus'

        };

        return this.permissionsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {

                    this.permission = data;



                }
            );

    }


    savePermission(): void {

       // console.log('permission',this.permission);
        const queryParams: any = {


            ...this.permission
        };

        const service = this.permission.id ?
            this.permissionsService.update(queryParams) : this.permissionsService.create(queryParams);

        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.permission = data;
                    this.sharedNavigationService.onItemUpdate.next(data)
                    this.permissionsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
