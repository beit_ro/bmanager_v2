import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Overlay} from '@angular/cdk/overlay';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {PermissionsService} from "../permissions.service";
import {ManagePermissionComponent} from "../manage-permission/manage-permission.component";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'permissions-list',
    templateUrl: './permissions-list.component.html',
    styleUrls: ['./permissions-list.component.scss'],
    animations: fuseAnimations
})


export class PermissionListComponent extends DefaultComponent implements OnInit, OnDestroy {

    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    permissions: any = [];
    dataSource: {};
    filters: any = {};

    displayedColumnsPermissions: string[] = ['id', 'name','create','read','update','delete', 'action'];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    /**
     * Constructor
     *
     * @param {PermissionsService} permissionsService
     * @param {MatDialog} _matDialog
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private permissionsService: PermissionsService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit() {

        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 5
        }
        //this.getPermissions();

        this.permissionsService.onItemUpdate
            .subscribe(permission => {
                this.getPermissions();
            });

        this.permissionsService.onSearchTextChanged
            .subscribe((newdata: any) => {
               // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getPermissions();
            });


    }



    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getPermissions();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    getPermissions() {
        const queryParams = {
            ...this.filters
        };
        return this.permissionsService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.permissions = data;
                    this.dataSource = new MatTableDataSource(this.permissions.data);

                }
            );
    }


    managePermission(permissionId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManagePermissionComponent, {
            panelClass: 'permission-dialog',
            minWidth: '40vw',
            data: {
                permissionId: permissionId ? permissionId : null,
                title: 'Edit permission'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getPermissions();
            });
    }

    deletePermission(permissionId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: permissionId
                };
                return this.permissionsService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getPermissions();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


}
