import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {InvoicesService} from "../invoices.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-invoice',
    templateUrl: './manage-invoice.component.html',
    styleUrls: ['./manage-invoice.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageInvoiceComponent extends DefaultComponent implements OnInit {

    invoice: any = {};
    users: any = [];
    invoiceId: number;
    invoiceParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageInvoiceComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private invoicesService: InvoicesService,
        public _matDialog: MatDialog,
        private invoiceservice: InvoicesService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.invoiceId = this._data.invoiceId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Invoice';
        }
        if (this.invoiceId) {
            this.getInvoice(this.invoiceId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getInvoice(invoiceId): Subscription {
        const queryParams = {
            id: invoiceId
        };

        return this.invoicesService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.invoice = data;
                }
            );

    }




    saveInvoice(): void {

        const queryParams: any = {
            ...this.invoice
        };

        const service = this.invoice.id ?
            this.invoicesService.update(queryParams) : this.invoicesService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.invoice = data;
                    this.invoicesService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
