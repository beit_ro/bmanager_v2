import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';

import {Overlay} from '@angular/cdk/overlay';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {InvoicesService} from "../invoices.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {ManageInvoiceComponent} from '../manage-invoice/manage-invoice.component';


@Component({
    selector: 'invoices-list',
    templateUrl: './invoices-list.component.html',
    styleUrls: ['./invoices-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})


export class InvoicesListComponent extends DefaultComponent implements OnInit, OnDestroy {

    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    invoices: any = [];
    dataSource: {};
    filters: any = {};

    displayedColumnsInvoices: string[] = ['id', 'name', 'description', 'action'];
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;


    /**
     * Constructor
     *
     * @param {InvoicesService} invoicesService
     * @param {MatDialog} _matDialog
     * @param overlay
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private invoicesService: InvoicesService,
        public _matDialog: MatDialog,
        private overlay: Overlay,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit() {


        this.dataSource = new MatTableDataSource();
        this.filters = {
            per_page: 5
        }


        this.getInvoices();


        this.invoicesService.onItemUpdate
            .subscribe(invoice => {
                this.getInvoices();
            });


        this.invoicesService.onSearchTextChanged
            .subscribe((newdata: any) => {
                // console.log(this.filters, 'filters');
                this.filters.q = newdata;
                this.getInvoices();
            });


    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getInvoices();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit tenant
     *
     * @param tenant
     */



    getInvoices() {

        const queryParams = {
            ...this.filters
        };

        return this.invoicesService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.invoices = data;
                    this.dataSource = new MatTableDataSource(this.invoices.data);

                }
            );
    }


    manageInvoice(invoiceId?) {
        const scrollStrategy = this.overlay.scrollStrategies.reposition();
        const dialogRef = this._matDialog.open(ManageInvoiceComponent, {
            panelClass: 'invoice-dialog',
            minWidth: '40vw',
            data: {
                invoiceId: invoiceId ? invoiceId : null,
                title: 'Edit invoice'
            },
            autoFocus: false,
            scrollStrategy

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {
                this.getInvoices();
            });
    }

    deleteInvoice(invoiceId) {

        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const queryParams = {
                    id: invoiceId
                };
                return this.invoicesService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getInvoices();
                    });

            }
            this.confirmDialogRef = null;
        });


    }


}
