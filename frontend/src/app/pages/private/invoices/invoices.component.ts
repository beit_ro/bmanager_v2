import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, filter, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {InvoicesService} from "./invoices.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../shared/_components/default.component";
import {SharedRoutesService} from "../../../shared/_services/shared-routes.service";
import {ManageInvoiceComponent} from './manage-invoice/manage-invoice.component';


@Component({
    selector     : 'invoices',
    templateUrl  : './invoices.component.html',
    styleUrls    : ['./invoices.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class InvoicesComponent extends DefaultComponent implements OnInit, OnDestroy
{
    dialogRef: any;
    searchInput: FormControl;

    /**
     *
     * @param invoicesService
     * @param _fuseSidebarService
     * @param _matDialog
     * @param router
     * @param activatedRoute
     * @param sharedRoutesService
     */
    constructor(
        private invoicesService: InvoicesService,
        private _fuseSidebarService: FuseSidebarService,
        private _matDialog: MatDialog,
         router: Router,
         activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);

        // Set the defaults
        this.searchInput = new FormControl('');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
       // console.log('rights',this.rights)

        this.searchInput.valueChanges
            .pipe(
                takeUntil(this.ngUnsubscribe),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
            });


    }



    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    search(): void {
        this.invoicesService.onSearchTextChanged.next(this.searchInput.value);
    }

    /**
     * New invoice
     */
    newInvoice(): void
    {
        this.dialogRef = this._matDialog.open(ManageInvoiceComponent, {
            panelClass: 'invoice-dialog',
            minWidth: '40vw',
            data      : {
                title: 'New invoice'
            }
        });
    }

}
