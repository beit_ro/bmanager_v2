import {Component, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {fuseAnimations} from "../../../../../@fuse/animations";
import {CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {PermissionsService} from "../../permissions/permissions.service";
import {takeUntil} from "rxjs/operators";
import {ActivatedRoute, Router} from "@angular/router";
import {DefaultComponent} from "../../../../shared/_components/default.component";
import {SharedUserService} from "../../../../shared/_services/shareduser.service";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";
import {UserRightsService} from "./user-rights.service";






@Component({
    selector     : 'user-right',
    templateUrl  : './user-right.component.html',
    styleUrls    : ['./user-right.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class UserRightComponent extends DefaultComponent  implements OnInit
{

    action: string;
    rights: any = [];
    userId: number;
    userRights: any = [];
    userForm: FormGroup;
    dialogTitle: string;
    saveData: any ={};

    /**
     * Constructor
     *
     * @param matDialogRef
     * @param _data
     * @param permissionsService
     * @param userService
     * @param userRightsService
     * @param router
     * @param activatedRoute
     * @param _formBuilder
     * @param sharedRoutesService
     */
    constructor(
        public matDialogRef: MatDialogRef<UserRightComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private permissionsService: PermissionsService,
        private userService: SharedUserService,
        private userRightsService: UserRightsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
    }
    ngOnInit(): void {

        this.userId = this._data.userId;
        this.getExistingRights();


    }


    addRights(event: CdkDragDrop<string[]>) {

        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {

            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            var moveditem= JSON.parse(JSON.stringify(event.container.data[event.currentIndex]));

            this.saveData.right_id = moveditem.id;
            this.saveData.user_id = this.userId;

            const queryParams = {
            ...this.saveData
            };
            return this.userRightsService
                .create(queryParams)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe((data) => {

                });

        }



    }


    removeRights(event: CdkDragDrop<string[]>) {

        if (event.previousContainer === event.container) {
           moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {

            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
                var moveditem = JSON.parse(JSON.stringify(event.container.data[event.currentIndex]));

                const queryParams = {
                    right_id: moveditem.id,
                    user_id:this.userId
                };
                return this.userRightsService
                    .removeRight(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {

                    });

        }

    }


    getUniquePermissions() {

        const queryParams = {
            user_id : this.userId
        };

        return this.permissionsService
            .uniqueList(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.rights = data;

                }
            );
    }



    getExistingRights() {
        const queryParams = {
            pagination: false,
            id : this.userId
        };

        this.userService
            .getRights(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (right: any) => {
                    this.userRights = right;
                    this.getUniquePermissions();
                });



    }

}
