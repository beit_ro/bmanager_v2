import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../../shared/_services/abstract/default.service";

export class UserRightsService extends DefaultService {

    link: any;
    filters: any = {};

    onUserUpdate: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;


    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('user_rights');
        this.onUserUpdate = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();

    }


    uniqueList(queryParams?): Observable<any> {
        const httpParams = new HttpParams({fromObject: queryParams});
        return this.http.get(this.endpoint + this.resourceLink + '/unique', {params: httpParams});
    }


    removeRight(queryParams): Observable<Object> {
        const httpParams = new HttpParams({fromObject: queryParams});
        return this.http.get(this.endpoint +  'rights/removeRight', {params: httpParams});    }
}
