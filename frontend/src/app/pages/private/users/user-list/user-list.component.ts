import {Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {DefaultComponent} from "../../../../shared/_components/default.component";
import {UsersService} from "../users.service";
import {ManageUserComponent} from "../manage-user/manage-user.component";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedUserService} from "../../../../shared/_services/shareduser.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ManagePermissionComponent} from "../../permissions/manage-permission/manage-permission.component";
import {UserRightComponent} from "../user-right/user-right.component";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class UserListComponent extends DefaultComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent', {static: false})
    dialogContent: TemplateRef<any>;

    users: any;
    user: any;
    filters: any = {};
    action: string;

    dataSource: {};

    displayedColumns = ['avatar','status','name', 'email', 'phone', 'address', 'action'];
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param _usersService
     * @param {MatDialog} _matDialog
     * @param router
     * @param activatedRoute
     * @param userService
     * @param sanitizer
     * @param sharedRoutesService
     */
    constructor(
        private _usersService: UsersService,
        public _matDialog: MatDialog,
        router: Router,
        activatedRoute: ActivatedRoute,
        private userService: SharedUserService,
        private sanitizer: DomSanitizer,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);



        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit(): void {

        //this.getUsers();

        this._usersService.onUserUpdate
            .subscribe(user => {
                this.getUsers();
            });

        this._usersService.onSearchTextChanged
            .subscribe((newdata: any) => {
                this.filters.q = newdata;
                this.getUsers();
            });


        this.dataSource = new MatTableDataSource();


    }


    getUsers() {
        const queryParams = {
            ...this.filters
        };

        this._usersService.list(queryParams)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
                (data: any) => {
                    this.users = data;
                    this.dataSource = new MatTableDataSource(this.users.data);

                }
            );

    }




    getAvatar(avatar) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(avatar);
    }


    /**
     * On Page change
     * Paginator
     */
    onPageChange(event): void {
        this.filters.page = event.pageIndex + 1;
        this.filters.per_page = event.pageSize;
        this.getUsers();
    }



    addUserRight(userId?) {
        const dialogRef = this._matDialog.open(UserRightComponent, {
            panelClass: 'manage-user-right',
            minWidth: '40vw',
            data: {
                userId: userId ? userId : null,
                title: 'Add Right'
            },
            autoFocus: false,

        });

        return dialogRef
            .afterClosed()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe((result) => {

            });
    }




    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Edit user
     *
     * @param user
     */




    editUser(user): void {
        this.dialogRef = this._matDialog.open(ManageUserComponent, {
            panelClass: 'manage-user-dialog',
            data: {
                user: user,
                action: 'edit'
            }
        });

        this.dialogRef.afterClosed()
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];

                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.updateUser(formData.getRawValue());

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':

                        this.deleteUser(user);
                        // TODO on backend disable also user on user deletion

                        break;
                }
            });
    }


    /**
     * Add User
     */


    addUser(user) {
        const queryParams: any = {
            ...user
        };


        this._usersService
            .create(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {

                    this.getUsers();


                }
            );


    }

    /**
     * Update User
     */


    updateUser(user) {
        const queryParams: any = {
            ...user
        };

        this._usersService
            .update(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.getUsers();
                }
            );


    }

    /**
     * Delete User
     */



    deleteUser(userId): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            console.log(result);

            if (result) {
                const queryParams = {
                    id: userId
                };
                return this._usersService
                    .remove(queryParams)
                    .pipe(takeUntil(this.ngUnsubscribe))
                    .subscribe((data) => {
                        this.getUsers();
                    });

            }
            this.confirmDialogRef = null;
        });

    }



}
