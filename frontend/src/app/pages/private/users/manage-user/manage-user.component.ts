import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {User} from "../user.model";



@Component({
    selector     : 'manage-user',
    templateUrl  : './manage-user.component.html',
    styleUrls    : ['./manage-user.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ManageUserComponent
{
    action: string;
    user: User;
    userForm: FormGroup;
    dialogTitle: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<manageUserComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ManageUserComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    )
    {
        // Set the defaults
        this.action = _data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Edit user';
            this.user = _data.user;
        }
        else
        {
            this.dialogTitle = 'New user';
            this.user = new User({});
        }

        this.userForm = this.createUserForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create user form
     *
     * @returns {FormGroup}
     */
    createUserForm(): FormGroup
    {
        return this._formBuilder.group({
            id      : [this.user.id],
            name    : [this.user.name],
            status    : [this.user.status],
            avatar  : '',
            email   : [this.user.email],
            password   : [this.user.password],
            phone   : [this.user.phone],
            address : [this.user.address],
            notes   : [this.user.notes]
        });
    }
}
