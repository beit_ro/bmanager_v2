import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {DefaultService} from "../../../shared/_services/abstract/default.service";

export class UsersService extends DefaultService {

    link: any;
    filters: any = {};
    IsfilterSelected: boolean;

    onUserUpdate: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;


    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('persons');
        this.onUserUpdate = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();

    }

    list(queryParams?): Observable<any> {
        const httpParams = new HttpParams({fromObject: queryParams});
        return this.http.get(this.endpoint + this.resourceLink, {params: httpParams});
    }

    /**
     * Add Person
     */

    addUser(user): void {
        const queryParams: any = {
            ...user
        };
        this.create(queryParams)
            .subscribe(
                (data: any) => {
                    this.onUserUpdate.next(data);
                }
            );


    }

}
