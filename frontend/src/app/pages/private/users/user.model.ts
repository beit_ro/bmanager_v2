import { FuseUtils } from '@fuse/utils';

export class User
{
    id: string;
    name: string;
    lastName: string;
    user_id: number;
    avatar: string;
    nickname: string;
    company: string;
    jobTitle: string;
    email: string;
    password: string;
    phone: string;
    address: string;
    notes: string;
    status: string;

    /**
     * Constructor
     *
     * @param user
     */
    constructor(user)
    {
        {
            this.id = user.id || null;
            this.name = user.name || '';
            this.lastName = user.lastName || '';
            this.user_id = user.user_id;
            this.avatar = user.avatar || 'assets/images/avatars/profile.jpg';
            this.nickname = user.nickname || '';
            this.company = user.company || '';
            this.jobTitle = user.jobTitle || '';
            this.email = user.email || '';
            this.password = user.password || '';
            this.phone = user.phone || '';
            this.address = user.address || '';
            this.notes = user.notes || '';
            this.status = user.status || '';
        }
    }
}
