import {Component, Inject, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator} from '@angular/material';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {takeUntil} from 'rxjs/operators';
import {DefaultComponent} from 'app/shared/_components/default.component';

import {Subscription} from 'rxjs';
import {SettingsService} from "../settings.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";


@Component({
    selector: 'manage-setting',
    templateUrl: './manage-setting.component.html',
    styleUrls: ['./manage-setting.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ManageSettingComponent extends DefaultComponent implements OnInit {

    setting: any = {};
    users: any = [];
    settingId: number;
    settingParents: any = [];
    dialogTitle: string;


    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

    constructor(
        public matDialogRef: MatDialogRef<ManageSettingComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private settingsService: SettingsService,
        public _matDialog: MatDialog,
        private settingservice: SettingsService,
        router: Router,
        activatedRoute: ActivatedRoute,
        sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);


    }

    ngOnInit(): void {
        this.settingId = this._data.settingId;
        if (this._data.title) {
            this.dialogTitle = this._data.title;
        } else {
            this.dialogTitle = 'New Setting';
        }
        if (this.settingId) {
            this.getSetting(this.settingId);
        }

    }


    close(): void {
        this.matDialogRef.close();
    }


    getSetting(settingId): Subscription {
        const queryParams = {
            id: settingId
        };

        return this.settingsService
            .getOne(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.setting = data;
                }
            );

    }




    saveSetting(): void {

        const queryParams: any = {
            ...this.setting
        };

        const service = this.setting.id ?
            this.settingsService.update(queryParams) : this.settingsService.create(queryParams);
        service.pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data: any) => {
                    this.setting = data;
                    this.settingsService.onItemUpdate.next(data);
                    this.close();
                }
            );

    }


}
