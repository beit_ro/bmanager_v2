import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import {takeUntil} from "rxjs/operators";
import { MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import {DefaultComponent} from '../../../../shared/_components/default.component';
import {TokenStorage} from '../../../../shared/_services/token-storage.service';
import {environment} from '../../../../../environments/environment';
import {AuthenticationService} from '../../../../shared/_services/authentication.service';
import {SharedUserService} from "../../../../shared/_services/shareduser.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {SharedRoutesService} from "../../../../shared/_services/shared-routes.service";



export const MY_FORMATS = {
    parse: {
      dateInput: 'LL',
    },
    display: {
      dateInput: 'YYYY-MM-DD',
      monthYearLabel: 'YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'YYYY',
    },
};

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: [ './user-profile.component.scss' ],
    providers: [{provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},],
    encapsulation: ViewEncapsulation.None,
    animations : fuseAnimations
})
export class UserProfileComponent extends DefaultComponent implements OnInit {
    genders: string[] = ['M','F','OTHER'];
    date: any;
    user: any = {};
    userName: any;
    endpoint: string;
    avatarChanged = false;
    newAvatarFile: File;
    newAvatarPreview: any;
    password: any = {};
    dataSource: any = [];
    gdprColumns = ['infoType','data','purpose'];


    constructor(private toasterService: ToasterService,
                private usersService: SharedUserService,
                private tokenStorage: TokenStorage,
                private autheService: AuthenticationService,
                private sanitizer: DomSanitizer,
                router: Router,
                activatedRoute: ActivatedRoute,
                sharedRoutesService: SharedRoutesService
    ) {
        super(router, activatedRoute, sharedRoutesService);
        this.endpoint = environment.endpoint;
    }

    ngOnInit() {
        this.getUser();
    }

    getUser() {
        return this.usersService
            .getMe()
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {

                    this.user = data;
                    this.dataSource = [
                        {
                            infoType: 'Name',
                            data: this.user.name,
                            purpose: 'Info'
                        },
                        {
                            infoType: 'Address',
                            data: this.user.address,
                            purpose: 'Info'
                        },{
                            infoType: 'Phone',
                            data: this.user.phone,
                            purpose: 'Info'
                        }
                    ];

                }
            );
    }


    onUpdateProfile() {
        const queryParams = this.user;

        this.usersService
            .update(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.user = data;
                    this.usersService.onUserUpdate.next(data)
                    this.user.avatar = this.sanitizer.bypassSecurityTrustResourceUrl( this.user.avatar)

                }
            );
                           
        this.userName = this.user.name;
    }

    onUpdatePass() {
        const queryParams = this.password;
        this.autheService
            .changePassword(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                }
            );

        this.userName = this.user.name;
    }

    onUpdateAvatar() {
        const queryParams = {
            id: this.user.id,
            new_avatar: this.newAvatarFile
        };
        this.usersService
            .setAvatar(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {
                    this.usersService.onUserUpdate.next(data)

                }
            );

    }

    fileChangeListener($event) {
        const image: any = new Image();
        const file: File = $event.target.files[ 0 ];
        const myReader: FileReader = new FileReader();
        myReader.onloadend = (loadEvent: any) => {
            image.src = loadEvent.target.result;
            this.newAvatarPreview = image.src;
            this.avatarChanged = true;
        };
        myReader.readAsDataURL(file);
        this.newAvatarFile = file;
        this.onUpdateAvatar();
    }





}

