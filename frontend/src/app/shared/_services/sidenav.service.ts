import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

export class SidenavService {
  private sideNav: MatSidenav;
  constructor() { }


  public setSidenav(sideNav: MatSidenav) {
    this.sideNav = sideNav;
  }

  public open() {
    return this.sideNav.open();
  }


  public close() {
    return this.sideNav.close();
  }

  public toggle(): void {
    this.sideNav.toggle();
  }
}





