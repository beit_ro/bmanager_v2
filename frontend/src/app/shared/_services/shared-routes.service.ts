import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToasterService } from 'angular2-toaster';
import {DefaultService} from './abstract/default.service';
import {BehaviorSubject} from "rxjs";


export class SharedRoutesService extends DefaultService {

    onItemUpdate: BehaviorSubject<any>;

    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('navigation/rights');
        this.onItemUpdate = new BehaviorSubject([]);
    }

}
