import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpEventType, HttpParams, HttpRequest} from '@angular/common/http';
import { BaseService } from './base.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/finally';
import {ToasterService} from "angular2-toaster";

export abstract class DefaultService extends BaseService {



    constructor(public http: HttpClient,public toasterService : ToasterService
                ) {
        super();
    }

    create(queryParams): Observable <any>  {
        return this.http.post(this.endpoint + this.resourceLink, queryParams)
            .catch(this.onCatch)
            .do((res: Response) => {
                this.onSuccess(res);
            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });

    }


    upload(queryParams): Observable <any>  {
        return this.http.post(this.endpoint + this.resourceLink, queryParams,{reportProgress: true, observe: 'events'})
            .catch(this.onCatch)
            .do((res: Response) => {

            }, (error: any) => {
                //this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });

    }

    update(queryParams) {
        const oneId = queryParams.id;
        delete queryParams.id;
        return this.http.put(this.endpoint + this.resourceLink + '/' + oneId, queryParams)
            .do(
                () => {
                    this.toasterService.pop('success', 'Action done with success!');
                }
            );
    }

    list(queryParams?): Observable<any> {
        const httpParams = new HttpParams({ fromObject: queryParams });
        return this.http.get(this.endpoint + this.resourceLink, { params: httpParams });
    }

    getOne(queryParams) {
        const oneId = queryParams.id;
        delete queryParams.id;
        const httpParams = new HttpParams({ fromObject: queryParams });
        return this.http.get(this.endpoint + this.resourceLink + '/' + oneId, { params: httpParams });
    }

    remove(queryParams) {
        return this.http.delete(this.endpoint + this.resourceLink + '/' + queryParams.id)
            .do(
                () => {
                    this.toasterService.pop('success', 'Action done with success!');
                }
            );
    }


    private onCatch(error: any, caught: Observable<any>): Observable<any> {
        return Observable.throw(error);
    }

    private onSuccess(res: Response): void {
        this.toasterService.pop('success', 'Action done with success!');
    }

    private onError(res: Response): void {

        this.toasterService.pop( 'warning, status code'+ res.status, 'Error');
    }

    private onEnd(): void {


    }




}
