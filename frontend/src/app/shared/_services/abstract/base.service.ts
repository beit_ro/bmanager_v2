import {environment} from "../../../../environments/environment";


export abstract class BaseService {

    resourceLink: string;
    endpoint = environment.endpoint;


    public setResourceLink(resourceLink) {
        this.resourceLink = resourceLink;
        return this;
    }


}