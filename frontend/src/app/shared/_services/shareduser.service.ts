import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToasterService } from 'angular2-toaster';
import {tap} from 'rxjs/operators';
import {DefaultService} from './abstract/default.service';
import {BehaviorSubject, Observable} from "rxjs";


export class SharedUserService extends DefaultService {

    avatar: any;

    onUserUpdate: BehaviorSubject<any>;
    constructor(public http: HttpClient,
                public toasterService: ToasterService) {
        super(http, toasterService);
        this.setResourceLink('users');
        this.onUserUpdate = new BehaviorSubject([]);

    }



    setAvatar(queryParams) {
        const formData: FormData = new FormData();
        formData.append('avatar', queryParams.new_avatar, queryParams.new_avatar.name);

        return this.http.post(this.endpoint + this.resourceLink + '/' + queryParams.id + '/post_avatar', formData)
            .pipe(
                tap(
                () => {
                    this.toasterService.pop('success', 'Action done with success!');
                }
            ));
    }


    getMe() {

        return this.http.get(this.endpoint + this.resourceLink + '/me');

    }


    getAvatar(): Observable<Blob> {
        return this.http.get(this.endpoint + 'user/get_avatar', { responseType: 'blob' });
    }


    getRights(queryParams) {
        return this.http.get(this.endpoint + 'users/' + queryParams.id + '/rights');
    }







}
