import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import {Observable, of} from "rxjs";
import {User} from "../_models/User";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";


export class TokenStorage {

    private endpoint = environment.endpoint;

    constructor(private socketService: Socket,
                private http: HttpClient) {
    }

    /**
     * Set access token
     * @returns {TokenStorage}
     */
    public setAccessToken(token: string): TokenStorage {
        localStorage.setItem('auth-token', token);

        return this;
    }

    public sendTokenToSocketIo() {
        const token: string = <string>localStorage.getItem('auth-token');
        this.socketService.emit('subscribe-to-channel', { channel: token });
        return this;
    }

    /**
     * Get access token
     * @returns {Observable<string>}
     */
    public getAccessToken(): Observable<string> {
        const token: string = <string>localStorage.getItem('auth-token');
        return of(token);
    }

    /**
     * Set logged user
     * @returns {TokenStorage}
     */
    public setLoggedUser(user: User): TokenStorage {
        localStorage.setItem('user', JSON.stringify(user));
        return this;
    }

    /**
     * Get logged user
     * @returns {Observable<string>}
     */
    public getLoggedUser(): Observable<User> {
        const user: User = <User>JSON.parse(localStorage.getItem('user'));
        return of(user);
    }

    /**
     * Set refresh token
     * @returns {TokenStorage}
     */
    public setRefreshToken(token: string): TokenStorage {
        localStorage.setItem('refreshToken', token);

        return this;
    }

    /**
     * Get refresh token
     * @returns {Observable<string>}
     */
    public getRefreshToken(): Observable<string> {
        const token: string = <string>localStorage.getItem('refreshToken');
        return of(token);
    }

    /**
     * Remove tokens
     */
    public clear() {
        localStorage.removeItem('auth-token');
        localStorage.removeItem('user');
    }

    getUser()
    {
        console.log('user');
        return this.http.get(this.endpoint +'users/me')
            .subscribe((data) => {

                }
            );
    }





}
