import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DataStorageService {
    private localStorageChangeHandler = new Subject<any>();
    constructor() {

    }


    /**
     * Set data in localStorage
     *
     * @param args
     * @returns {this}
     */
    // @ts-ignore
    public setData(...args) {
        // @ts-ignore
        for (let i = 0; i < args.length; i = i + 2) {
            // @ts-ignore
            localStorage.setItem(args[ i ], JSON.stringify(args[ i + 1 ]));
            this.sendLocalStorageChanged(args[ i ]);
        }
        return this;
    }

    /**
     * Get data from local storage
     * @param {string} key
     * @returns {any}
     */
    public getData(key: string) {
        const item = localStorage.getItem(key);

        // Try catch for json parse errors on undefined variables
        try {
            // @ts-ignore
            return JSON.parse(item);
        } catch (e) {
            return undefined;
        }
    }

    /**
     * Remove data from local storage
     *
     * @param keys
     */
    // @ts-ignore
    public removeData(...keys) {
        // @ts-ignore
        for (const key of keys) {
            if (key !== 'auth-token' || key !== 'user') {
                localStorage.removeItem(key);
            } else {
               // this.toasterService.pop('error', 'You can\'t do this action!');
            }
        }
    }


    /**
     * Function that simulates $rootScope.$broadcast from angular 1
     */
    sendLocalStorageChanged(keyValue) {
        this.localStorageChangeHandler.next(keyValue);
    }

    /**
     * Function that simulates $rootScope.$on from angular 1
     */
    getLocalStorageChanged(): Observable<any> {
        return this.localStorageChangeHandler.asObservable();
    }
}
