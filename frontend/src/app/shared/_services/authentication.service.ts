import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from "rxjs";
import {AuthService} from 'ngx-auth';
import {environment} from "../../../environments/environment";
import {TokenStorage} from "./token-storage.service";
import {DataStorageService} from "./data-storage.service";
import {map, tap} from "rxjs/operators";
import {ToasterService} from "angular2-toaster";
import {Socket} from "ngx-socket-io";
import {Session, User} from "../_models";



interface AccessData {
    session: Session;
    user : User;
}


export function getMe(http: HttpClient, tokenStorage: TokenStorage): Observable<any> {
    return http.get(environment.endpoint + 'users/me')
        .pipe(tap(
            (data) => {

                tokenStorage.setLoggedUser(data);
            }
        ));
}


export class AuthenticationService implements AuthService {
    private endpoint = environment.endpoint;
    private resourceLink = 'auth';

    constructor(private http: HttpClient,
                private tokenStorage: TokenStorage,
                private dataStorage: DataStorageService,
                private socketService: Socket,
                private toasterService: ToasterService,
               ) {
    }

    /**
     * Check, if user already authorized.
     * @description Should return Observable with true or false values
     * @returns {Observable<boolean>}
     * @memberOf AuthService
     */
    public isAuthorized(): Observable<boolean> {
        return this.tokenStorage
            .getAccessToken()
            .pipe(
                tap((token) => {
                }),
                map(token => !!token)
            );

    }

    /**
     * Get access token
     * @description Should return access token in Observable from e.g.
     * localStorage
     * @returns {Observable<string>}
     */
    public getAccessToken(): Observable<string> {
        return this.tokenStorage.getAccessToken();
    }

    /**
     * Function, that should perform refresh token verifyTokenRequest
     * @description Should be successfully completed so interceptor
     * can execute pending requests or retry original one
     * @returns {Observable<any>}
     */
    public refreshToken(): Observable<AccessData> {

        const accData: AccessData = {
            session: undefined,
            user: undefined
        };

        return of(accData);
    }

    /**
     * Checks if status is 401
     *
     * @description Essentialy checks status
     * @param {Response} response
     * @returns {boolean}
     */
    public refreshShouldHappen(response: HttpErrorResponse): boolean {
        if (response.status === 401) {
            this.tokenStorage.clear();
            location.reload(true);
        }

        return response.status === 401;
    }

    /**
     * Verify that outgoing request is refresh-token,
     * so interceptor won't intercept this request
     * @param {string} url
     * @returns {boolean}
     */
    public verifyTokenRequest(url: string): boolean {
        // @ts-ignore
        return url.endsWith('/refresh');
    }

    /**
     * EXTRA AUTH METHODS
     */

    /**
     * Register a normal user (customer)
     *
     * @param queryParams
     * @returns {Observable<any>}
     */
    public register(queryParams): Observable<any> {
        return this.http.post(this.endpoint + this.resourceLink + '/register', queryParams)
            .pipe(
                map(
                (data) => {
                    this.toasterService.pop('success', 'Registration success!');
                    return this.login(queryParams);
                }
            ))
            .catch((err) => {
                for (const error in err.error.errors) {
                    if (!err.error.errors.hasOwnProperty(error)) {
                        continue;
                    }

                    for (const message of err.error.errors[error]) {
                       this.toasterService.pop('error', err.error.message, message);

                    }
                }
                return Observable.throw(err);
            });
    }

    /**
     * Log in
     *
     * @param queryParams
     * @returns {Observable<any>}
     */
    public login(queryParams): Observable<any> {
        return this.http.post(this.endpoint + this.resourceLink + '/login', queryParams)
            .pipe(
                tap((tokens: AccessData) => {
                    this.saveAccessData(tokens);
                    this.tokenStorage.sendTokenToSocketIo();

                }, err => {
                    //console.error('aaa', err.error.errors[0]);
                    this.toasterService.pop('error', err.error.errors[0]);
                })
            )

    }

    /**
     * Log out
     */
    public logout(): Observable<any> {


        return this.http.post(this.endpoint + this.resourceLink + '/logout', {})
            .pipe(tap(
                (data) => {

                    const tk: string = <string>localStorage.getItem('auth-token');
                    this.socketService.emit('AUTH_SIGNOUT_SUCCESS', {token: tk});
                    //console.log('logout made')
                    this.tokenStorage.clear();
                    location.reload(true);
                }
            ));
    }


    private saveAccessData(data: AccessData) {
        this.tokenStorage
            .setAccessToken(data.session.token)
            .setLoggedUser(data.user);


    }


    public getHeaders(token: string) {
        return {
            'Auth-Token': token
        };
    }

    public forgotPassword(queryParams) {
        return this.http.post(this.endpoint + this.resourceLink + '/forgot_password', queryParams).pipe(
            tap((tokens: AccessData) => {
                this.toasterService.pop('success', 'Request received!');

            }, err => {
                //console.error('aaa', err.error.errors[0]);
                this.toasterService.pop('error', err.error.errors[0]);
            })
        )
    }

    public changePassword(queryParams) {
        return this.http.post(this.endpoint + this.resourceLink + '/change_reset_password', queryParams)
            .pipe(tap((data) => {
                this.toasterService.pop('success', 'Password changed with success!');
            }));
    }




}
