export class ml {
    public static pick(matrix, col) {
        var column = [];
        for (var i = 0; i < matrix.length; i++) {
            column.push(matrix[i][col]);
        }
        return column;
    }
    public static shuffle(arr, options) {

        if (!Array.isArray(arr)) {
            throw new Error('shuffle expect an array as parameter.');
        }
        options = options || {};

        var collection = arr,
            len = arr.length,
            rng = options.rng || Math.random,
            random,
            temp;

        if (options.copy === true) {
            collection = arr.slice();
        }

        while (len) {
            random = Math.floor(rng() * len);
            len -= 1;
            temp = collection[len];
            collection[len] = collection[random];
            collection[random] = temp;
        }

        return collection;
    };

    public static  _sort(A) {
        let result = []
        let i = 0;
        const iMax = A.length;
        for (; i < iMax; i++) {
            if (result.indexOf(A[i]) < 0) {
                result.push(A[i]);
            }
        }
        return result
    }
    public static _configResponse(sentence) {
        var resp = this._replaceAll(this.random(sentence), '{botname}', null);
        resp = this._replaceAll(resp, '{botversion}', '1.0.beta');
        return resp;
    }
   private static _replaceAll(str, needle, replacement) {
        return str.split(needle).join(replacement);
    }
    public static  _containsInArray(arr, check) {
        var found = false;
        for (var i = 0; i < check.length; i++) {
            if (arr.indexOf(check[i]) > -1) {
                found = true;
                break;
            }
        }
        return found;
    }
    public static removeDups(data) {
        let process = (names) => names.filter((v, i) => names.indexOf(v) === i)
        return process(data)
    }
    public static zeroTest(myArray) {
        var flag = false;
        for (var i = 0; i < myArray.length; ++i) {
            if (myArray[i] !== 0) {
                flag = true;
                break;
            }
        }
        return flag
    }
    public static inArray(key, A) {
        if (A[key] != undefined && A[key] != null && A[key].length > 0) {
            return true;
        }
        return false;
    }
    public static random(A) {
        return A[Math.floor(Math.random() * A.length)]
    }
   public static tokenize ( s ) {
        return s.match(/\S+/g);
    }



}