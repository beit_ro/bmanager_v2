import {Subject} from 'rxjs/Subject';
import {OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SharedRoutesService} from "../_services/shared-routes.service";
import {UsersService} from "../../pages/private/users/users.service";
import {takeUntil} from "rxjs/operators";


export abstract class DefaultComponent implements OnDestroy {
    // internal unSubscriber for observables
    public ngUnsubscribe: Subject<any> = new Subject();
    rights: any = {};
    exception: any = [];
    protected constructor(
        protected router: Router,
        protected activatedRoute: ActivatedRoute,
        protected sharedRoutesService: SharedRoutesService

    ) {
        this.getRouteRights();
    }


    getRouteRights(): any {


        const queryParams = {
            route: this.router.url
        };

        return this.sharedRoutesService
            .list(queryParams)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
                (data) => {

                    if(data){
                        if (data.crud == '0000') {
                            // console.log('data',data.crud);
                            // this.router.navigate(['/private/dashboard']);
                        } else {
                            this.rights.create = data.crud.charAt(0);
                            this.rights.read = data.crud.charAt(1);
                            this.rights.update = data.crud.charAt(2);
                            this.rights.delete = data.crud.charAt(3);
                        }
                        console.log('data',data);
                    }



                }
            );
    }


    ngOnDestroy() {
        // unsubscribe from all the observables
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }


}
