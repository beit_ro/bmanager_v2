import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FacadeService } from "./_services/facade.service";
import { DataStorageService } from "./_services/data-storage.service";
import { TokenStorage } from "./_services/token-storage.service";
import {ToasterService} from "angular2-toaster";
import {SharedUserService} from "./_services/shareduser.service";
import {SharedNavigationService} from "./_services/sharednavigation.service";
import {SharedRoutesService} from "./_services/shared-routes.service";



@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers: [

        FacadeService,
        DataStorageService,
        TokenStorage,
        ToasterService,
        SharedUserService,
        SharedNavigationService,
        SharedRoutesService
    ]
})

export class SharedModule { }
