import Swal from 'sweetalert2';

export function customConfirm(callback) {
    return Swal.fire({
        title: "Are you sure?",
        text: '',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'})
        .then((result) => {
            if (result.value) {
                if (callback) {
                    callback();
                }
            }
        });
}
