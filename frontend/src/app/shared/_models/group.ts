
export class Group
{
    id: string;
    name: string;
    data_start: string;
    data_end: string;
    code: string;
    notes: string;

    /**
     * Constructor
     *
     * @param group
     */
    constructor(group)
    {
        {
            this.id = group.id || null;
            this.name = group.name || '';
            this.code = group.code || 'TU';
            this.data_start = group.data_start || '';
            this.data_end = group.data_end || '';
            this.notes = group.notes || '';
        }
    }
}
