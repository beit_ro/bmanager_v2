export class Person {
    id: string;
    name: string;
    user_id: number;
    avatar: string;
    nickname: string;
    company: string;
    jobTitle: string;
    email: string;
    password: string;
    phone: string;
    address: string;
    notes: string;
    status: string;


    /**
     * Constructor
     *
     * @param person
     */
    constructor(person) {
        {
            this.id = person.id || null;
            this.name = person.name || '';
            this.user_id = person.user_id;
            this.avatar = person.avatar || 'assets/images/avatars/profile.jpg';
            this.nickname = person.nickname || '';
            this.company = person.company || '';
            this.jobTitle = person.jobTitle || '';
            this.email = person.email || '';
            this.password = person.password || '';
            this.phone = person.phone || '';
            this.address = person.address || '';
            this.notes = person.notes || '';
            this.status = person.status || '';
        }
    }



}
