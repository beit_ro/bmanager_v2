export class UserDetail {
    id: number;
    name: string;
    email: string;
    phone: string;
    avatar: string;
    nickname: string;
    company: string;
    jobTitle: string;
    address: string;
    status: string;
    notes: string;
    groups: any[];


    /**
     * Constructor
     *
     * @param person
     */
    constructor(user) {
        {
            this.id = user.user_id || null;
            this.name = user.name || '';
            this.avatar = user.avatar || 'assets/images/avatars/profile.jpg';
            this.nickname = user.nickname || '';
            this.company = user.company || '';
            this.jobTitle = user.jobTitle || '';
            this.email = user.email || '';
            this.phone = user.phone || '';
            this.address = user.address || '';
            this.status = user.status || '';
            this.notes = user.notes || '';
        }
    }
}