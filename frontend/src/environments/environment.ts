// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    hmr: false,
    endpoint: 'http://localhost:8100/api/',
    //endpoint: 'http://198.57.27.236/api/',
    //endpoint: 'http://193.1.101.236/api/',
    //endpoint: 'http://192.168.0.105:8000/api/',
    // uncomment for production
    socket: 'http://com.beit.ro:443',
    //socket: 'http://localhost:6301',
    project: {
        identifier: 'bmanager',
        name: 'BMANAGER'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
