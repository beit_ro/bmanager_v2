export const environment = {
    production: true,
    hmr: false,
    endpoint: 'http://193.1.101.236/api/',
    //endpoint: 'http://localhost:8000/api/',
    //endpoint: 'http://aspenapi.saccess.io/api/',
    //endpoint: 'http://192.168.0.105:8000/api/',
    // uncomment for production
    socket: 'http://com.beit.ro:443',
    //socket: 'http://localhost:6301',
    project: {
        identifier: 'SHOPS',
        name: 'SHOPS'
    }
};
