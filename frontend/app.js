// Get dependencies
var express = require('express');
var path = require('path');
var http = require('http');
var compression = require('compression');
var ENV = require('./env.prod');
var useragent = require('express-useragent');
var fs = require('fs');


var app = express();

/**
 * Ussages
 */

//TO DO  uncoment for ssl certificate

/*app.use(function (req, res, next) {
    var host = req.get('host');
    if (host.indexOf('www.') === -1) {
        host = 'www.' + host;
    }
    if (!req.secure) {
        //FYI this should work for local development as well
        return res.redirect(301, 'https://' + host + req.url);
    }
    return next();
});*/

app.use(compression());
app.use(useragent.express());
// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));


app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', [

        'https://cue.beit.ro',
        'http://localhost'

    ]);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
/*
* Custom routes
*/


app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
var port = ENV.expressPort || '3000';
app.set('port', port);

/**
 /* * Create servers.
 *!/
 var httpsServer = require('https').createServer({
    cert: fs.readFileSync('cert/fullchain.pem'),
    key: fs.readFileSync('cert/private.key')
}, app);*/
var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, function () {
    console.log('Server running on localhost:' + port);
});



