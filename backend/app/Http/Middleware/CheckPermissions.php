<?php

	namespace App\Http\Middleware;

	use Closure;
    use Illuminate\Http\Request;

	class CheckPermissions
	{
		/**
		 * Handle an incoming request.
		 *
		 * @param  Request  $request
		 * @param  Closure  $next
		 * @return mixed
		 */

		// @todo check if this is working corectly

		public function handle($request, Closure $next)
		{
			return auth()->user()->hasPermissionForRoute($request->route())
				? $next($request)
				: redirect()->back()->withErrors(["You don't have access to the requested page."]);

		}
	}

