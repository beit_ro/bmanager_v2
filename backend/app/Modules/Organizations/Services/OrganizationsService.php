<?php


namespace App\Modules\Organizations\Services;


use App\Helpers\EmailHelper;
use App\Helpers\Responser;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Internationalization\Models\Country;
use App\Modules\Organizations\Models\Organization;
use App\Modules\Users\Models\User;
use Illuminate\Support\Facades\Mail;


class OrganizationsService extends DefaultService
{

    private $usersModel;

    public function __construct(Organization $defaultModel, User $usersModel)

    {
        $this->usersModel = $usersModel;
        parent::__construct($defaultModel);
    }



}
