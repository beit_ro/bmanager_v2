<?php

namespace App\Modules\Organizations\Controllers;


use App\Helpers\EmailHelper;
use App\Modules\DefaultController;
use App\Modules\Organizations\Services\OrganizationsService;
use App\Modules\Users\Models\UserDetail;
use Illuminate\Http\Request;


/**
 * Class TenantsController
 * @package App\Modules\Tenants\Controllers
 */
class OrganizationsController extends DefaultController
{

    protected $organizationsService;

		/**
         *
         * @param $organizationsService
         * @param $rightsToCheckFor
         * */

    public function __construct(OrganizationsService $organizationsService)
    {
        $this->organizationsService = $organizationsService;

        parent::__construct($organizationsService);
    }





}
