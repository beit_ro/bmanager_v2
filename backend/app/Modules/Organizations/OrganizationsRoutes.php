<?php


use App\Modules\Organizations\Controllers\OrganizationsController;

Route::group([
    'middleware' => ['auth','right'],
], function () {

    Route::group([
        'prefix' => 'organizations'
    ], function () {

        Route::post('pass/{id}', OrganizationsController::getShortClassName() . '@updatePass');
    });

    Route::resources([
        'organizations' => OrganizationsController::getShortClassName()
    ]);



});


