<?php


namespace App\Modules\Organizations\Models;


use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\User;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\ImageManagerStatic as Image;

class Organization extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organizations';


    use Encryptable;

    protected $encryptable = [

    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organisation_name',
        'address',
        'city',
        'county',
        'country',
        'postal_code',
        'phone',
        'vat_no',
        'director_name',
        'secretary_name',
        'iban',
        'date_inclusion',
        'responsible_id',
        'owner_id'

    ];



    public $searchable = ['organisation_name', 'address', 'city','county',  'country','postal_code', 'phone', 'vat_no', 'director_name'];



    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }


}
