<?php


use App\Modules\Groups\Controllers\GroupsRightsController;
use App\Modules\Groups\Controllers\GroupsController;

Route::group([
    'middleware' => 'auth', 'right'
], function () {

    Route::get('groups/{id}/rights', GroupsRightsController::getShortClassName() . '@groupRights');
    Route::resources([
        'groups' => GroupsController::getShortClassName(),
        'group_rights' => GroupsRightsController::getShortClassName(),

    ]);




});

