<?php

namespace App\Modules\Groups\Models;

use App\Modules\Companies\Models\Company;
use App\Modules\Companies\Models\CompanyGroupSlot;
use App\Modules\Companies\Models\GroupBuildingOffice;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Group extends Model
{
    use SoftDeletes;
    /**PrivateUser
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code','observations','owner_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * @var array
     */
    public $searchable = ['name', 'code','observations'];


    /**
     * Validate the input data
     *
     * @param array $input
     * @return mixed
     */
    public static function validate(array $input)
    {
        $validator = Validator::make($input, [
            'name' => 'required',
            'code' => 'required|unique:groups',
        ]);
        return $validator;
    }


    public function users()
    {
        return $this->belongsToMany(User::class, 'user_groups', 'group_id', 'user_id')
            ->withTimestamps()
            ->whereNull('user_groups.deleted_at');
    }


    public function user_groups()
    {
        return $this->hasMany(UserGroup::class);
    }

    public function rights()
    {
        return $this->hasManyThrough(Right::class, GroupRights::class, 'group_id', 'id', 'id', 'right_id');
    }

}
