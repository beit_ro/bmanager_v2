<?php


namespace App\Modules\Groups\Services;


use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\UserRights;
use Illuminate\Http\Request;


class GroupsService extends DefaultService
{


    public function __construct(Group $defaultModel)

    {
        parent::__construct($defaultModel);
    }

    public function create($params)
    {
       $userId =  UserHelper::getUser();
        $group = parent::create($params);
        $group_id = $group->id;
        if (isset($params['user_groups'])) {
            foreach ($params['user_groups'] as $userGroupData) {
                UserGroup::create([
                    'group_id' => $group_id,
                    'user_id' => $userGroupData['user_id'],
                    'owner_id' => $userId

                ]);
            }
        }
        return Group::with('user_groups.user.details')->find($group->id);
    }




    public function update($id, $params)
    {

        $group = parent::update($id, $params);
        $group_id = $group->id;
        $userId =  UserHelper::getUser();

        $remaining_users = [];

        $userGroupusers = UserGroup::where('group_id', $group->id)->pluck('user_id')->toArray();

        if (isset($params['user_groups'])) {
            $users = $params['user_groups'];
            foreach ($users as $user) {
                array_push($remaining_users, $user['user_id']);
            }

            UserGroup::where('group_id', $group->id)->whereNotIn('user_id', $remaining_users)->delete();



            foreach ($remaining_users as $user) {
                if (!in_array($user, $userGroupusers)) {
                    UserGroup::create([
                        'group_id' => $group->id,
                        'user_id' => $user,
                        'owner_id' => $userId
                    ]);

                }



            }
        }

        return Group::with('user_groups.user.details')->find($group->id);
    }



}
