<?php


namespace App\Modules\Groups\Services;


use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\Right;
use Illuminate\Http\Request;


class GroupRightsService extends DefaultService
{


    public function __construct(GroupRights $defaultModel)

    {
        parent::__construct($defaultModel);
    }


    public function groupRights(Request $request, $id)
    {
        $groupRights = GroupRights::where('group_id', $id)->pluck('right_id');
        $rights = Right::whereIn('id', $groupRights)->get();

        return $rights;
    }


}
