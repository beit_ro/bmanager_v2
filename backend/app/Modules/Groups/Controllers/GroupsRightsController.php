<?php

namespace App\Modules\Groups\Controllers;


use App\Modules\DefaultController;
use App\Modules\Groups\Services\GroupRightsService;
use App\Modules\Groups\Services\GroupsService;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\UserRights;
use Illuminate\Http\Request;


/**
 * Class TenantsController
 * @package App\Modules\Tenants\Controllers
 */
class GroupsRightsController extends DefaultController
{

    protected $groupsService;

		/**
         * UsersController constructor.
         *
         * @param $groupsService
         * */

    public function __construct(GroupRightsService $groupsService)
    {
        $this->groupsService = $groupsService;

        parent::__construct($groupsService);
    }



    public function groupRights(Request $request, $id)
    {
        return $this->groupsService->groupRights($request, $id);
    }




}
