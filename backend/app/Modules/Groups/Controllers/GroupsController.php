<?php

namespace App\Modules\Groups\Controllers;


use App\Modules\DefaultController;
use App\Modules\Groups\Services\GroupsService;


/**
 * Class TenantsController
 * @package App\Modules\Tenants\Controllers
 */
class GroupsController extends DefaultController
{

    protected $groupsService;

		/**
         * UsersController constructor.
         *
         * @param $groupsService
         * @param $rightsToCheckFor
         * */

    public function __construct(GroupsService $groupsService, array $rightsToCheckFor = [])
    {
        $this->groupsService = $groupsService;

        parent::__construct($groupsService, $rightsToCheckFor);
    }








}
