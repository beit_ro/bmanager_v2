<?php

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGroupsRightsTable extends Migration
{

	const TABLE_NAME = 'group_rights';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create(self::TABLE_NAME, function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->biginteger('group_id')->unsigned();
			$table->biginteger('right_id')->unsigned();

			$table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
			$table->foreign('right_id')->references('id')->on('rights')->onDelete('cascade');
            $table->unsignedBigInteger('owner_id');
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop(self::TABLE_NAME);
	}

}
