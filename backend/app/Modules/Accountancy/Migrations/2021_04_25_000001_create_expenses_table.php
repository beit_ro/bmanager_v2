<?php

use App\Helpers\GlobalHelper;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    protected $tableName;

    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->tableName = str_replace("Create", "", get_class($this));
        $this->tableName = str_replace("Table", "", $this->tableName);
        $this->tableName = GlobalHelper::fromCamelCaseToSnakeCase($this->tableName);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {

            //SHARED
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->text('details')->nullable();

            $table->double('amount',2);
            $table->text('vat_rate');
            $table->double('vat_amount',2)->nullable();
            $table->timestamp('date');
            $table->bigInteger('expense_type_id')->unsigned();
            $table->enum('currency',['RON','USD','EUR'])->default('EUR');

            //INVOICES
            $table->string('invoice_number')->nullable();
            $table->bigInteger('provider_id')->unsigned()->nullable();

            //----------------------------------------------------------------------------------









            $table->unsignedBigInteger('owner_id');

            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
            $table->foreign('expense_type_id')->references('id')->on('nomen_expenses_type')->onDelete('cascade');
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }

}
