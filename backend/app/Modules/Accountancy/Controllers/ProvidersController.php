<?php

namespace App\Modules\Accountancy\Controllers;


use App\Modules\DefaultController;
use App\Modules\Nomenclatoare\Services\NomenclatoareService;


class ProvidersController extends DefaultController
{

    public function __construct(NomenclatoareService $defautService)
    {

        parent::__construct($defautService);
    }


}
