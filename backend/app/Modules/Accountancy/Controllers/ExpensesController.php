<?php

namespace App\Modules\Accountancy\Controllers;


use App\Modules\Accountancy\Services\ExpensesService;
use App\Modules\DefaultController;


class ExpensesController extends DefaultController
{

    public function __construct(ExpensesService $defautService)
    {

        parent::__construct($defautService);
    }


}
