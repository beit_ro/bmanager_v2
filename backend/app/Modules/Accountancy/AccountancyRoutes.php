<?php


use App\Modules\Accountancy\Controllers\AccountancyController;
use App\Modules\Accountancy\Controllers\ExpensesController;
use App\Modules\Accountancy\Controllers\ProvidersController;

Route::group([
    //'prefix' => 'accountancy',
    //'middleware' => ['auth','right'],
], function () {

    Route::resources([
        'accountancy' => AccountancyController::getShortClassName(),
        'providers' => ProvidersController::getShortClassName(),
        'expenses' => ExpensesController::getShortClassName()
    ]);



});


