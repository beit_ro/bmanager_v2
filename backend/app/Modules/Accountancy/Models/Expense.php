<?php

namespace App\Modules\Accountancy\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Expense extends Model
{
    use SoftDeletes;

    protected $table = 'expenses';

    protected $fillable = [
        'name',
        'details',
        'number',
        'date',
        'date_due',
        'amount',
        'provider_id',
        'invoice_type_id',
        'invoice_product_id',
        'expense_type_id',
        'user_id'
    ];

    protected $hidden = [
        'pivot', 'deleted_at'
    ];
    protected $appends = [
       'remainingAmount',
        'totalPayment'
    ];


    public function getremainingAmountAttribute()
    {
        $plata = Payment::where('expense_id',$this->id)
        ->sum('sum');
        return $this->remainingAmount($this->amount,$plata);

    }

    public function gettotalPaymentAttribute()
    {
        $plata = Payment::where('expense_id',$this->id)
            ->sum('sum');

        return $plata;


    }

    protected function remainingAmount($expense,$payment)
    {

        $rezultat = floatval($expense) - floatval($payment);
        return $rezultat;


    }

//
//    public function service()
//    {
//        return $this->belongsTo('App\Models\NomenInvoiceType', 'invoice_type_id');
//    }
//    public function files()
//    {
//        return $this->belongsToMany(File::class(), 'expense_files');
//    }
//
//    public function provider()
//    {
//        return $this->belongsTo('App\Models\Institution', 'provider_id');
//    }
//
//    public function product()
//    {
//        return $this->belongsTo('App\Models\NomenInvoiceProduct', 'invoice_product_id');
//    }
//
//    public function expense_type()
//    {
//        return $this->belongsTo('App\Models\NomenExpense', 'expense_type_id');
//    }
//    public function user()
//    {
//        return $this->belongsTo('App\Models\User', 'user_id');
//    }
//    public function payment()
//    {
//        return $this->hasMany('App\Models\Payment', 'expense_id');
//    }
//
//

}
