<?php

namespace App\Modules\Accountancy\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Receipt extends Model
{
    use SoftDeletes;

    protected $table = 'receipts';

    protected $fillable = [
        'date',
        'details',
        'income_id',
        'sum',
        'moneda'
    ];

    protected $hidden = ['pivot', 'deleted_at'];


      public function income()
      {
          return $this->belongsTo('App\Models\Income','income_id');
      }


    public function files()
    {
        return $this->belongsToMany('App\Models\File', 'receipt_files');
    }

}
