<?php

namespace App\Modules\Accountancy\Models;

use Illuminate\Database\Eloquent\Model;

class ReceiptFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'receipt_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_id', 'receipt_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * Returns user files
     *
     * @return mixed
     */

}
