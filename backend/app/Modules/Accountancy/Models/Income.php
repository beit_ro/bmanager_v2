<?php

namespace App\Modules\Accountancy\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Income extends Model
{
    use SoftDeletes;

    protected $table = 'incomes';

    protected $fillable = [
        'number',
        'date',
        'date_due',
        'amount',
        'client_id',
        'contract_id',
//        'receipt_id'
    ];


    protected $appends = [
        'remainingAmount',
        'totalCollection'
    ];

    public function getremainingAmountAttribute()
    {
        $income=Receipt::where('income_id',$this->id)
            ->sum('sum');
        return $this->remainingAmount($this->amount,$income);

    }

    public function gettotalCollectionAttribute()
    {
        $collection=Receipt::where('income_id',$this->id)
            ->sum('sum');

        return $collection;


    }

    protected function remainingAmount($income,$collection)
    {

        $rezultat= floatval($income) - floatval($collection);
        return $rezultat;


    }

    protected $hidden = ['pivot', 'deleted_at'];


    public function files()
    {
        return $this->belongsToMany('App\Models\File', 'income_files');
    }

    public function client()
    {
        return $this->belongsTo(Institution::class)
            ->where('institution_relation_id',2);
    }
    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function receipt()
    {
        return $this->hasMany('App\Models\Receipt', 'income_id');
    }



}
