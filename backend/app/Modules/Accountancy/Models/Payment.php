<?php

namespace App\Modules\Accountancy\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $fillable = [
        'date',
        'details',
        'expense_id',
        'moneda',
        'sum'
    ];

    protected $hidden = ['pivot', 'deleted_at'];

//    public function service()
//    {
//        return $this->belongsTo('App\Models\NomenInvoiceType', 'invoice_type_id');
//    }
      public function expense()
      {
          return $this->belongsTo(Expense::class);
      }
    public function files()
    {
        return $this->belongsToMany('App\Models\File', 'payment_files');
    }

}
