<?php

namespace App\Modules\Unsecured\Controllers;


use App\Modules\DefaultController;
use App\Modules\Organizations\Models\Organization;
use App\Modules\System\Services\DashboardService;


class UnsecuredController extends DefaultController
{



    public function __construct(DashboardService $defautService)
    {

        parent::__construct($defautService);
    }


    public static function getOrganizations()
    {
        return Organization::whereNotNull('id')->get();

    }


}
