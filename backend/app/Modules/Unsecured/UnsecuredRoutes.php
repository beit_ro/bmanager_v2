<?php


use App\Modules\System\Controllers\DashboardController;
use App\Modules\System\Controllers\SettingsController;
use App\Modules\Unsecured\Controllers\UnsecuredController;

Route::group([
    'prefix' => 'unsecured'
], function () {

      Route::get('organizations', UnsecuredController::getShortClassName() . '@getOrganizations');

});


