<?php


namespace App\Modules\Files\Services;

use App\Helpers\NotificationHelper;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Files\Models\File;
use App\Modules\Files\Models\UserFile;
use App\Modules\Organizations\Models\Organization;
use Illuminate\Support\Facades\Response;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use stdClass;
use ZipArchive;

/**
 * Class FilesService
 * @package App\Modules\Users\Services
 */
class FoldersService extends DefaultService
{

    /**
     * Default per page number
     */
    const PER_PAGE = 50;
    /**
     * @var File
     */
    protected $model;

    /**
     * FilesService constructor.
     * @param File $defaultModel
     */
    public function __construct(File $defaultModel)
    {
        parent::__construct($defaultModel);
    }

    public function create($params)
    {

        $route = [];
        $user_id = UserHelper::getUser();
        $userdetails = UserHelper::getUserDetails($user_id);

        $params['original_name'] = $params['name'];
        $params['md5'] = $this->generatefileKey();
        $params['path'] ='';

        $folderExists = File::where('name', $params['name'])
            ->where('parent_id', $params['parent_id'])
            ->first();
        // $currentFolder = $user_id . '/' . $folderExists['original_name'] . '/';


        if ($folderExists) {
            $rand = mt_rand(100000, 999999);
            $params['original_name'] = $params['name'] . '_' . $rand;
            $params['name'] = $params['name'] . '_' . $rand;
        }

        if ($params['parent_id'] == 1) {
            $params['path'] = $user_id . '/' . $params['name'];

        } else {

            $children = File::where('id', $params['parent_id'])
                ->first();
            array_push($route,$children['original_name']);
           while (isset($children['parent_id'])) {
               $children = File::where('id', $children['parent_id'])->first();
               //$params['path'] =  $children['original_name']. '/' .$params['path'] ;
               array_unshift($route,$children['original_name']);
              //echo($params['path']);

            }

            $params['path'] = $user_id . implode('/', $route);
        }


//return $params;


        (isset($params->description)) ? $data_description = $params->description : $data_description = '';
        (isset($params->notification)) ? $data_notification = $params->notification : $data_notification = 'none';
        (isset($params->level)) ? $data_level = $params->level : $data_level = 'private';



        $params['description'] = $data_description;
        $params['notification'] = $data_notification;
        $params['level'] = $data_level;
        $params['organization_id'] = $userdetails['organization_id'];

        $storagePathFiles = storage_path('app/files/');
        $folder = parent::create($params);

        $path= $storagePathFiles.$params['path'];
        if (!mkdir($path, 0777, true)) {

        }

        if (isset($params['users'])) {
            foreach ($params['users'] as $item) {
                UserFile::create([
                    'user_id' => $item,
                    'owner_id' => $user_id,
                    'file_id' => $folder['id']
                ]);
            }
        }

        if (isset($params['notification']) && isset($params['users'])) {
            $userDetails = UserHelper::getUserDetails($user_id);
            //return $userDetails;
            $message = 'A new file folder ' . $params['name'] . ' was created by ' . $userDetails['name'] . ' and access was granted';
            if ($params['users']) {
                NotificationHelper::recordNotification($data_notification, $message, $params['users']);
            }

            $blade = 'GB.emails.file';
            $organization = Organization::where('id', $userDetails['organization_id'])->first();
            $email_data = new stdClass;
            $email_data->url = 'file/' . $folder['md5'];
            $email_data->name = 'User';
            $email_data->file_name = $folder['name'];

            if ($data_notification == 'selected') {


                   $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$userDetails['email']);
            }

            if ($data_notification == 'organization') {


                //  $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$organization['email']);
            }

            if ($data_notification == 'everyone') {

                // $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$organization['email']);
                //  $email2 = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$userDetails['email']);
            }
        }
        return $folder;
    }


    public function getFolderDownload($id, $params = NULL)
    {

        $file = $this->defaultModel->where('md5', $id)->first();
        $relPath = storage_path('app/files/' . $file->path);
        $zipname = 'Archive' . '_' . time() . '.zip';
        $zip = new ZipArchive();
        $zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        try {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($relPath), RecursiveIteratorIterator::LEAVES_ONLY);
        }
        catch (\Exception $err){
            return response()->json("No files inside folder! There is nothing to download!",500);
        }



        //Archive server files
        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($relPath) + 1);
                $zip->addFile($filePath, $relativePath);
            }

        }
        $zip->close();
        if($files){

            return response()->download($zipname);
        }
        else {
            return response()->json('Not available for download');
        }


    }


    function generatefileKey($length = 75)
    {
        $characters = strtolower('0123456789ABCDEFGHIJKLMNOPQRSTUXYZ');
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }




}
