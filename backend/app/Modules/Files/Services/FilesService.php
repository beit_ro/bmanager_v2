<?php


namespace App\Modules\Files\Services;

use App\Helpers\EmailHelper;
use App\Helpers\NotificationHelper;
use App\Helpers\TreeHelper;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Files\Models\File;
use App\Modules\Files\Models\UserFile;
use App\Modules\Organizations\Models\Organization;
use Illuminate\Support\Facades\Response;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use stdClass;
use ZipArchive;

/**
 * Class FilesService
 * @package App\Modules\Users\Services
 */
class FilesService extends DefaultService
{

    /**
     * Default per page number
     */
    const PER_PAGE = 50;
    /**
     * @var File
     */
    protected $model;

    /**
     * FilesService constructor.
     * @param File $defaultModel
     */
    public function __construct(File $defaultModel)
    {
        parent::__construct($defaultModel);
    }

    public function create($request)
    {

        $data = json_decode($request['data']);
        $user_id = UserHelper::getUser();
        $userdetails = UserHelper::getUserDetails($user_id);
        $nameonly = str_random(65);
        if ($request->hasFile('file')) {
            $reqFile = $request->file('file');
            $reqFileSize = $request->file('file')->getClientSize();
            $reqFileExt = $reqFile->getClientOriginalExtension();
            $reqFileOriginalName = $reqFile->getClientOriginalName();
            $reqFilewoExt = pathinfo($reqFileOriginalName, PATHINFO_FILENAME);
            $nameonly = $this->filename_sanitizer($reqFileOriginalName);
            $storagePathFiles = storage_path('app/files/');
            $folderName = File::where('id', $request['parent_id'])->first();
            // $currentDayFilesFolder = date('Y_m_d') . '/';
            $fileDay = date('Y_m_d');
            $currentDayFilesFolder = $user_id . '/' . $folderName['original_name'] . '/';
            //$random_name = str_random(64) . '.' . $reqFileExt;
            $rand = str_random(16);
            $newFileName = $nameonly . '_' . $rand . '.' . $reqFileExt;
            //return $newFileName;
            $newFilePath = $folderName['path'] . '/' . $folderName['original_name'] . '/' . $newFileName;

            //return $reqFileOriginalName;
        }


        if (isset($reqFile) && $reqFile) {
            $reqFile->move($storagePathFiles . '/' . $folderName['path'] . '/' . $folderName['original_name'], $newFileName);

            $lwFileExt = strtolower($reqFileExt);
            if (in_array($lwFileExt, ['pdf', 'doc', 'txt', 'xls', 'idx'])) {
                $type = 'document';
                $icon = 'description';
            } else if (in_array($lwFileExt, ['jpg', 'jpeg', 'png', 'gif', 'tif'])) {
                $type = 'image';
                $icon = 'image';
            } else if (in_array($lwFileExt, ['db', 'sql', 'xml'])) {
                $type = 'dbase';
                $icon = 'analytics';
            } else if (in_array($lwFileExt, ['mp4', 'vid', 'avi', 'webm', 'mkv', 'flv', 'ogg', 'vob', 'mov', 'wmv', 'mpg', 'mpeg', 'm4v', 'svi'])) {
                $type = 'video';
                $icon = 'play_circle_outline';
            } else if (in_array($lwFileExt, ['mp3', '3gp', 'aa', 'aac', 'aax', 'act', 'aiff', 'alac', 'amr', 'dvf', 'raw', 'vox', 'wav', 'wma', 'cda'])) {
                $type = 'audio';
                $icon = 'audiotrack';
            } else if (in_array($lwFileExt, ['zip', 'tar', 'rar', '7z', 'iso', 'mar', 'gz', 'lzo', 'z'])) {
                $type = 'compressed';
                $icon = 'texture';
            } else if ($lwFileExt == '') {
                $type = 'folder';
                $icon = 'folder';
            } else {
                $type = 'document';
                $icon = 'texture';
            }

            (isset($data->description)) ? $data_description = $data->description : $data_description = '';
            (isset($data->notification)) ? $data_notification = $data->notification : $data_notification = 'none';
            (isset($data->level)) ? $data_level = $data->level : $data_level = 'private';
            $size = number_format((float)$reqFileSize * pow(10, -6), 2, '.', '');

            $file = File::create([
                'name' => $reqFilewoExt,
                'original_name' => $reqFilewoExt,
                'description' => $data_description,
                'notification' => $data_notification,
                'md5' => $this->generatefileKey(),
                'path' => $newFilePath,
                'level' => $data_level,
                'organization_id' => $userdetails['organization_id'],
                'size' => $size,
                'type' => $type,
                'icon' => $icon,
                'extension' => $reqFileExt,
                'user_id' => $user_id,
                'parent_id' => $request['parent_id'],
                //'checksum' => md5_file(storage_path('app/files/' . $newFilePath)),
            ]);


            if (isset($data->users)) {
                foreach ($data->users as $item) {
                    UserFile::create([
                        'user_id' => $item,
                        'owner_id' => $user_id,
                        'file_id' => $file['id']
                    ]);
                }

            }
            if (isset($data->notification) && isset($data->users)) {

                $userDetails = UserHelper::getUserDetails($user_id);
                //return $userDetails;
                $message = 'A new file named ' . $reqFileOriginalName . ' was created by ' . $userDetails['name'] . ' and access was granted';
                if ($data->users) {
                    NotificationHelper::recordNotification($data_notification, $message, $data->users);
                }


                $blade = 'GB.emails.file';
                $organization = Organization::where('id', $userDetails['organization_id'])->first();
                $email_data = new stdClass;
                $email_data->url = 'file/' . $file['md5'];
                $email_data->name = 'User';
                $email_data->file_name = $file['name'];

                if ($data_notification == 'selected') {


                    $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared", $userDetails['email']);
                }

                if ($data_notification == 'organization') {


                    //  $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$organization['email']);
                }

                if ($data_notification == 'everyone') {

                    // $email = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$organization['email']);
                    //  $email2 = EmailHelper::sendCustomEmail($blade, $email_data, "File shared",$userDetails['email']);
                }
            }

        } else {
            $file = 'Error';
        }


        return $file;
    }

    public function update($params, $id)
    {

        $user_id = UserHelper::getUser();
        $file = File::find($id);
        $file->update($params);
        if (count($params['users']) > 0) {
            //return 'aa';
            UserFile::where('file_id', $id)->delete();
            foreach ($params['users'] as $item) {
                UserFile::create([
                    'user_id' => $item['id'],
                    'owner_id' => $user_id,
                    'file_id' => $id
                ]);
            }

        }


        return $file;

    }


    public function getFileDownload($id, $params = NULL, $relationships = NULL)
    {
        $file = $this->defaultModel->where('md5', $id)->first();
        $filename = $file->path;
        $name = $file->name;
        $extension = $file->extension;
        $path = storage_path('app/files/') . $filename;
        $type = $file->extension;
        if (isset($params['preview']) && $params['preview'] == true) {
            $lwFileExt = strtolower($file->extension);
            if ($lwFileExt == 'pdf') {
                $type = 'application/pdf';
            } else if (in_array($lwFileExt, ['jpg', 'jpeg', 'png', 'gif'])) {
                $type = 'image/' . $lwFileExt;
            }


            return response()->make(file_get_contents($path), 200, [
                'Content-Type' => $type,
                'Content-Disposition' => 'inline; filename="' . $filename . '.' . $type . '"'
            ]);
        } else {

            return response()->download($path, $name . '.' . $extension);
        }


    }


    public function getZip($id, $params = NULL)
    {

        $file = $this->defaultModel->where('md5', $id)->first();
        $relPath = storage_path('app/files/' . $file->path);
        $zipname = 'Archive' . '_' . time() . '.zip';
        $zip = new ZipArchive();
        $zip->open($zipname, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($relPath), RecursiveIteratorIterator::LEAVES_ONLY);


        //Archive server files
        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($relPath) + 1);
                $zip->addFile($filePath, $relativePath);
            }

        }
        $zip->close();
        return response()->download($zipname);

    }

    function generatefileKey($length = 75)
    {
        $characters = strtolower('0123456789ABCDEFGHIJKLMNOPQRSTUXYZ');
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    function filename_sanitizer($unsafeFilename)
    {

        // our list of "unsafe characters", add/remove characters if necessary
        $dangerousCharacters = array(" ", '"', "'", "&", "/", "\\", "?", "#", ".");

        // every forbidden character is replaced by an underscore
        $safe_filename = str_replace($dangerousCharacters, '_', $unsafeFilename);

        return $safe_filename;
    }




    public function getShared($id, $params = NULL)
    {
        $params['parent_id'] = 2;
        //$userId = UserHelper::getUser();
        $userId = 1;
        $userOrg = UserHelper::getUserDetails($userId);

        $queryOrganization = $this->defaultModel->whereNotNull('id')
            ->where('organization_id', $userOrg['organization_id'])
            ->where('level', 'organization')->pluck('id')->toArray();

        $queryPublic = $this->defaultModel->whereNotNull('id')
            ->where('level', 'public')->pluck('id')->toArray();


        $queryUser = $this->defaultModel->whereNotNull('id')
            ->where('level', 'private')
            ->where('user_id', $userId)->pluck('id')->toArray();

        $queryShared = UserFile::where('user_id', $userId)->pluck('file_id')->toArray();


       // $comma_separated = implode(",", $queryUser);

        $result = array_values(array_unique (array_merge($queryShared,$queryUser,$queryPublic,$queryOrganization),SORT_NUMERIC));



        $queryFinal = $this->defaultModel->whereNotNull('id')
            ->whereIn('id',$result)->get();


        return $queryFinal;



    }

    public function getAll($params, $paginated = true, $relationships = NULL)
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : self::PER_PAGE;
        $orderBy = isset($params['orderby']) ? $params['orderby'] : self::ORDER_BY;
        $orderByType = isset($params['order']) ? $params['order'] : 'DESC';
        $groupBy = isset($params['groupby']) ? $params['groupby'] : 'ID';

        //$params['parent_id'] = 2;
        $userId = UserHelper::getUser();
        ///$userId = 1;
        $userOrg = UserHelper::getUserDetails($userId);

        $queryOrganization = $this->defaultModel->whereNotNull('id')
            ->where('organization_id', $userOrg['organization_id'])
            ->where('level', 'organization')->pluck('id')->toArray();

        $queryPublic = $this->defaultModel->whereNotNull('id')
            ->where('level', 'public')->pluck('id')->toArray();


        $queryUser = $this->defaultModel->whereNotNull('id')
            ->where('level', 'private')
            ->where('user_id', $userId)->pluck('id')->toArray();

        $queryShared = UserFile::where('user_id', $userId)->pluck('file_id')->toArray();

        $result = array_values(array_unique (array_merge($queryShared,$queryUser,$queryPublic,$queryOrganization),SORT_NUMERIC));

        $query = $this->defaultModel->whereNotNull('id')
            ->whereIn('id',$result);

        if ($relationships) {
            $relationships = is_array($relationships) ? $relationships : [$relationships];
            $query = $query->with($relationships);
        }
        $this->attachFilters($params, $query);
        return $paginated === true ? $query->orderBy($orderBy, $orderByType)->groupBy($groupBy)
            ->paginate($perPage) : $query->get();


    }


}
