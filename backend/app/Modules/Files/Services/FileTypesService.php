<?php


namespace App\Modules\Files\Services;

use App\Modules\DefaultService;
use App\Modules\Files\Models\File;
use App\Modules\Files\Models\FileType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;

/**
 * Class FilesService
 * @package App\Modules\Users\Services
 */
class FileTypesService extends DefaultService
{
    public function __construct(FileType $defaultModel)
    {
        parent::__construct($defaultModel);
    }
}