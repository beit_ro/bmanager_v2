<?php


use App\Modules\Files\Controllers\FilesController;
use App\Modules\Files\Controllers\FoldersController;

Route::group([
    'middleware' => 'auth'
], function () {
    Route::get('files/zip/{id}', FilesController::getShortClassName() . '@getZip');
    Route::get('file/{id}', FilesController::getShortClassName() . '@getFileDownload');
    Route::get('folder/{id}', FoldersController::getShortClassName() . '@getFolderDownload');
    Route::get('files/shared', FilesController::getShortClassName() . '@getShared');
    Route::resources([
        'files' => 'FilesController',
        'folders' => 'FoldersController',
        'file_types' => 'FileTypesController',

    ]);




});



