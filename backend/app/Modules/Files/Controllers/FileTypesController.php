<?php

namespace App\Modules\Files\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\Claims\Models\ExternalFile;
use App\Modules\DefaultController;
use App\Modules\Files\Models\File;
use App\Modules\Files\Services\FilesService;
use App\Modules\Files\Services\FileTypesService;
use App\Modules\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as LaravelFile;
use Illuminate\Support\Facades\Response;
use Ixudra\Curl\Facades\Curl;

/**
 * Class FileTypesController
 * @package App\Modules\Files\Controllers
 */
class FileTypesController extends DefaultController
{
    /**
     * FileTypesController constructor.
     * @param FileTypesService $defaultService
     */
    public function __construct(FileTypesService $defaultService)
    {
        parent::__construct();
    }
}
