<?php

namespace App\Modules\Files\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\Claims\Models\ExternalFile;
use App\Modules\DefaultController;
use App\Modules\Files\Models\File;
use App\Modules\Files\Services\FilesService;
use App\Modules\Files\Services\FileTypesService;
use App\Modules\Files\Services\FoldersService;
use App\Modules\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as LaravelFile;
use Illuminate\Support\Facades\Response;
use Ixudra\Curl\Facades\Curl;

class FoldersController extends DefaultController
{

    public function __construct(FoldersService $defautService)
    {

        parent::__construct($defautService);
    }

    public function getFolderDownload($id, Request $request)
    {
        $params = $request->json()->all();
        return $this->defaultService->getFolderDownload($id, $params);
    }


}
