<?php

namespace App\Modules\Files\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\Claims\Models\ExternalFile;
use App\Modules\DefaultController;
use App\Modules\Files\Models\File;
use App\Modules\Files\Services\FilesService;
use App\Modules\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as LaravelFile;
use Illuminate\Support\Facades\Response;
use Ixudra\Curl\Facades\Curl;

/**
 * Class FilesController
 * @package App\Http\Controllers
 */
class FilesController extends DefaultController
{
    protected $defaultService;

    /**
     * FilesController constructor.
     * @param FilesService $defaultService
     */
    public function __construct(FilesService $defaultService)
    {
        $this->defaultService = $defaultService;

        parent::__construct($defaultService);
    }


    public function store(Request $request)
    {
        return $this->defaultService->create($request);
    }

    public function getZip($id,Request $request)
    {
        $params = $request->json()->all();
        return $this->defaultService->getZip($id ,$params);
    }

    public function getFileDownload($id, Request $request)
    {
        $params = $request->json()->all();
        return $this->defaultService->getFileDownload($id, $params);
    }

    public function update(Request $request, $id)
    {
        $params = $request->json()->all();
        return $this->defaultService->update($params, $id);
    }

    public function getShared(Request $request)
    {
        $params = $request->json()->all();
        return $this->defaultService->getShared($params);
    }


}
