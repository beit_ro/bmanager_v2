<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('name');
            $table->text('original_name');
            $table->text('path')->nullable();
            $table->text('type')->nullable();
            $table->text('icon')->nullable();;
            $table->text('description')->nullable();;
            $table->double('size',2)->nullable();
            $table->string('extension')->nullable();;
            $table->string('level')->nullable();;

            $table->string('md5')->nullable();
            $table->string('notification')->nullable();

            $table->unsignedBigInteger('organization_id')->unsinged()->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('files')->onDelete('cascade');


            $table->timestamps();
            $table->softDeletes();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
