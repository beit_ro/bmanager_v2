<?php

namespace App\Modules\Files\Models;


use App\Helpers\RightsHelper;
use App\Helpers\TreeHelper;
use App\Modules\Incidents\Models\IncidentFile;
use App\Modules\Navigation\Models\Navigation;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['id','name', 'path','type','icon','size','extension','original_filename','user_id','parent_id',
        'level','organization_id','description','md5','notification','original_name'];
    public $searchable = ['name', 'path','type','size','extension','original_name','user_id','parent_id'];


    protected $appends = ['breadcrumb'];

//
//    public function getFileSizeAttribute()
//    {
//        if ($this->path) {
//
//            if (file_exists(storage_path('app/files/' . $this->path))) {
//                $getSize = Storage::size('/files/' . $this->path);
//                $size = number_format((float)$getSize * pow(10, -6), 2, '.', '');
//                $size .= 'MB';
//                return $size;
//            } else {
//                return 'UNKNOWN';
//            }
//        } else {
//            return 'UNKNOWN';
//        }
//    }
    public static function put(string $string, $output)
    {
    }


    public function creator()
    {
        return $this->belongsTo(UserDetail::class, 'user_id');
    }

    public function file_type()
    {
        return $this->belongsTo(FileType::class);
    }

//    public function parent()
//    {
//        return $this->belongsTo(self::class);
//    }

//    public function getCommentIdAttribute()
//    {
//        $comment = IncidentFile::where('file_id', $this->id)->first();
//        return $comment['comment_id'];
//    }

    public function getBreadCrumbAttribute()
    {

    }

    public function parent()
    {
        return $this->belongsTo(self::class,'id','parent_id');
    }

    public function children()
    {
        return $this->belongsTo(self::class,'parent_id','id');
    }


    public function recursiveChildren() {
        return $this->children()->with('recursiveChildren');
        //It seems this is recursive
    }

    public function recursiveParent() {
        return $this->parent()->with('recursiveParent');
        //It seems this is recursive
    }

    public function users()
    {
        return $this->hasManyThrough(User::class, UserFile::class,'file_id', 'id', 'id', 'user_id');
    }

//    public function children()
//    {
//        $result = $this->hasMany(File::class, 'parent_id', 'id')
//           ->orderBy('id','ASC');
//
//
//        return $result;
//
//
//
//    }
//
//    public function parent()
//    {
//
//        return $this->belongsTo(File::class, 'parent_id', 'id');
//
//    }





}
