<?php


namespace App\Modules\System\Models;



use App\Modules\Users\Models\User;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\ImageManagerStatic as Image;

class Settings extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';



    use Encryptable;
    protected $encryptable = [

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value'.
        'owner_id'

    ];

    public $searchable = ['name'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


}
