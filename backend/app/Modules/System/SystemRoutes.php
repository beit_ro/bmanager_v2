<?php


use App\Modules\System\Controllers\DashboardController;
use App\Modules\System\Controllers\SettingsController;

Route::group([
   // 'middleware' => ['auth','right'],
], function () {

    Route::resources([
        'settings' => SettingsController::getShortClassName(),
       // 'dashboard' =>DashboardController::getShortClassName()
    ]);


    Route::get('dashboard/stats', DashboardController::getShortClassName() . '@getStats');

});


