<?php

namespace App\Modules\System\Controllers;


use App\Modules\DefaultController;
use App\Modules\System\Services\DashboardService;


class DashboardController extends DefaultController
{



    public function __construct(DashboardService $defautService)
    {

        parent::__construct($defautService);
    }


    public function getStats(){

        $stats = [];
        $uptime = shell_exec("uptime -p");
        $cpu = shell_exec("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'");
        $hdd = shell_exec("df -H /dev/md0 --output=pcent,size");
        $pre = explode("\n",$hdd);
        $stats['cpu_usage'] = number_format((float)$cpu).'%';
        if($pre){
           //$fs = array_filter(explode(" ",$pre[1]));
          // $stats['hdd_percent'] = $fs['2'];
          // $stats['hdd_size'] = $fs['4'];

        }
        $stats['uptime'] = str_replace("\n" ,'' ,$uptime);
        $stats['memory_usage_percent'] = $this->getMemoryUsage().'%';



        return $stats;


    }


    function getSymbolByQuantity($bytes) {
        $symbols = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
        $exp = floor(log($bytes)/log(1024));

        return sprintf('%.2f '. $symbols[$exp], ($bytes/pow(1024, floor($exp))));
    }



    protected function getMemoryUsage(): float
    {
        $fh = fopen('/proc/meminfo', 'r');
        $mem = 0;
        $all_str = '';

        while ($line = fgets($fh)) {
            $all_str .= $line;
        }
        fclose($fh);

        preg_match_all('/(\d+)/', $all_str, $pieces);

        $used = round($pieces[0][6] / $pieces[0][0], 2);
        return $used;
    }






}
