<?php namespace App\Modules\Users\Middleware;

use App\Modules\Users\Models\RawLog;
use Closure;

class HttpLog {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $response = $next($request);
        RawLog::create([
            'ip_address'=> $request->getClientIp(),
            'request' => $request,
            'response' => $response,
            'url' => $request->url(),
            'token' => $request->header('Auth-Token')
        ]);
        return $response;
    }

}
