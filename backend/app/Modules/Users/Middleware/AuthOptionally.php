<?php

namespace App\Modules\Users\Middleware;

use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use Closure;

class AuthOptionally
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->header(Auth::HEADER_NAME);
        if (!$token) {
            if ($request->has(Auth::TOKEN_NAME)) {
                $token = $request->input(Auth::TOKEN_NAME);
            }
        }

        if (isset($token) && $token) {
            $session = Session::where('token', '=', $token)
                ->where('expires_at', '>=', time())
                ->first();
        }

        if (isset($session) && $session) {
            $user = User::find($session->user_id);
            $request->user = $user;
        }

        return $next($request);
    }
}
