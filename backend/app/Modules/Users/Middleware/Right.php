<?php

namespace App\Modules\Users\Middleware;

use App\Helpers\RightsHelper;
use Closure;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class Right
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $allowed = false;
        $controller = Route::currentRouteAction();
        $controllers = last(explode('\\', $controller));
        $controllers = explode('@', $controllers);
        $class = str_replace('Controller', '', $controllers[0]);
        $result = RightsHelper::checkClassRights($class);
        $method = Request::method();

        if($method == 'POST' && $result[0] == 1)
        {
            $allowed = true;
        }
        if($method == 'GET' && $result[1] == 1)
        {
            $allowed = true;
        }
        if($method == 'PUT' && $result[2] == 1)
        {
            $allowed = true;
        }
        if($method == 'OPTIONS' && $result[2] == 1)
        {
            $allowed = true;
        }
        if($method == 'DELETE' && $result[3] == 1)
        {
            $allowed = true;
        }
        if( $allowed == false)
        {
            return response(['message' => 'Unauthorized'], 401);
        }


        return $next($request);


    }
}
