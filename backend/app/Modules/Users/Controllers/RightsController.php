<?php


	namespace App\Modules\Users\Controllers;

    use App\Modules\DefaultController;
	use App\Modules\Users\Services\RightsService;
    use Illuminate\Http\Request;


    class RightsController extends DefaultController
	{
		protected $rightsService;

        /**
         * UsersController constructor.
         *
         * @param RightsService $rightsService
         */
		public function __construct(RightsService $rightsService)
		{
			$this->rightsService = $rightsService;

            parent::__construct($rightsService);


		}

        public function NameSpaces()
        {

            return $this->rightsService->NameSpaces();
        }



        public function userRights(Request $request, $id)
        {
            return $this->rightsService->userRights($request, $id);
        }


        public function unique(Request $request)
        {
            return $this->rightsService->unique($request);
        }



        public function removeRight(Request $request)
        {
            return $this->rightsService->removeRight($request);
        }


    }
