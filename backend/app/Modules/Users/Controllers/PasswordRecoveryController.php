<?php


namespace App\Modules\Users\Controllers;


use App\Helpers\Responser;
use App\Http\Controllers\Controller;
use App\Modules\Users\Models\PasswordResetToken;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PasswordRecoveryController extends Controller
{
    public function resetPassword(Request $request, $token)
    {
        $tk = PasswordResetToken::where('token', $token)
            ->where('expire_at', '>', Carbon::now())
            ->where('used', 0)
            ->first();

        if (!$tk) {
            return Responser::throwErrors(403, 'Invalid token!');
        }

        $user = $tk->user()->first();

        $password = $request->json('password');
        $passwordConf = $request->json('password_confirmation');
        if ($password === $passwordConf) {
            $user->update([
                'password' => bcrypt($password)
            ]);
        } else {
            return Responser::throwErrors(403, 'Passwords don\'t match!');
        }

        $tk->used = 1;
        $tk->save();

    }
}