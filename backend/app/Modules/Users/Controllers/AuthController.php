<?php


namespace App\Modules\Users\Controllers;


use App\Helpers\Responser;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Modules\Groups\Models\Group;
use App\Modules\Suppliers\Models\Supplier;
use App\Modules\Suppliers\Models\SupplierUser;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use App\Modules\Users\Services\UsersService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

/**
 * Class AuthController
 * @package App\Modules\Users\Controllers
 */
class AuthController extends Controller
{
    protected $usersService;

    /**
     * AuthController constructor.
     *
     * @param UsersService $usersService
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }


    /**
     * Login function
     *
     * @param Request $request
     * @return array
     */
    public function login(Request $request)
    {
        $user = null;

        if (!$request->json('email'))
            return Responser::throwErrors(400, 'No email sent as parameter!');

        $type = Input::get('mobile');
        $post_email = $request->json('email');

        $emails = User::where('deleted_at', null)
            ->activeOnly()
            ->select('email', 'id')
            ->get();

        if ($type == true) {

            // Mobile auth

            foreach ($emails as $email) {
                if (strtolower($post_email) == strtolower($email->email)) {


                    //todo Secure mobile
                    $user = User::where('id', $email->id)
                        ->activeOnly()
                        ->first();
                }

            }
        } else {

            foreach ($emails as $email) {


                if (strtolower($post_email) == strtolower($email->email)) {
                    $user = User::where('id', $email->id)
                        ->activeOnly()
                        ->first();


                }

            }

        }


        if (!$user)
            return Responser::throwErrors(400, 'Your user or password are wrong or your user was not activated!');


        if ($user && Hash::check($request->json('password'), $user->password)) {
            $type = $user->type;
            $externalIp = $_SERVER['REMOTE_ADDR'];
            $internalIp = $request->getClientIp();

            $session = Session::where('user_id', $user->id)
                ->where('internal_ip', $internalIp)
                ->where('external_ip', $externalIp)
                ->where('expires_at', '>', Carbon::now())
                ->first();

            if (!$session) {
                $session = Session::create([
                    'user_id' => $user->id,
                    'token' => str_random(128),
                    'external_ip' => $externalIp,
                    'internal_ip' => $internalIp,
                    'expires_at' => Carbon::now()->addDays(30)
                ]);
            }
            if ($type == 2) {

                $supplierData = SupplierUser::where('user_id', $user->id)->first();
                $supplier = Supplier::where('id', $supplierData['id'])->first();

                return [
                    'session' => $session,
                    'user' => $user,
                    'supplier' => $supplier,

                ];
            } else {

            }


            return [
                'session' => $session,
                'user' => $user,

            ];

        } else {
            return Responser::throwErrors(403, 'Wrong password!');
        }

    }

    /**
     * Log out function
     *
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        if ($request->header(Auth::HEADER_NAME)) {
            $token = $request->header(Auth::HEADER_NAME);
        } elseif ($request->has(Auth::TOKEN_NAME)) {
            $token = $request->input(Auth::TOKEN_NAME);
        } else {
            $token = null;
        }

        if ($token) {

            $session = Session::where('token', $token)
                ->first();

            if ($session) {
                $session->update(['expires_at' => Carbon::now()]);
            } else {
                return Responser::throwErrors(200, 'You are not logged in!');
            }

            // return $session;
        } else {
            return Responser::throwErrors(200, 'You are not logged in!');
        }

        return response()->json('Logged out');
    }

    /**
     * Return the user from the request token
     *
     * @param Request $request
     * @return mixed
     */
    public function getMe(Request $request)
    {

        $externalIp = $_SERVER['REMOTE_ADDR'];
        $internalIp = $request->ip();

        $session = Session::where(function ($q) use ($request) {
            $q->where('token', '=', $request->header(Auth::HEADER_NAME))
                ->orWhere('token', '=', $request->input(Auth::TOKEN_NAME));
        })
            ->where('expires_at', '>', Carbon::now())
            ->where('external_ip', $externalIp)
            ->where('internal_ip', $internalIp)
            ->first();


        if (!$session) {
            return Responser::throwErrors(401, 'You are not logged in');
        }


        $userDetail = UserDetail::where('user_id', $session->user_id)->first();
        $userEmail = User::where('id', $session->user_id)->first();
        $userDetail['email'] = $userEmail['email'];
        //$userDetail['avatar'] = $userEmail['email'];
        return $userDetail;
    }


    public function forgotPassword(Request $request)
    {
        return $this->usersService->forgotPassword($request);
    }


    public function changePassword(Request $request)
    {
        if ($request->header(Auth::HEADER_NAME)) {
            $token = $request->header(Auth::HEADER_NAME);
        } elseif ($request->has(Auth::TOKEN_NAME)) {
            $token = $request->input(Auth::TOKEN_NAME);
        } else {
            $token = null;
        }

        if ($token) {
            $session = Session::where('token', $token)
                ->where('expires_at', '>', Carbon::now())
                ->first();

            if ($session) {
                $user = $session->user;
                if (Hash::check($request->json('current_password'), $user->password)) {
                    $newPassword = $request->json('new_password');
                    $newPasswordConfirmation = $request->json('new_password_confirmation');
                    if ($newPassword && $newPasswordConfirmation && $newPassword === $newPasswordConfirmation) {
                        $user->update(['password' => bcrypt($newPassword)]);
                    } else {
                        return Responser::throwErrors(403, 'New password is missing or doesn\'t match with the confirmation!');
                    }
                } else {
                    return Responser::throwErrors(403, 'Wrong current password!');
                }

            } else {
                return Responser::throwErrors(401, 'No session found!');
            }
        } else {
            return Responser::throwErrors(403, 'You are not logged in!');
        }
    }


    public function disableAccount(Request $request)
    {
        if ($request->header(Auth::HEADER_NAME)) {
            $token = $request->header(Auth::HEADER_NAME);
        } elseif ($request->has(Auth::TOKEN_NAME)) {
            $token = $request->input(Auth::TOKEN_NAME);
        } else {
            $token = null;
        }

        if ($token) {
            $session = Session::where('token', $token)
                ->where('expires_at', '>', Carbon::now())
                ->first();
            if ($session) {
                $user = $session->user;
                $user->update(['status' => 0, 'disabled_at' => Carbon::now()]);
                $session->expires_at = Carbon::now();
                $session->save();

            } else {
                return Responser::throwErrors(401, 'Invalid token');
            }
        } else {
            return Responser::throwErrors(403, 'You are not logged in!');
        }
        return ["status" => "success"];
    }


    /**
     * Register a customer
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $params = $request->json()->all();


        $user = $this->usersService->create($params);
        if (get_class($user) !== User::class) {
            return $user;
        }


        return $user;
    }


    public function activate(Request $request)
    {
        return $this->usersService->activate($request);
    }


    public function changeResetPassword(Request $request)
    {
        return $this->usersService->changeResetPassword($request);
    }


}
