<?php


	namespace App\Modules\Users\Controllers;
	use App\Helpers\UserHelper;
    use App\Http\Controllers\Controller;
    use App\Modules\Users\Services\UserDetailsService;
    use Illuminate\Http\Request;
    use Intervention\Image\ImageManagerStatic as Image;



    class UserDetailsController extends Controller
	{
		protected $usersDetailsService;

		/**
		 * UsersController constructor.
		 *
		 * @param $usersDetailsService
		 */
		public function __construct(UserDetailsService $usersDetailsService)
		{
			$this->usersDetailsService = $usersDetailsService;
			$this->middleware('throttle:100000,1')->only('getAvatar');
		}


		/**
		 * Display a listing of the resource.
		 *
		 * @param Request $request
		 * @return mixed
		 */
		public function index(Request $request)
		{
			$params = $request->all();

			return $this->usersDetailsService->getAll($params, true);
		}

		/**
		 * Display the specified resource.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function show(Request $request, $id)
		{
			$params = $request->json()->all();

			return $this->usersDetailsService->getOne($id, $params);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param Request $request
		 * @return mixed
		 */
		public function store(Request $request)
		{
			$params = $request->json()->all();

			return $this->usersDetailsService->create($params);
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function update(Request $request, $id)
		{
			$params = $request->json()->all();

			return $this->usersDetailsService->update($id, $params);
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function destroy(Request $request, $id)
		{
			$params = $request->json()->all();

			return $this->usersDetailsService->delete($id, $params);
		}

		/**
		 * Serve user's avatar
		 *
		 * @param Request $request
		 * @param $id
		 * @return mixed
		 */
		public function getAvatar(Request $request)
		{
		    $userId = UserHelper::getUser();
			$user = $this->usersDetailsService->getOne($userId);

			if($user->avatar== null)
            {
                $path = storage_path() . '/app/avatars/profile.png';
            }
			else{
                $path = storage_path() . '/app/avatars/' . $user->avatar ;

            }

			$img = Image::make($path);
			$img->resize(128, 128);

			return $img->response('png');
		}



		public function setAvatar(Request $request, $id)
		{
			$user = $this->usersDetailsService->getOne($id);

			if ($request->hasFile('avatar')) {
				$avatar = $id . '.' . $request->file('avatar')->getClientOriginalExtension();
				$request->file('avatar')->move(storage_path('app/avatars/'), $avatar);
				$img = Image::make(storage_path('app/avatars/') . $avatar);
				$img->resize(512, 512);
				$img->save();
				$user->avatarName = $avatar;
				$user->save();
			}

			return $user->load('groups');

		}





	}
