<?php


	namespace App\Modules\Users\Controllers;

	use App\Helpers\KeyHelper;
    use App\Modules\DefaultController;
	use App\Modules\Users\Models\User;
	use App\Modules\Users\Services\UserDetailsService;
    use App\Modules\Users\Services\UsersService;
    use Illuminate\Http\Request;
    use Intervention\Image\ImageManagerStatic as Image;



    class UsersController extends DefaultController
	{
		protected $usersService;
		protected $usersDetailsService;

		/**
		 * UsersController constructor.
		 *
		 * @param $usersService
		 * @param $usersDetailsService
		 */
		public function __construct(UsersService $usersService,
                                    UserDetailsService $usersDetailsService, array $rightsToCheckFor = [])
		{
			$this->usersService = $usersService;
			$this->usersDetailsService = $usersDetailsService;

            parent::__construct($usersService, $rightsToCheckFor);

            $this->middleware('throttle:100000,1')->only('getAvatar');
		}

		/**
		 * Display the specified resource.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function show(Request $request, $id)
		{
			$params = $request->json()->all();

			return $this->usersService->getOne($id, $params);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param Request $request
		 * @return mixed
		 */
		public function store(Request $request)
		{
			$params = $request->json()->all();

			return $this->usersService->create($params);
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function update(Request $request, $id)
		{
			return $this->usersService->update($id, $request);
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param Request $request
		 * @param  int $id
		 * @return mixed
		 */
		public function destroy(Request $request, $id)
		{
			$params = $request->json()->all();

			return $this->usersService->delete($id, $params);
		}



    }
