<?php


	namespace App\Modules\Users\Controllers;

    use App\Modules\DefaultController;
	use App\Modules\Users\Services\RightsService;
    use App\Modules\Users\Services\UserRightsService;
    use Illuminate\Http\Request;


    class UserRightsController extends DefaultController
	{
		protected $rightsService;

        /**
         * UsersController constructor.
         *
         * @param UserRightsService $rightsService
         */
		public function __construct(UserRightsService $rightsService)
		{
			$this->rightsService = $rightsService;

            parent::__construct($rightsService);


		}

        public function NameSpaces()
        {

            return $this->rightsService->NameSpaces();
        }

        public function userRights(Request $request, $id)
        {

            return $this->rightsService->userRights($request, $id);
        }


    }
