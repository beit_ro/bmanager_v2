<?php


use App\Modules\Users\Controllers\AuthController;
use App\Modules\Users\Controllers\PasswordRecoveryController;
use App\Modules\Users\Controllers\RightsController;
use App\Modules\Users\Controllers\UserDetailsController;
use App\Modules\Users\Controllers\UserRightsController;
use App\Modules\Users\Controllers\UsersController;


Route::post('auth/register', AuthController::getShortClassName() . '@register');
Route::post('auth/activate', AuthController::getShortClassName() . '@activate');
Route::post('auth/forgot_password', AuthController::getShortClassName() . '@forgotPassword');
Route::post('auth/change_reset_password', AuthController::getShortClassName() . '@changeResetPassword');


Route::post('auth/login', AuthController::getShortClassName() . '@login');
Route::any('auth/logout', AuthController::getShortClassName() . '@logout');




Route::group([
    'middleware' => 'auth', 'right'
], function () {

    Route::get('users/me', AuthController::getShortClassName() . '@getMe');
    Route::put('auth/change_password', AuthController::getShortClassName() . '@changePassword');
    Route::put('auth/disable_account', AuthController::getShortClassName() . '@disableAccount');
    Route::post('users/{id}/post_avatar', UserDetailsController::getShortClassName() . '@setAvatar');
    Route::get('rights/getNameSpaces', RightsController::getShortClassName() . '@NameSpaces');
    Route::get('rights/unique', RightsController::getShortClassName() . '@unique');
    Route::get('rights/removeRight', RightsController::getShortClassName() . '@removeRight');
    Route::get('users/{id}/rights', RightsController::getShortClassName() . '@userRights');

    Route::resources([
        'users' => UsersController::getShortClassName(),
        'rights' => RightsController::getShortClassName(),
        'user_rights' => UserRightsController::getShortClassName(),
    ]);

    Route::get('user/get_avatar', UserDetailsController::getShortClassName() . '@getAvatar');

});

