<?php

namespace App\Modules\Users\Services;

use App\Helpers\EmailHelper;
use App\Helpers\Responser;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\ActivationToken;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;


class UsersService extends DefaultService
{
    protected $usersModel;
    protected $sessionModel;
    protected $userDetailModel;
    protected $notificationModel;
    protected $activationTokenModel;
    const PER_PAGE = 50;

    /**
     * UsersService constructor.
     * @param User $usersModel
     * @param Session $sessionModel
     * @param UserDetail $userDetailModel
     * @param ActivationToken $activationTokenModel
     */
    public function __construct(User $usersModel,
                                Session $sessionModel,
                                UserDetail $userDetailModel,
                                ActivationToken $activationTokenModel)
    {
        $this->usersModel = $usersModel;
        $this->sessionModel = $sessionModel;
        $this->userDetailModel = $userDetailModel;
        $this->activationTokenModel = $activationTokenModel;
        parent::__construct($usersModel);
    }

    public function create($params)
    {
        if (!$params['email'])
            return Responser::throwErrors(400, 'No email sent as parameter!');

        $post_email = strtolower($params['email']);
        $emails = $this->usersModel->where('deleted_at', null)
            ->select('email', 'id')
            ->get();

        foreach ($emails as $email) {
            if ($post_email == strtolower($email->email)) {
                return response([
                    'message' => 'Email already exists!',
                ], 400);
            }

        }

        $validator = $this->usersModel::validate($params);
        if (!$validator->passes()) {
            return response([
                'message' => 'Validation not passed',
                'errors' => $validator->errors()
            ], 400);
        }

        if (!isset($params['password']))
            $params['password'] = bcrypt(self::generateOrderKey());
        else
            $params['password'] = bcrypt($params['password']);
        $params['status'] = 0;
        $params['type'] = 1;
        $user = $this->usersModel
            ->create($params);

        $details['country_id'] = '102';
        $details['user_id'] = $user ['id'];
        $details['name'] = $params['name'];
        $details['organization_id'] = $params['organization_id'];
        $details['owner_id'] = $user['id'];
        $details['gdpr_accepted'] = 1;
        if(isset($params['name'])) {
            $dataGroup['organization_id'] = $params['name'];;
        } else{
            $dataGroup['organization_id'] = 1;
        }



        $userDetails = $this->userDetailModel
            ->create($details);

        $token['user_id'] = $user ['id'];
        $token['used'] = 0;
        $token['code'] = self::generateOrderKey();
        $tokenDetails = $this->activationTokenModel
            ->create($token);
        $userDetails['token'] = $tokenDetails ['code'];
        $blade = 'GB.emails.register';
        $email = EmailHelper::sendEmail($blade, $userDetails, "Welcome to EumarineRobots platform");
        $groups = Group::where('code', 'STF')
            ->first();
        $dataGroup['user_id'] = $user['id'];
        $dataGroup['group_id'] = $groups['id'];
        $dataGroup['owner_id'] = $user['id'];
        UserGroup::create($dataGroup);

        return $user;
    }


    //update

    public function update($id, $request)
    {


        $id = UserHelper::getUser();
        $params = $request->json()->all();
        $user = $this->usersModel->where('id', $params['user_id'])->first();
        if (isset($params['password'])) {
            unset($params['confirm_password']);
            $pass['password'] = bcrypt($params['password']);
            $user->update($pass);
            unset($params['password']);

        }

        $userDetail = $this->userDetailModel
            ->where('user_id', $id)
            ->first();
        if (!$userDetail) {
            $params['user_id'] = $id;
            $params['country_id'] = '229';
            unset($params['code']);
            $this->userDetailModel->create($params);
        } else {
            unset($params['avatar']);
            unset($params['email']);
            $userDetail->update($params);
        }


        return $userDetail;
    }

    /**
     * Delete item
     *
     * @param $id
     * @param $params
     * @return string
     */
    public function delete($id, $params = NULL)
    {

        $item = $this->usersModel->find($id);
        if (!$item) {
            return Responser::throwErrors(404, 'Can\'t find the specified resource!');
        }

        $item->delete();

        return "Resource deleted!";
    }


    public function getActiveSessions($userId)
    {
        $sessions = $this->sessionModel->where('user_id', $userId)
            ->where('expires_at', '>=', time())
            ->get();
        return $sessions;
    }

    public function sendRedisNotificationForActiveUserSessions($userId, $event, $data, $notifBody = NULL)
    {

        $sessions = $this->getActiveSessions($userId);

        if ($notifBody) {
            $notification = $this->notificationModel->create([
                'user_id' => $userId,
                'event' => $event,
                'body' => $notifBody,
                'extra' => $data
            ]);
            $data['notification'] = $notification;
        }

        foreach ($sessions as $session) {

            /*  Redis::publish($session->token, json_encode([
                  'event' => $event,
                  'data' => $data
              ]));

              //Redis::publish($session->token, $event,json_encode($data));
              //event(new RedisMessage(, $event, $data));
            */
        }

        return true;
    }

    public function getUserByEmail($email)
    {
        $post_email = strtolower($email);
        $emails = $this->usersModel->where('deleted_at', null)
            ->select('email', 'id')
            ->get();

        foreach ($emails as $email) {
            if ($post_email == strtolower($email->email)) {
                return $email['id'];
            }

        }


    }


    function generateOrderKey($length = 75)
    {
        $characters = strtolower('0123456789ABCDEFGHIJKLMNOPQRSTUXYZ');
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function activate($params)
    {

        $item = $this->activationTokenModel
            ->where('code', $params['token'])
            ->where('used', 0)
            ->first();
        $user = $this->usersModel
            ->where('id', $item['user_id'])
            ->first();
        $token['used'] = 1;
        $userData['status'] = 1;
        if ($item) {
            $item->update($token);
            $user->update($userData);

        }

        if ($user) {
            $user->update($userData);
            $externalIp = $_SERVER['REMOTE_ADDR'];
            $internalIp = $params->getClientIp();

            $session = Session::where('user_id', $user->id)
                ->where('internal_ip', $internalIp)
                ->where('external_ip', $externalIp)
                ->where('expires_at', '>', Carbon::now())
                ->first();

            if (!$session) {
                $session = Session::create([
                    'user_id' => $user->id,
                    'token' => str_random(128),
                    'external_ip' => $externalIp,
                    'internal_ip' => $internalIp,
                    'expires_at' => Carbon::now()->addDays(30)
                ]);
            }


            return [
                'session' => $session,
                'user' => $user,

            ];

        } else {
            return Responser::throwErrors(403, 'User already active!');
        }


       // $email = EmailHelper::sendEmail($blade,$userDetails,"Welcome");

    }

    public function forgotPassword($params)
    {

        $post_email = strtolower($params['email']);
        $users = $this->usersModel->where('deleted_at', null)
            ->select('email', 'id')
            ->get();

        foreach ($users as $user) {
            if ($post_email == strtolower($user->email)) {

                $userDetail = $this->userDetailModel
                    ->where('user_id', $user['id'])
                    ->first();

                $token['user_id'] = $user['id'];
                $token['used'] = 0;
                $token['code'] = self::generateOrderKey();
                $tokenDetails = $this->activationTokenModel
                    ->create($token);
                $userDetail['token'] = $tokenDetails['code'];
                //return $userDetail;
                $blade = 'GB.emails.password-reset';
                $email = EmailHelper::sendEmail($blade, $userDetail, "Reset password");
                //return $email;
            }

        }

    }



    public function changeResetPassword($params)
    {

        $item = $this->activationTokenModel
            ->where('code', $params['token'])
            ->where('used', 0)
            ->first();
        if($item)
        {
            $user = $this->usersModel
                ->where('id', $item['user_id'])
                ->first();
            $token['used'] = 1;
            $item->update($token);
            $newPassword = $params['new_password'];
            $newPasswordConfirmation = $params['new_password_confirmation'];
            if ($newPassword && $newPasswordConfirmation && $newPassword === $newPasswordConfirmation) {
                $user->update(['password' => bcrypt($newPassword)]);
                return response([
                    'message' => 'Validation passed',
                ], 200);
            } else {
                return Responser::throwErrors(403, 'New password is missing or doesn\'t match with the confirmation!');
            }
        }
        else{
            return response([
                'message' => 'Not found',
            ], 404);
        }

    }



}
