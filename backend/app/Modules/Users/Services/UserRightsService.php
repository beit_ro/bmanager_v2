<?php

namespace App\Modules\Users\Services;

use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use App\Modules\Users\Models\UserRights;
use Illuminate\Http\Request;


class UserRightsService extends DefaultService
{
    protected $permissionModel;
    protected $usersModel;
    protected $defaultModel;
    const PER_PAGE = 50;

    /**
     * UsersService constructor.
     *
     * @param UserRights $defaultModel
     * @param User $usersModel
     */
    public function __construct(UserRights $defaultModel,
                                User $usersModel
    )
    {
        parent::__construct($defaultModel);
        $this->usersModel = $usersModel;

    }


}
