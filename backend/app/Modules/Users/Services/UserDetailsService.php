<?php

namespace App\Modules\Users\Services;

use App\Helpers\Responser;
use App\Modules\Users\Models\UserDetail;


class UserDetailsService
{
    protected $userDetailModel;


    /**
     * UsersService constructor.
     * @param $userDetailModel
     */
    public function __construct(UserDetail $userDetailModel)
    {
        $this->usersDetailModel = $userDetailModel;

    }

    public function create($params)
    {
        $userDetail = $this->userDetailModel
            ->create($params);
        return $userDetail;
    }

    /**
     * Get all items
     *
     * @param $params
     * @param $paginated
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll($params, $paginated = True)
    {
        $perPage = isset($params['per_page']) ? $params['per_page'] : self::PER_PAGE;
        $query = $this->usersDetailModel->whereNotNull('id');


        $this->attachFilters($params, $query);

        return $paginated ? $query->paginate($perPage) : $query->get();
    }

    protected function attachFilters(array $rawFilters, $query)
    {
        $rawFilters = array_filter($rawFilters);

        if (isset($rawFilters['q'])) {
            $word = $rawFilters['q'];
            $query->where(function ($q) use ($word) {
                $q->where('name', 'like', '%' . $word . '%')
                    ->orWhere('phone', 'like', '%' . $word . '%')
                    ->orWhere('email', 'like', '%' . $word . '%')
                    ->orWhere('address', 'like', '%' . $word . '%');
            });
        }

    }

    /**
     * Get one item
     *
     * @param $id
     * @param $params
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getOne($id, $params = NULL)
    {
        $item = $this->usersDetailModel->find($id);
        if (!$item) {
            return Responser::throwErrors(404, 'Can\'t find the specified resource!');
        }

        return $item;
    }

    public function update($id, $params)
    {
        $item = $this->usersDetailModel
            ->find($id);
        if (!$item)
            return Responser::throwErrors(404, 'Can\'t find the specified resource!');
        $item->update($params);

        return $item;
    }

    /**
     * Delete item
     *
     * @param $id
     * @param $params
     * @return string
     */
    public function delete($id, $params = NULL)
    {

        $item = $this->usersDetailModel->find($id);
        if (!$item) {
            return Responser::throwErrors(404, 'Can\'t find the specified resource!');
        }

        $item->delete();

        return "Resource deleted!";
    }

}