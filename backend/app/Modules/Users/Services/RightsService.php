<?php

namespace App\Modules\Users\Services;

use App\Helpers\Responser;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use App\Modules\Users\Models\UserRights;
use Illuminate\Http\Request;


class RightsService extends DefaultService
{
    protected $permissionModel;
    protected $usersModel;
    protected $defaultModel;
    const PER_PAGE = 50;

    /**
     * UsersService constructor.
     *
     * @param Right $permissionModel
     * @param User $usersModel
     * @param UserDetail $userDetailModel
     */
    public function __construct(Right $defaultModel,
                                User $usersModel
    )
    {
        parent::__construct($defaultModel);
        $this->usersModel = $usersModel;

    }

    public function create($params)
    {
        $userId = UserHelper::getUser();

        $navMenus = [];
        if (isset($params['menus'])) {
            $navMenus = $params['menus'];
        }
        unset($params['menus']);
        $params['owner_id'] = $userId;
        $right = $this->defaultModel->create($params);

        if ($navMenus) {
            NavigationRights::where('right_id', $right->id)->delete();
            foreach ($navMenus as $navMenu) {
                NavigationRights::create(
                    [
                        'right_id' => $right->id,
                        'navigation_id' => $navMenu['id'],
                        'owner_id' => $userId
                    ]);
            }
        }
        return $right;
    }

    public function update($id, $params)
    {
        $navMenus = [];
        if (isset($params['menus'])) {
            $navMenus = $params['menus'];
        }
        unset($params['menus']);
        $right = parent::update($id, $params);

        if ($navMenus) {
            NavigationRights::where('right_id', $right->id)->delete();
            foreach ($navMenus as $navMenu) {
                $userId = UserHelper::getUser();
                NavigationRights::create(
                    [
                        'right_id' => $right->id,
                        'navigation_id' => $navMenu['id'],
                        'owner_id' => $userId
                    ]);
            }
        }
        return $right;
    }


    public function NameSpaces()
    {

        $class_name = [];
        $classes = array_filter(get_declared_classes(), function ($class) {
            $isController = substr($class, -10) == 'Controller';
            $isNotPlainController = $isController && substr($class, -11) != '\Controller';
            return $isNotPlainController;
        });

        //Optional: to clear controller name from its namespace
        $controllers = array_map(function ($controller) {
            return last(explode('\\', $controller));
        }, $classes);

        $controllers = array_values($controllers);

        foreach ($controllers as $controller) {
            $replaced = str_replace('Controller', '', $controller);
            array_push($class_name, $replaced);
        }

        return $class_name;
    }


    public function userRights3(Request $request, $id)
    {
        $rights = UserRights::with('right')->where('user_id', $id)->get();
        return $rights;
    }

    public function userRights(Request $request, $id)
    {
        $userRights = UserRights::where('user_id', $id)->pluck('right_id');
        $rights = Right::whereIn('id', $userRights)->get();

        return $rights;
    }

    public function unique(Request $request)
    {

        if ($request['user_id']) {
            $existingRights = UserRights::where('user_id', $request['user_id'])->pluck('right_id');
            $rights = Right::whereNotIn('id', $existingRights)->get();
        }

        if ($request['group_id']) {
            $existingRights = GroupRights::where('group_id', $request['group_id'])->pluck('right_id');
            $rights = Right::whereNotIn('id', $existingRights)->get();
        }


        return $rights;
    }




    public function removeRight(Request $request)
    {

        if ($request['user_id']) {
            $result= UserRights::where('user_id',$request['user_id'])->where('right_id',$request['right_id'])->first();
            $rfinal=$result->delete();
        }
        if ($request['group_id']) {
            $result= GroupRights::where('group_id',$request['group_id'])->where('group_id',$request['group_id'])->first();
            $rfinalg =$result->delete();
        }

        return $result;
    }


}
