<?php

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNavigationRightsTable extends Migration
{

	const TABLE_NAME = 'navigation_rights';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements('id');

	        $table->bigInteger('navigation_id');
	        //$table->foreign('navigation_id')->references('id')->on('navigation')->onDelete('cascade');

	        $table->bigInteger('right_id');
	       // $table->foreign('right_id')->references('id')->on('rights')->onDelete('cascade');

            $table->unsignedBigInteger('owner_id');
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

	        $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }

}
