<?php

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

	class CreateRightsTable extends Migration
	{
		const TABLE_NAME = 'rights';

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create(self::TABLE_NAME, function (Blueprint $table) {
                $table->bigIncrements('id');
				$table->string('name')->nullable();
				$table->longText('class_namespace')->nullable();
				$table->boolean('C')->nullable();
				$table->boolean('R')->nullable();
				$table->boolean('U')->nullable();
				$table->boolean('D')->nullable();
                $table->unsignedBigInteger('owner_id');
                $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

				$table->timestamps();
				$table->softDeletes();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::drop(self::TABLE_NAME);
		}
	}
