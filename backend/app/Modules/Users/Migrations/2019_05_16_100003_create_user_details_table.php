<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserDetailsTable extends Migration
	{

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('user_details', function (Blueprint $table) {
				$table->bigIncrements('id');


				$table->unsignedBigInteger('user_id');

				$table->longText('name');
				$table->longText('phone')->nullable();
				$table->longText('organization')->nullable();
				$table->longText('nickname')->nullable();
				$table->longText('avatarName')->nullable();
				$table->longText('notes')->nullable();
				$table->longText('address')->nullable();
				$table->longText('code')->nullable();

				$table->boolean('gdpr_accepted')->default(false);

                $table->unsignedBigInteger('organization_id')->unsinged()->nullable();


                $table->unsignedBigInteger('owner_id')->unsinged()->nullable();
                $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

				$table->unsignedBigInteger('country_id');
				$table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


				$table->timestamps();
				$table->softDeletes();

			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::drop('user_details');
		}

	}
