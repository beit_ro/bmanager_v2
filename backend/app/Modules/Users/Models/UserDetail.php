<?php

namespace App\Modules\Users\Models;


use App\Modules\Companies\Models\Company;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Internationalization\Models\Country;
use App\Modules\Locks\Models\Lock;
use App\Modules\Locks\Models\UserKey;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intervention\Image\ImageManagerStatic as Image;

class UserDetail extends Model
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=['id'];

    use Encryptable;

    protected $encryptable = [
        'name',
        'avatarName',
        'birth_date',
    ];

    public $searchable = ['name', 'notes', 'company'];


    protected $appends = ['email','avatar'];


    public function scopeActiveOnly($query)
    {
        return $query->where('status', '=', 1);
    }


    /**
     * Validate the input data
     *
     * @param array $input
     * @return mixed
     */
    public static function validate(array $input)
    {

    }


    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function groups()
    {
        return $this->hasManyThrough(Group::class, UserGroup::class,'user_id', 'id', 'user_id', 'group_id');
    }



    public function getEmailAttribute()
    {
        $User = User::where('id', $this->user_id)->first();
        return $User['email'];
    }

    public function getAvatarAttribute()
    {
//        if($this->avatarName == null)
//        {
//            $path = storage_path() . '/app/avatars/profile.png';
//        }
//        else{
//            $path = storage_path() . '/app/avatars/' . $this->avatarName ;
//        }
      //  $img = Image::make($path)->encode('data-url');
      //  return $img->encoded;
        return '';

    }



}
