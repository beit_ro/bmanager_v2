<?php

namespace App\Modules\Users\Models;

use App\Modules\Users\Models\Right;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRights extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_rights';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $fillable = ['user_id', 'right_id','owner_id'];


    public function right()
    {
        return $this->hasOne(Right::class, 'id','right_id');
    }


}
