<?php

namespace App\Modules\Users\Models;

use App\Modules\Navigation\Models\Navigation;
use App\Modules\Navigation\Models\NavigationRights;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Right extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'class_namespace', 'C', 'R', 'U', 'D','owner_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $casts = [
        'class_namespace' => 'array'
    ];



    public $searchable = [
        'name',
        'class_namespace'
    ];

    public function menus()
    {
        return $this->hasManyThrough(Navigation::class, NavigationRights::class, 'right_id', 'id', 'id', 'navigation_id');
    }


}
