<?php namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;

class RawLog extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'raw_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ip_address', 'request',
        'response', 'url', 'token'];

}
