<?php

namespace App\Modules\Users\Models;


use App\Modules\Groups\Models\Group;
use App\Modules\Users\Services\UserDetailsService;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

class User extends Model
{
    use SoftDeletes;
    use Encryptable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'type',
        'status'
    ];


    protected $encryptable = [
        'email'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];



    /**
     * Validate the input data
     *
     * @param array $input
     * @return mixed
     */
    public static function validate(array $input)
    {
        $validator = Validator::make($input, [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);
        return $validator;
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'user_groups', 'user_id', 'group_id')
            ->withTimestamps()
            ->whereNull('user_groups.deleted_at');
    }


    public function details()
    {
        return $this->hasOne(UserDetail::class, 'user_id');
    }


    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function scopeActiveOnly($query)
    {
        return $query->where('status', '=', 1);
    }


    public function rights()
    {
        return $this->hasManyThrough(Right::class, UserRights::class, 'right_id', 'id', 'id', 'user_id');
    }




}
