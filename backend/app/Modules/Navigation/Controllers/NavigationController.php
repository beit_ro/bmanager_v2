<?php

namespace App\Modules\Navigation\Controllers;


use App\Modules\ Navigation\Services\NavigationService;
use App\Modules\DefaultController;
use Illuminate\Http\Request;

class NavigationController extends DefaultController
{
    public function __construct(NavigationService $defaultService, array $rightsToCheckFor = [])
    {
        parent::__construct($defaultService, $rightsToCheckFor);
    }

    public function getMenu()
    {
       return $this->defaultService->getMenu();
    }

    public function getRights(Request $request)
    {
        return $this->defaultService->getRights($request);
    }

}
