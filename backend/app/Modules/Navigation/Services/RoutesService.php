<?php


namespace App\Modules\ Navigation\Services;



use App\Modules\DefaultService;
use App\Modules\Navigation\Models\Route;


class RoutesService extends DefaultService
{
    /**
     * NotificationsService constructor.
     * @param Route $defaultModel
     */
    public function __construct(Route $defaultModel)
    {
        parent::__construct($defaultModel);
    }


    protected function attachFilters(array $rawFilters, $query)
    {

        parent::attachFilters($rawFilters, $query);
        if (array_key_exists('navigation_id', $rawFilters)) {
            $query = $query->where('navigation_id', $rawFilters['navigation_id']);
        }

        return $query;
    }





}
