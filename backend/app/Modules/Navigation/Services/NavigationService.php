<?php


namespace App\Modules\ Navigation\Services;


use App\Helpers\RightsHelper;
use App\Helpers\TreeHelper;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Navigation\Models\Navigation;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Navigation\Models\Route;


class NavigationService extends DefaultService
{
    /**
     * NotificationsService constructor.
     * @param Navigation $defaultModel
     */
    public function __construct(Navigation $defaultModel)
    {
        parent::__construct($defaultModel);
    }

    public function getMenu()
    {
        $isAdmin = RightsHelper::isAdmin();

        $rights = NavigationRights::where('deleted_at', null)->pluck('navigation_id');

        $object = Navigation::with('children')
            ->where('parent_id', null)
            ->whereIn('id', $rights)
            ->orderBy('position', 'ASC')->get();


        if ($isAdmin == true) {
            $object = Navigation::with('children')
                ->where('parent_id', null)
                ->where('type', 'NOT LIKE', '%system%')
                ->orderBy('position', 'ASC')->get();
        }
        return $object;

    }


    public function getRights($request)
    {
//        $routes = Route::where('route',$request['route'])->first();
//        $rights['crud'] = '0000';
//        if($routes)
//        {
//           // return 'aa';
//            $navId = $routes['navigation_id'];
//            $rights['crud'] = RightsHelper::checkNavRights($navId);
//
//        }
//        else{
//
//            $isAdmin = RightsHelper::isAdmin();
//            if($isAdmin == true){
//                $rights['crud'] = '1111';
//            }
//            else{
//                $rights['crud'] = RightsHelper::checkUserRights();
//            }
//        }
        $reqData = (string)$request['route'];
        $routes = Navigation::where('url', $reqData)->first();
       // return $routes;
        $rights['crud'] = '0000';
        if ($routes) {
            $navId = $routes['id'];
        } else {
            $secondary_routes = Route::where('route', $request['route'])->first();
            if($secondary_routes){
                $navId = $secondary_routes['navigation_id'];
            }
        }
        if(isset($navId)){
            $rights['crud'] = RightsHelper::checkNavRights($navId);
            if($rights['crud'] == '0000'){
                $isAdmin = RightsHelper::isAdmin();
                if($isAdmin == true){
                    $rights['crud'] = '1111';
                }
            }
        }



        return $rights;

    }


}
