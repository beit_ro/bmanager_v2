<?php

namespace App\Modules\Navigation\Models;

use App\Helpers\RightsHelper;
use App\Helpers\UserHelper;
use App\Modules\Users\Models\Right;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

class Navigation extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'navigation';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $fillable = ['title', 'type', 'icon', 'url', 'parent_id', 'position','owner_id'];

    public $searchable = ['title', 'type', 'icon', 'url'];

    protected $appends = ['rights'];


    public function parent()
    {

        return $this->belongsTo(Navigation::class, 'parent_id', 'id');

    }

    public function children()
    {
        $isAdmin = RightsHelper::isAdmin();
        $rights = NavigationRights::where('deleted_at',null)->pluck('navigation_id');
        $result = $this->hasMany(Navigation::class, 'parent_id', 'id')
         ->whereIn('id',$rights)->orderBy('position','ASC');

        if($isAdmin == true)
        {
            $result = $this->hasMany(Navigation::class, 'parent_id', 'id')
                ->orderBy('position','ASC');
        }

        return $result;



    }

    public function rights()
    {
        return $this->hasManyThrough(Right::class, NavigationRights::class,
            'navigation_id', 'id', 'id', 'right_id');


    }


    public function getRightsAttribute()
    {

        return RightsHelper::checkNavRights($this->id);

    }

}
