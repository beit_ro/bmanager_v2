<?php

namespace App\Modules\Navigation\Models;

use App\Helpers\RightsHelper;
use App\Helpers\UserHelper;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

class Route extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routes';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $fillable = ['route', 'navigation_id','owner_id'];

    public $searchable = ['route'];



    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }




}
