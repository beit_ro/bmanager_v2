<?php


use App\Modules\Navigation\Controllers\NavigationController;
use App\Modules\Navigation\Controllers\RoutesController;


Route::group(
    [
       // 'middleware' => 'auth', 'right'
    ], function()
    {
        Route::group([
            'prefix' => 'navigation'
        ], function () {
            Route::get('menu', NavigationController::getShortClassName() . '@getMenu');
            Route::get('rights', NavigationController::getShortClassName() . '@getRights');
        });

        Route::resources([
		    'navigation' => NavigationController::getShortClassName(),
		    'routes' => RoutesController::getShortClassName(),
	    ]);

    }
);
