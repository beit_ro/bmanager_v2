<?php


use App\Modules\Persons\Controllers\PersonsController;

Route::group([
    'middleware' => ['auth','right'],
], function () {

    Route::group([
        'prefix' => 'persons'
    ], function () {

        Route::post('pass/{id}', PersonsController::getShortClassName() . '@updatePass');
    });

    Route::resources([
        'persons' => PersonsController::getShortClassName()
    ]);



});


