<?php


namespace App\Modules\Persons\Models;


use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\User;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\ImageManagerStatic as Image;

class Person extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_details';


    use Encryptable;

    protected $encryptable = [
        'name',
        'phone',
        'address',
        'avatarName',
        'birth_date',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'company',
        'job_title',
        'address',
        'notes',
        'code',
        'nickname',
        'avatarName',
        'birth_date',
        'gender',
        'country_id',
        'user_id',
        'tos_accepted',
        'owner_id',

    ];
    protected $appends = ['email','avatar','status'];


    public $searchable = ['phone', 'company', 'job_title', 'address', 'notes', 'code', 'nickname'];


    public function groups()
    {
        return $this->belongsToMany(Group::class, 'user_groups', 'user_id', 'group_id', 'user_id')
            ->withTimestamps()
            ->whereNull('user_groups.deleted_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user_groups()
    {
        return $this->belongsTo(UserGroup::class, 'user_id', 'user_id');
    }

    public function getEmailAttribute()
    {
        $result = User::where('id', $this->user_id)->select('email')->first();
        return $result['email'];
    }

    public function getStatusAttribute()
    {
        $result = User::where('id', $this->user_id)->select('status')->first();
        return $result['status'];
    }


    public function getAvatarAttribute()
    {
//        if($this->avatarName == null)
//        {
//            $path = storage_path() . '/app/avatars/profile.png';
//        }
//        else{
//            $path = storage_path() . '/app/avatars/' . $this->avatarName ;
//        }
//
//        //return $path;
//        $img = Image::make($path)->encode('data-url');
       // return $img->encoded;

        return '';

    }

}
