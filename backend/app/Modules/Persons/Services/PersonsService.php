<?php


namespace App\Modules\Persons\Services;


use App\Helpers\EmailHelper;
use App\Helpers\Responser;
use App\Helpers\UserHelper;
use App\Modules\DefaultService;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Internationalization\Models\Country;
use App\Modules\Persons\Models\Person;
use App\Modules\Users\Models\User;
use Illuminate\Support\Facades\Mail;


class PersonsService extends DefaultService
{

    private $usersModel;

    public function __construct(Person $defaultModel, User $usersModel)

    {
        $this->usersModel = $usersModel;
        parent::__construct($defaultModel);
    }

    public function updatePass($id, $request)
    {

            if($request['password']!='')
            {
                $pass = bcrypt($request['password']);
                $user = User::where('id',$id)->first();
                $update = $user->update(array('password' => $pass));
            }
            else{
                return Responser::throwErrors(403, 'Empty password!');
            }




    }


    protected function attachFilters(array $rawFilters, $query)
    {

        parent::attachFilters($rawFilters, $query);
        if (array_key_exists('groups', $rawFilters)) {
            $query = $query->whereHas('groups', function ($q) use ($rawFilters) {
                $q->where('code', $rawFilters['groups']);
            });
        }

        return $query;
    }


    public function create($params)
    {

        unset($params['avatar']);
        if($params['password']!='')
        {
            $params['password'] = bcrypt($params['password']);
        }
        else{
            $params['password'] = bcrypt('password');
        }
        $user = $this->usersModel
            ->create($params);

        $params['user_id'] = $user['id'];
        $params['country_id'] = '229';
        $this->defaultModel->create($params);

        $userDetail = $this->defaultModel
            ->where('user_id', $user['id'])
            ->first();

        if (!$userDetail) {
            $params['user_id'] = $user['id'];
            $params['country_id'] = '229';

            return $params;
            $this->defaultModel->create($params);
        } else {
            if (isset($params['user_id']))
            $userDetail->update($params);
            $userDetail = Person::where('user_id', $params['user_id'])->first();
            $userEmail = User::where('id', $params['user_id'])->first();
            $userDetail['email'] = $userEmail['email'];
        }
        $country=Country::where('id', $userDetail->country_id)->first();
        $blade = $country->code . '.emails.register';

        EmailHelper::sendEmail($blade,$userDetail,"Register");


        return $userDetail;


    }

    public function update( $params, $id)
    {



        unset($params['avatar']);
        if($params['password']!='')
        {
            $params['password'] = bcrypt($params['password']);
        }
        else{
            $params['password'] = bcrypt('password');
        }

        $user = $this->usersModel
            ->find($id);

        $user->update($params);

        $params['user_id'] = $id;
        $params['country_id'] = '229';

        $userDetail = $this->defaultModel
            ->where('user_id', $user['id'])
            ->first();

             $userDetail->update($params);
        return $userDetail;
    }


}
