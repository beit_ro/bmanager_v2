<?php

namespace App\Modules\Notifications\Controllers;


use App\Modules\DefaultController;
use App\Modules\Notifications\Services\NotificationsService;


class NotificationsController extends DefaultController
{

    public function __construct(NotificationsService $defautService)
    {

        parent::__construct($defautService);
    }


}
