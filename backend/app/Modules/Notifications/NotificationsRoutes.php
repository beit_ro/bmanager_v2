<?php


use App\Modules\Notifications\Controllers\NotificationsController;

Route::group([
    'middleware' => ['auth','right'],
], function () {

    Route::resources([
        'notifications' => NotificationsController::getShortClassName()
    ]);



});


