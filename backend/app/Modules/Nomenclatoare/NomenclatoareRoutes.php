<?php


use App\Modules\Nomenclatoare\Controllers\NomenExpensesTypeController;

Route::group([
    'middleware' => ['auth','right'],
], function () {

    Route::resources([
      'expenses_type' => NomenExpensesTypeController::getShortClassName()
    ]);



});


