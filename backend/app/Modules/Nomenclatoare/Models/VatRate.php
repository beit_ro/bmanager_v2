<?php


namespace App\Modules\Nomenclatoare\Models;


use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\ImageManagerStatic as Image;

class VatRate extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nomen_vat_rate';


    use Encryptable;

    protected $encryptable = [

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'owner_id'

    ];

    public $searchable = ['name', 'description'];


    public function user()
    {
        return $this->belongsTo(UserDetail::class, 'owner_id','user_id');
    }


}
