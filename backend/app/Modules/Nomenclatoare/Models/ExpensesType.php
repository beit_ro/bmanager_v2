<?php


namespace App\Modules\Nomenclatoare\Models;

use App\Modules\Users\Models\UserDetail;
use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpensesType extends Model
{
    use SoftDeletes;


    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nomen_expenses_type';


    use Encryptable;

    protected $encryptable = [

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'owner_id',

    ];

    public $searchable = ['name', 'description'];


    public function user()
    {
        return $this->belongsTo(UserDetail::class, 'owner_id','user_id');
    }


}
