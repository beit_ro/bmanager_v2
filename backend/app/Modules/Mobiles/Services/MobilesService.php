<?php


namespace App\Modules\Mobiles\Services;


use App\Modules\DefaultService;
use App\Modules\Info\Models\Info;
use App\Modules\Notifications\Models\ExpensesType;
use App\Modules\mobiles\Models\mobile;
use App\Modules\Records\Models\Record;
use App\Modules\Records\Models\SymptomRecord;
use App\Modules\Symptoms\Models\Symptom;
use GuzzleHttp\Client;


class MobilesService extends DefaultService
{


    public function __construct(Info $defaultModel)

    {
        parent::__construct($defaultModel);
    }


}
