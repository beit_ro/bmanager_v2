<?php

namespace App\Modules\Mobiles\Controllers;


use App\Modules\DefaultController;
use App\Modules\Mobiles\Services\MobilesService;
use Illuminate\Http\Request;


/**
 * @package App\Modules\Tenants\Controllers
 */
class MobilesController extends DefaultController
{

    protected $servicesMobile;

		/**
         *
         * @param $servicesMobile
         * */

    public function __construct(MobilesService $servicesMobile)
    {
        $this->servicesMobile = $servicesMobile;

        parent::__construct($servicesMobile);
    }

    public function getQa(Request $request)
    {
        $params = $request->all();
        return $this->servicesMobile->getQa($params , $paginated = true, $relationships = NULL);
    }

    public function getNews(Request $request)
    {
        $params = $request->all();
        return $this->servicesMobile->getNews($params , $paginated = true, $relationships = NULL);
    }

    public function getSymptoms(Request $request)
    {
        $params = $request->all();
        return $this->servicesMobile->getSymptoms($params , $paginated = true, $relationships = NULL);
    }

    public function getResults(Request $request)
    {
        $params = $request->all();
        return $this->servicesMobile->getResults($params);
    }

    public function saveResults(Request $request)
    {
        return $this->servicesMobile->saveResults($request);
    }

}
