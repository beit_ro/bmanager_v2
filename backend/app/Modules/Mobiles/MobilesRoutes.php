<?php


use App\Modules\Mobiles\Controllers\MobilesController;

Route::group([
   // 'middleware' => ['auth','right'],
], function () {


    Route::group([
        'prefix' => 'mobiles'
    ], function () {

        Route::get('qa', MobilesController::getShortClassName() . '@getQa');
        Route::get('news', MobilesController::getShortClassName() . '@getNews');
        Route::get('symptoms', MobilesController::getShortClassName() . '@getSymptoms');
        Route::post('results', MobilesController::getShortClassName() . '@saveResults');
    });

    Route::resources([
        'mobiles' => MobilesController::getShortClassName()
    ]);



});


