<?php


namespace App\Modules\Internationalization\Services;


use App\Modules\DefaultService;
use App\Modules\Internationalization\Models\City;
use App\Modules\Internationalization\Models\Country;

class CountriesService extends DefaultService
{
    public function __construct(Country $defaultModel ,City $cityModel)
    {
        parent::__construct($defaultModel,$cityModel);
    }

    protected function attachFilters(array $rawFilters, $query)
    {

        $query->orderBy('name', 'ASC');
    }

    public function getAvailableCities($params)
    {
        $cities = City::get();
        $cities = $cities->map(function ($el) {
            return [
                'id' => $el->id,
                'name' => $el->name,
                'code' => strtolower($el->code)
            ];
        });

        return $cities;
    }


	public function getCurrentCountry($params)
	{
		if (!isset($params['country_code'])) {
			$params['country_code'] = 'us';
		}

		$country = $this->defaultModel
			->where('code', $params['country_code'])
			->where('available', 1)
			->first();

		if (!$country) {
			$country = $this->defaultModel->with('currency')
				->where('code', 'us')
				->first();
		}

		return [
			'id' => $country->id,
			'name' => $country->name,
			'code' => strtolower($country->code)
		];

	}




}
