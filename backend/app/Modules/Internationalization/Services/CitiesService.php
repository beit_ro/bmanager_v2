<?php


namespace App\Modules\Internationalization\Services;


use App\Modules\DefaultService;
use App\Modules\Internationalization\Models\City;
use App\Modules\Internationalization\Models\Country;

class CitiesService extends DefaultService
{
    public function __construct(City $defaultModel)
    {
        parent::__construct($defaultModel);
    }

    protected function attachFilters(array $rawFilters, $query)
    {

        $query->orderBy('name', 'ASC');
    }

    public function getAvailableCities($params)
    {
        $cities = $this->defaultModel
            ->where('available', 1);
        $cities = $cities->get();

        return $cities;
    }

    public function getOne($id, $params = NULL,$relationships = NULL)
    {
        $item = $this->defaultModel->find($id);
        if (!$item) {
            return Responser::throwErrors(404, 'Can\'t find the specified resource!');
        }

        return $item;
    }

    public function create($params)
    {

        if ($params->hasFile('cover')) {
            $cover = $params->file('cover')->getClientOriginalExtension();
            $params->file('cover')->move(storage_path('app/covers/'), $cover);
            $img = Image::make(storage_path('app/avatars/') . $cover);
            $img->resize(400, 200);
            $img->save();

            $params['cover'] = $cover;
        }
        $item =parent::create($params);
        return $item;
    }


}