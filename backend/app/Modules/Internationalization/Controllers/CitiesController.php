<?php


namespace App\Modules\Internationalization\Controllers;


use App\Modules\DefaultController;
use App\Modules\Internationalization\Services\CitiesService;
use App\Modules\Internationalization\Services\CountriesService;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CitiesController extends DefaultController
{
    public function __construct(CitiesService $cityService, array $rightsToCheckFor = [])
    {
        $this->cityService = $cityService;
        parent::__construct($cityService, $rightsToCheckFor);
    }


    public function getAvailableCities(Request $request)
    {
        $params = $request->all();

        return $this->defaultService->getAvailableCities($params);
    }


    public function store(Request $request)
    {
        $params = $request->all();
        return $this->cityService->store($params);

    }


    public function getCover(Request $request, $id)
    {
        $city = $this->cityService->getOne($id);
       // dd($city);
        $path = storage_path() . '/app/covers/' . $city->cover;
        $img = Image::make($path);
        $img->resize(400, 200);

        return $img->response('png');
    }




}