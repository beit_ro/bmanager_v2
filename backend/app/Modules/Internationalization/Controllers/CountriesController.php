<?php


namespace App\Modules\Internationalization\Controllers;


use App\Modules\DefaultController;
use App\Modules\Internationalization\Services\CountriesService;
use Illuminate\Http\Request;

class CountriesController extends DefaultController
{
    public function __construct(CountriesService $countryService, array $rightsToCheckFor = [])
    {
        parent::__construct($countryService, $rightsToCheckFor);
    }


    public function getAvailableCities(Request $request)
    {
        $params = $request->all();

        return $this->defaultService->getAvailableCities($params);
    }

	public function getCurrentCountry(Request $request)
	{
		$params = $request->all();

		return $this->defaultService->getCurrentCountry($params);
	}
}
