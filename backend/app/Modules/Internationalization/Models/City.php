<?php

namespace App\Modules\Internationalization\Models;

use App\Modules\Companies\Models\Building;
use App\Modules\Companies\Models\BuildingOffice;
use App\Modules\Shop\Models\Product;
use App\Modules\Shop\Models\ProductOffice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'code', 'country_id', 'available', 'cover'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

	public function country()
	{
		return $this->hasOne(Country::class,'id','country_id');
	}

}
