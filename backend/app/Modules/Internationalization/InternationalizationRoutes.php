<?PHP


	use App\Modules\Internationalization\Controllers\CitiesController;
	use App\Modules\Internationalization\Controllers\CountriesController;


Route::get('countries/available', CountriesController::getShortClassName() . '@getAvailableCities');
Route::get('countries/current', CountriesController::getShortClassName() . '@getCurrentCountry');
Route::get('cities/{id}/cover', CitiesController::getShortClassName() . '@getCover');

Route::resources([
    'countries' => CountriesController::getShortClassName(),
	'cities' => CitiesController::getShortClassName(),


]);

Route::group([
    'middleware' => 'auth'
], function () {


});
