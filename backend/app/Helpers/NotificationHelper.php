<?php


namespace App\Helpers;


use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Notifications\Models\ExpensesType;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserRights;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class NotificationHelper
{
    public static function recordNotification($type, $description, $users)
    {
        $userId = UserHelper::getUser();
        if ($type == 'everyone') {
            $notification = ExpensesType::create([
                'description' => $description,
                'owner_id' => $userId,
                'type' => $type
            ]);
            return $notification;
        } else {
            if ($type == 'selected') {
                foreach ($users as $item) {
                    $notification = ExpensesType::create([
                        'description' => $description,
                        'owner_id' => $userId,
                        'type' => $item
                    ]);
                }
                return $notification;
            }

        }



    }


}
