<?php


namespace App\Helpers;


use App\Modules\Files\Models\File;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserRights;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use ZipArchive;

class FileHelper
{
    public static function zipFiles($fileIds, $userId = NULL)
    {
        $serverFiles = File::whereIn('id', $fileIds)
            ->get();

        $relPath = storage_path('app/files/temp');
        $zipname = 'Archive' . '_' . time() . '.zip';


        $zip = new ZipArchive();
        $zip->open($zipname, ZipArchive::CREATE);

        //Archive server files
        foreach ($serverFiles as $file) {
            $path = $relPath . $file->path;
            if (file_exists($path)) {
                $zip->addFile($path, $file->name);
            }

        }
        $zip->close();


    //    $newPath = storage_path("app/files/zip" . $zipname);
     //   copy($zipname, $newPath);
//        $newFile = File::create([
//            'name' => $zipname,
//            'path' => $zipname,
//            'extension' => 'zip',
//            'barcode' => Helper::getBarcodeNumber(),
//            'checksum' => md5_file(storage_path('app/files/temp/' . $zipname)),
//            'user_id' => $userId
//        ]);

        return response()->download($zip);
    }


}
