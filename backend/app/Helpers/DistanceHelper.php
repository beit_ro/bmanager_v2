<?php
/**
 * Created by PhpStorm.
 * User: dexterionut
 * Date: 27/01/2018
 * Time: 02:16
 */

namespace App\Helpers;


class DistanceHelper
{
    /**
     * Calculate the distance between two lat,lng points using
     * 
     *
     *
     * Form the array like this:
     * <code>
     * $point1 = $point2 = array(
     *   'lat'   => degreeLatNumber,          // the lat in degrees
     *   'lng'   => degreeLngNumber,         // the lng in degrees
     * );
     *
     * </code>
     *
     * @param $point1[string] double
     * @param $point2[string] double
     * @return float|int
     */
    public static function getDistanceBetweenTwoPoints($point1, $point2)
    {
        $earthRadius = 6371;  // earth radius in km
        $point1Lat = $point1['lat'];
        $point2Lat = $point2['lat'];
        $deltaLat = deg2rad($point2Lat - $point1Lat);
        $point1Lng = $point1['lng'];
        $point2Lng = $point2['lng'];
        $deltaLng = deg2rad($point2Lng - $point1Lng);
        $angle = sin($deltaLat / 2) * sin($deltaLat / 2)
            + cos(deg2rad($point1Lat)) * cos(deg2rad($point2Lat)) * sin($deltaLng / 2) * sin($deltaLng / 2);
        $c = 2 * atan2(sqrt($angle), sqrt(1 - $angle));

        $distance = $earthRadius * $c;
        return $distance;    // in km
    }
}