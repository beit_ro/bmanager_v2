<?php


namespace App\Helpers;


use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Suppliers\Models\Supplier;
use App\Modules\Suppliers\Models\SupplierUser;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use App\Modules\Users\Models\UserRights;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserHelper
{

    public static function getUser()
    {
        $request = request();
        $externalIp = $_SERVER['REMOTE_ADDR'];
        $internalIp = $request->ip();

        $session = Session::where(function ($q) use ($request) {
            $q->where('token', '=', $request->header(Auth::HEADER_NAME))
                ->orWhere('token', '=', $request->input(Auth::TOKEN_NAME));
        })
            ->where('expires_at', '>', Carbon::now())
            ->where('external_ip', $externalIp)
            ->where('internal_ip', $internalIp)
            ->first();


        if (!$session) {
            $user = null;
        } else {
            $user = $session->user_id;
        }


        return $user;
    }

    public static function getUserDetails($id)
    {
        $user = UserDetail::where('user_id',$id)->first();
        return $user;
    }

    public static function getUserGroups()
    {
        $userId = self::getUser();

        $groups = UserGroup::where('user_id', $userId)->pluck('group_id');

        return $groups;
    }

    public function childrenOf($item)
    {
        $result = array();
        foreach ($this->items as $i) {
            if ($i->parent_id == $item->id) {
                $result[] = $i;
            }
        }
        return $result;
    }

    public function itemWithChildren($item)
    {
        $result = array();
        $children = $this->childrenOf($item);
        foreach ($children as $child) {
            $result[$child->name] = $this->itemWithChildren($child);
        }
        return $result;
    }

    public function parent()
    {
        return $this->belongsTo('App\CourseModule','parent_id')->where('parent_id',0)->with('parent');
    }

    public function children()
    {
        return $this->hasMany('App\CourseModule','parent_id')->with('children');
    }

//    public static function getUserCompany()
//    {
//        $userId = self::getUser();
//        $supplierData = SupplierUser::with('supplier')->where('user_id', $userId)->first();
//        return $supplierData['supplier'];
//    }

}
