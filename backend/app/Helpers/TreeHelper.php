<?php

namespace App\Helpers;
class TreeHelper
{




    public function childrenOf($item)
    {
        $result = array();
        foreach ($item as $i) {
            if ($i->parent_id == $item->id) {
                $result[] = $i;
            }
        }
        return $result;
    }

    public function itemWithChildren($item)
    { //return $item;
        $result = array();
        $children = $this->childrenOf($item);

        foreach ($children as $child) {
            $result[$child->name] = $this->itemWithChildren($child);
        }
        return $result;
    }

}
