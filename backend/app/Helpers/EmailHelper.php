<?php


namespace App\Helpers;


use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Navigation\Models\NavigationRights;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserRights;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailHelper
{
    public static function sendEmail($blade, $user, $subject)
    {
        $email = Mail::send($blade,['user' => $user], function ($message) use ($user, $subject) {
            $message->from('no_reply@eumarines.ul.ie', 'Eumarines Platform');
            $message->to($user->email, $user->name)->subject($subject);
        });
        return $email;
    }


    public static function sendCustomEmail($blade, $data, $subject,$email)
    {
        $email = Mail::send($blade,['data' => $data], function ($message) use ($data, $subject,$email) {
            $message->from('no_reply@eumarines.ul.ie', 'Eumarines Platform');
            $message->to($email, $data->name)->subject($subject);
        });
        return $email;
    }



}
