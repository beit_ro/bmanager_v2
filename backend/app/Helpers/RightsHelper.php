<?php


namespace App\Helpers;


use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Users\Middleware\Auth;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\Session;
use App\Modules\Users\Models\UserRights;
use Carbon\Carbon;

class RightsHelper
{


    public static function checkUserRights()
    {
        $finalObj = [];
        $result = [0, 0, 0, 0];
        $counter = 0;
        $userId = self::userId();


        $userRights = UserRights::where('user_id', $userId)->pluck('right_id')->toArray();
        $userGroups = UserGroup::where('user_id', $userId)->pluck('group_id')->toArray();
        $groupRights = GroupRights::whereIn('group_id', $userGroups)->pluck('right_id')->toArray();
        $rightsArray = array_unique(array_merge($userRights, $groupRights));
        $rights = Right::whereIn('id', $rightsArray)->get();


        foreach ($rights as $item) {
            foreach ($item['class_namespace'] as $rightItem) {
                $counter++;
                $finalObj['id'] = $counter;
                $finalObj['class'] = $rightItem;
                $finalObj['crud'] = $item['C'] . $item['R'] . $item['U'] . $item['D'];
                $finalObj['right_id'] = $item['id'];
                $column = array_column($result, 'class');
                $key = array_search($rightItem, $column, TRUE);
                if ($key !== false) {
                    if ($finalObj['crud'][0] == 1) {
                        $result[$key]['crud'][0] = 1;
                    }
                    if ($finalObj['crud'][1] == 1) {
                        $result[$key]['crud'][1] = 1;
                    }
                    if ($finalObj['crud'][2] == 1) {
                        $result[$key]['crud'][2] = 1;
                    }
                    if ($finalObj['crud'][3] == 1) {
                        $result[$key]['crud'][3] = 1;
                    }
                } else {
                    array_push($result, $finalObj);
                }
            }
        }



        return $result;
    }


    public static function checkClassRights($class)
    {

        $finresult = "0000";
        $finalObj = [];
        $result = [];
        $counter = 0;
        $userId = self::userId();
        $isAdmin = self::isAdmin();

        $userRights = UserRights::where('user_id', $userId)->pluck('right_id')->toArray();
        $userGroups = UserGroup::where('user_id', $userId)->pluck('group_id')->toArray();
        $groupRights = GroupRights::whereIn('group_id', $userGroups)->pluck('right_id')->toArray();
        $rightsArray = array_unique(array_merge($userRights, $groupRights));
        $rights = Right::whereIn('id', $rightsArray)->get();


        foreach ($rights as $item) {
            foreach ($item['class_namespace'] as $rightItem) {
                $counter++;
                $finalObj['id'] = $counter;
                $finalObj['class'] = $rightItem;
                $finalObj['crud'] = $item['C'] . $item['R'] . $item['U'] . $item['D'];
                $finalObj['right_id'] = $item['id'];
                $column = array_column($result, 'class');
                $key = array_search($rightItem, $column, TRUE);

                if ($key !== false) {
                    if ($finalObj['crud'][0] == 1) {
                        $result[$key]['crud'][0] = 1;
                    }
                    if ($finalObj['crud'][1] == 1) {
                        $result[$key]['crud'][1] = 1;
                    }
                    if ($finalObj['crud'][2] == 1) {
                        $result[$key]['crud'][2] = 1;
                    }
                    if ($finalObj['crud'][3] == 1) {
                        $result[$key]['crud'][3] = 1;
                    }
                } else {
                    array_push($result, $finalObj);
                }
            }
        }


        $columnResult = array_column($result, 'class');
        $response = array_search($class, $columnResult, TRUE);
        if ($response !== false) {
            $finresult = $result[$response]['crud'];
        }

      if($isAdmin == true)
      {
          $finresult = "1111";
      }
        return $finresult;
    }


    public static function checkNavRights($navId)
    {

        $userId = self::userId();
        $finales = "0000";
        $stringRight = [];
        $finalObj = [];
        $result = [];
        $isAdmin = self::isAdmin();

        $userRights = UserRights::where('user_id', $userId)->pluck('right_id')->toArray();
        $userGroups = UserGroup::where('user_id', $userId)->pluck('group_id')->toArray();
        $groupRights = GroupRights::whereIn('group_id', $userGroups)->pluck('right_id')->toArray();
        $rightsArray = array_unique(array_merge($userRights, $groupRights));
        $rights = Right::whereIn('id', $rightsArray)->get();


        foreach ($rights as $item) {
            $stringRight['crud'] = $item['C'] . $item['R'] . $item['U'] . $item['D'];
            array_push($finalObj, $stringRight);

        }

        foreach ($finalObj as $key => $object) {

            if ($object['crud'][0] == 1) {
                $finales[0] = 1;
            }
            if ($object['crud'][1] == 1) {
                $finales[1] = 1;
            }
            if ($object['crud'][2] == 1) {
                $finales[2] = 1;
            }
            if ($object['crud'][3] == 1) {
                $finales[3] = 1;
            }
        }

        if($isAdmin == true)
        {
            $finales = "1111";
        }
        return $finales;
    }


    public static function isAdmin()
    {

        $esteAdmin = false;
        $userId = self::userId();
        $adminGroup = Group::where('code', 'ADM')
            ->where('system', 1)
            ->select('id')
            ->first();
        $isAdmin = UserGroup::where('user_id', $userId)->where('group_id', $adminGroup['id'])->first();

        if ($isAdmin) {
            $esteAdmin = true;
        }
        return $esteAdmin;

    }


    private static function userId()
    {

        $request = request();
        $externalIp = $_SERVER['REMOTE_ADDR'];
        $internalIp = $request->ip();


        $session = Session::where(function ($q) use ($request) {
            $q->where('token', '=', $request->header(Auth::HEADER_NAME))
                ->orWhere('token', '=', $request->input(Auth::TOKEN_NAME));
        })
            ->where('expires_at', '>', Carbon::now())
            ->where('external_ip', $externalIp)
            ->where('internal_ip', $internalIp)
            ->first();

        if (!$session) {
            $userId = null;
        } else {
            $userId = $session->user_id;
        }

        return $userId;
    }


}
