<?php
	/**
	 * Created by PhpStorm.
	 * User: penicamihai
	 * Date: 16/08/2018
	 * Time: 13:39
	 */

	namespace App\Listeners;


	use App\Events\DeviceHasEvent;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Support\Facades\Redis;

	class NotifyRedis implements ShouldQueue
	{
		public function __construct()
		{
			//
		}

		/**
		 * Handle the event.
		 *
		 * @param $event
		 * @return void
		 */

		public function handle(DeviceHasEvent $event)
		{
			$message = $event->getDeviceEvent();

			$data = [
				'event' => 'DEVICE.EVENT_RECEIVED',
				'data' => $message->toArray(),
				'user_id' => $message->user_id

			];
			Redis::publish('message', json_encode($data));

			/*$notification = AdminNotification::create([
				'reference' => $message->supportSession->reference,
				'type' => 'SUPPORT_MESSAGE_RECEIVED',
				'content' => 'A message was received on ' . $message->supportSession->reference . '!'
			]);*/

			$data = [
				'event' => 'NOTIFICATION.NEW',
				//'data' => $notification->toArray(),
			];
			Redis::publish('message', json_encode($data));
		}


	}