<?php
	/**
	 * Created by PhpStorm.
	 * User: penicamihai
	 * Date: 16/08/2018
	 * Time: 13:52
	 */

	namespace App\Events;


	class DeviceHasEvent extends Event
	{
		use SerializesModels;

		private $event;

		/**
		 * Create a new event instance.
		 *
		 * @param Message $message
		 */
		public function __construct(DeviceEvent $event)
		{
			$this->deviceEvent = $event;
		}

		/**
		 * Get the channels the event should be broadcast on.
		 *
		 * @return array
		 */
		public function broadcastOn()
		{
			return [];
		}

		/**
		 * @return Order
		 */
		public function getDeviceEvent()
		{
			return $this->event;
		}
	}

