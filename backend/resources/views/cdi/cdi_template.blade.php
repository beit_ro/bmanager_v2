{!! '<'.'?xml version="1.0"?>' !!}
<!-- this file has been created using MIKADO version 3.6.2 -->
{!! '<'.'?xml-model href="https://schemas.seadatanet.org/Standards-Software/Metadata-formats/SDN_CDI_ISO19139_12.2.0.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>' !!}
{!! '<'.'gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd"  xmlns:gmi="http://www.isotc211.org/2005/gmi"  xmlns:srv="http://www.isotc211.org/2005/srv"  xmlns:gco="http://www.isotc211.org/2005/gco"  xmlns:gts="http://www.isotc211.org/2005/gts"  xmlns:gmx="http://www.isotc211.org/2005/gmx"  xmlns:xlink="http://www.w3.org/1999/xlink"  xmlns:sdn="http://www.seadatanet.org"  xmlns:gml="http://www.opengis.net/gml"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xsi:schemaLocation="http://www.seadatanet.org https://schemas.seadatanet.org/Standards-Software/Metadata-formats/SDN_CDI_ISO19139_12.2.0.xsd http://www.isotc211.org/2005/gmd http://schemas.opengis.net/iso/19139/20060504/gmd/gmd.xsd" >' !!}
    <gmd:fileIdentifier>
        <gco:CharacterString>urn:SDN:CDI:LOCAL:WellHead_Kinsale</gco:CharacterString>
    </gmd:fileIdentifier>
    <gmd:language>
        <gmd:LanguageCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#LanguageCode"  codeListValue="eng"  codeSpace="ISOTC211/19115" >English</gmd:LanguageCode>
    </gmd:language>
    <gmd:characterSet>
        <gmd:MD_CharacterSetCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_CharacterSetCode"  codeListValue="utf8"  codeSpace="ISOTC211/19115" >utf8</gmd:MD_CharacterSetCode>
    </gmd:characterSet>
    <gmd:hierarchyLevel>
        <gmd:MD_ScopeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_ScopeCode"  codeListValue="dataset"  codeSpace="ISOTC211/19115" >dataset</gmd:MD_ScopeCode>
    </gmd:hierarchyLevel>
    <gmd:hierarchyLevelName>
        <sdn:SDN_HierarchyLevelNameCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_HierarchyLevelNameCode"  codeListValue="CDI"  codeSpace="SeaDataNet" >Common Data Index record</sdn:SDN_HierarchyLevelNameCode>
    </gmd:hierarchyLevelName>
    <gmd:contact>
        <gmd:CI_ResponsibleParty>
            <gmd:organisationName>
                <sdn:SDN_EDMOCode codeList="https://edmo.seadatanet.org/isocodelists/edmo-edmerp-codelists.xml#SDN_EDMOCode"  codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->code}}" >{{$view['organization']->organisationname}}</sdn:SDN_EDMOCode>
            </gmd:organisationName>
            <gmd:contactInfo>
                <gmd:CI_Contact>
                    <gmd:phone>
                        <gmd:CI_Telephone>
                            <gmd:voice>
                                <gco:CharacterString>{{$view['organization']->voice}}</gco:CharacterString>
                            </gmd:voice>
                            <gmd:facsimile>
                                <gco:CharacterString>{{$view['organization']->facsimile}}</gco:CharacterString>
                            </gmd:facsimile>
                        </gmd:CI_Telephone>
                    </gmd:phone>
                    <gmd:address>
                        <gmd:CI_Address>
                            <gmd:deliveryPoint>
                                <gco:CharacterString>{{$view['organization']->address}} </gco:CharacterString>
                            </gmd:deliveryPoint>
                            <gmd:city>
                                <gco:CharacterString>{{$view['organization']->city}}</gco:CharacterString>
                            </gmd:city>
                            <gmd:postalCode>
                                <gco:CharacterString>{{$view['organization']->postalCode}}</gco:CharacterString>
                            </gmd:postalCode>
                            <gmd:country>
                                <sdn:SDN_CountryCode codeSpace="SeaDataNet"  codeListValue="IE"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_CountryCode" >Ireland</sdn:SDN_CountryCode>
                            </gmd:country>
                            <gmd:electronicMailAddress>
                                <gco:CharacterString>{{$view['organization']->country}}</gco:CharacterString>
                            </gmd:electronicMailAddress>
                        </gmd:CI_Address>
                    </gmd:address>
                    <gmd:onlineResource>
                        <gmd:CI_OnlineResource>
                            <gmd:linkage>
                                <gmd:URL>{{$view['organization']->url}}</gmd:URL>
                            </gmd:linkage>
                        </gmd:CI_OnlineResource>
                    </gmd:onlineResource>
                </gmd:CI_Contact>
            </gmd:contactInfo>
            <gmd:role>
                <gmd:CI_RoleCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_RoleCode"  codeListValue="pointOfContact"  codeSpace="ISOTC211/19115" >pointOfContact</gmd:CI_RoleCode>
            </gmd:role>
        </gmd:CI_ResponsibleParty>
    </gmd:contact>
    <gmd:dateStamp>
        <gco:Date>{{$view['ci_date']}}</gco:Date>
    </gmd:dateStamp>
    <gmd:metadataStandardName>
        <gco:CharacterString>ISO 19115/SeaDataNet profile</gco:CharacterString>
    </gmd:metadataStandardName>
    <gmd:metadataStandardVersion>
        <gco:CharacterString>1.0</gco:CharacterString>
    </gmd:metadataStandardVersion>
    <gmd:spatialRepresentationInfo>
        <gmd:MD_VectorSpatialRepresentation>
            <gmd:geometricObjects>
                <gmd:MD_GeometricObjects>
                    <gmd:geometricObjectType>
                        <gmd:MD_GeometricObjectTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_GeometricObjectTypeCode"  codeListValue="{{$view['MD_GeometricObjectTypeCode']->identifier}}"  codeSpace="{{$view['MD_GeometricObjectTypeCode']->iso}}" >{{$view['MD_GeometricObjectTypeCode']->identifier}}</gmd:MD_GeometricObjectTypeCode>
                    </gmd:geometricObjectType>
                </gmd:MD_GeometricObjects>
            </gmd:geometricObjects>
        </gmd:MD_VectorSpatialRepresentation>
    </gmd:spatialRepresentationInfo>
    <gmd:spatialRepresentationInfo>
        <gmd:MD_GridSpatialRepresentation>
            <gmd:numberOfDimensions>
                <gco:Integer>{{$view['numberOfDimensions']}}</gco:Integer>
            </gmd:numberOfDimensions>
            <gmd:cellGeometry>
                <gmd:MD_CellGeometryCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_CellGeometryCode"  codeListValue="{{$view['MD_CellGeometryCode']->identifier}}"  codeSpace="{{$view['MD_CellGeometryCode']->iso}}" >{{$view['MD_CellGeometryCode']->identifier}}</gmd:MD_CellGeometryCode>
            </gmd:cellGeometry>
            <gmd:transformationParameterAvailability>
                <gco:Boolean>{{$view['transformationParameterAvailability']}}</gco:Boolean>
            </gmd:transformationParameterAvailability>
        </gmd:MD_GridSpatialRepresentation>
    </gmd:spatialRepresentationInfo>
    <gmd:referenceSystemInfo>
        <gmd:MD_ReferenceSystem>
            <gmd:referenceSystemIdentifier>
                <gmd:RS_Identifier>
                    <gmd:authority>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>{{$view['referenceSystemInfoTitle']}}</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>{{$view['referenceSystemInfoalternateTitle']}}</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>{{$view['referenceSystemInfodate']}}</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                            <gmd:edition>
                                <gco:CharacterString>{{$view['cdireferenceSystemInfoEdition']}}</gco:CharacterString>
                            </gmd:edition>
                            <gmd:identifier>
                                <gmd:MD_Identifier>
                                    <gmd:code>
                                        <gco:CharacterString>https://www.seadatanet.org/urnurl/SDN:{{$view['referenceSystemInfoalternateTitle']}}</gco:CharacterString>
                                    </gmd:code>
                                </gmd:MD_Identifier>
                            </gmd:identifier>
                        </gmd:CI_Citation>
                    </gmd:authority>
                    <gmd:code>
                        <sdn:SDN_CRSCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_CRSCode"  codeListValue="4326"  codeSpace="SeaDataNet" >{{$view['SDN_CRSCode']}}</sdn:SDN_CRSCode>
                    </gmd:code>
                </gmd:RS_Identifier>
            </gmd:referenceSystemIdentifier>
        </gmd:MD_ReferenceSystem>
    </gmd:referenceSystemInfo>
    <gmd:metadataExtensionInfo xlink:href="https://schemas.seadatanet.org/Standards-Software/Metadata-formats/cdiExtensionInformation.xml"  />
    <gmd:identificationInfo>
        <sdn:SDN_DataIdentification gco:isoType="MD_DataIdentification_Type" >
            <gmd:citation>
                <gmd:CI_Citation>
                    <gmd:title>
                        <gco:CharacterString>{{$view['title']}}</gco:CharacterString>
                    </gmd:title>
                    <gmd:alternateTitle>
                        <gco:CharacterString>{{$view['alternateTitle']}}</gco:CharacterString>
                    </gmd:alternateTitle>
                    <gmd:date>
                        <gmd:CI_Date>
                            <gmd:date>
                                <gco:Date>{{$view['ci_date']}}</gco:Date>
                            </gmd:date>
                            <gmd:dateType>
                                <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                            </gmd:dateType>
                        </gmd:CI_Date>
                    </gmd:date>
                    <gmd:identifier>
                        <gmd:MD_Identifier>
                            <gmd:code>
                                <gco:CharacterString>urn:SDN:CDI:LOCAL:{{$view['alternateTitle']}}</gco:CharacterString>
                            </gmd:code>
                        </gmd:MD_Identifier>
                    </gmd:identifier>
                    <gmd:citedResponsibleParty>
                        <gmd:CI_ResponsibleParty>
                            <gmd:organisationName>
                                <sdn:SDN_EDMOCode codeList="https://edmo.seadatanet.org/isocodelists/edmo-edmerp-codelists.xml#SDN_EDMOCode"  codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->code}}" >{{$view['organization']->organisationname}}</sdn:SDN_EDMOCode>
                            </gmd:organisationName>
                            <gmd:contactInfo>
                                <gmd:CI_Contact>
                                    <gmd:phone>
                                        <gmd:CI_Telephone>
                                            <gmd:voice>
                                                <gco:CharacterString>{{$view['organization']->voice}}</gco:CharacterString>
                                            </gmd:voice>
                                            <gmd:facsimile>
                                                <gco:CharacterString>{{$view['organization']->facsimile}}</gco:CharacterString>
                                            </gmd:facsimile>
                                        </gmd:CI_Telephone>
                                    </gmd:phone>
                                    <gmd:address>
                                        <gmd:CI_Address>
                                            <gmd:deliveryPoint>
                                                <gco:CharacterString>{{$view['organization']->deliveryPoint}} </gco:CharacterString>
                                            </gmd:deliveryPoint>
                                            <gmd:city>
                                                <gco:CharacterString>{{$view['organization']->city}}</gco:CharacterString>
                                            </gmd:city>
                                            <gmd:postalCode>
                                                <gco:CharacterString>{{$view['organization']->postalCode}}X</gco:CharacterString>
                                            </gmd:postalCode>
                                            <gmd:country>
                                                <sdn:SDN_CountryCode codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->countrycode}}"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_CountryCode" >{{$view['organization']->countrycode}}</sdn:SDN_CountryCode>
                                            </gmd:country>
                                            <gmd:electronicMailAddress>
                                                <gco:CharacterString>{{$view['organization']->email}}</gco:CharacterString>
                                            </gmd:electronicMailAddress>
                                        </gmd:CI_Address>
                                    </gmd:address>
                                    <gmd:onlineResource>
                                        <gmd:CI_OnlineResource>
                                            <gmd:linkage>
                                                <gmd:URL>{{$view['organization']->url}}</gmd:URL>
                                            </gmd:linkage>
                                        </gmd:CI_OnlineResource>
                                    </gmd:onlineResource>
                                </gmd:CI_Contact>
                            </gmd:contactInfo>
                            <gmd:role>
                                <gmd:CI_RoleCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_RoleCode"  codeListValue="{{$view['citation_role']}}"  codeSpace="ISOTC211/19115" >{{$view['citation_role']}}</gmd:CI_RoleCode>
                            </gmd:role>
                        </gmd:CI_ResponsibleParty>
                    </gmd:citedResponsibleParty>
                </gmd:CI_Citation>
            </gmd:citation>
            <gmd:abstract>
                <gco:CharacterString>Pictures of the well head.</gco:CharacterString>
            </gmd:abstract>
            <gmd:pointOfContact>
                <gmd:CI_ResponsibleParty>
                    <gmd:organisationName>
                        <sdn:SDN_EDMOCode codeList="https://edmo.seadatanet.org/isocodelists/edmo-edmerp-codelists.xml#SDN_EDMOCode"  codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->code}}" >{{$view['organization']->organisationname}}</sdn:SDN_EDMOCode>
                    </gmd:organisationName>
                    <gmd:contactInfo>
                        <gmd:CI_Contact>
                            <gmd:phone>
                                <gmd:CI_Telephone>
                                    <gmd:voice>
                                        <gco:CharacterString>{{$view['organization']->voice}}</gco:CharacterString>
                                    </gmd:voice>
                                    <gmd:facsimile>
                                        <gco:CharacterString>{{$view['organization']->facsimile}}</gco:CharacterString>
                                    </gmd:facsimile>
                                </gmd:CI_Telephone>
                            </gmd:phone>
                            <gmd:address>
                                <gmd:CI_Address>
                                    <gmd:deliveryPoint>
                                        <gco:CharacterString>{{$view['organization']->address}} </gco:CharacterString>
                                    </gmd:deliveryPoint>
                                    <gmd:city>
                                        <gco:CharacterString>{{$view['organization']->city}}</gco:CharacterString>
                                    </gmd:city>
                                    <gmd:postalCode>
                                        <gco:CharacterString>{{$view['organization']->postalCode}}</gco:CharacterString>
                                    </gmd:postalCode>
                                    <gmd:country>
                                        <sdn:SDN_CountryCode codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->countrycode}}"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_CountryCode" >{{$view['organization']->countrycode}}</sdn:SDN_CountryCode>
                                    </gmd:country>
                                    <gmd:electronicMailAddress>
                                        <gco:CharacterString>{{$view['organization']->email}}e</gco:CharacterString>
                                    </gmd:electronicMailAddress>
                                </gmd:CI_Address>
                            </gmd:address>
                            <gmd:onlineResource>
                                <gmd:CI_OnlineResource>
                                    <gmd:linkage>
                                        <gmd:URL>{{$view['organization']->url}}</gmd:URL>
                                    </gmd:linkage>
                                </gmd:CI_OnlineResource>
                            </gmd:onlineResource>
                        </gmd:CI_Contact>
                    </gmd:contactInfo>
                    <gmd:role>
                        <gmd:CI_RoleCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_RoleCode"  codeListValue="custodian"  codeSpace="ISOTC211/19115" >custodian</gmd:CI_RoleCode>
                    </gmd:role>
                </gmd:CI_ResponsibleParty>
            </gmd:pointOfContact>
            <gmd:resourceMaintenance>
                <gmd:MD_MaintenanceInformation>
                    <gmd:maintenanceAndUpdateFrequency>
                        <gmd:MD_MaintenanceFrequencyCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_MaintenanceFrequencyCode"  codeListValue="continual" >continual</gmd:MD_MaintenanceFrequencyCode>
                    </gmd:maintenanceAndUpdateFrequency>
                    <gmd:userDefinedMaintenanceFrequency>
                        <gts:TM_PeriodDuration>{{$view['resourceMaintenanceFrequency']->identifier}}</gts:TM_PeriodDuration>
                    </gmd:userDefinedMaintenanceFrequency>
                </gmd:MD_MaintenanceInformation>
            </gmd:resourceMaintenance>
            <gmd:descriptiveKeywords>
                <gmd:MD_Keywords>
                    <gmd:keyword>
                        <gco:CharacterString>Oceanographic geographical features</gco:CharacterString>
                    </gmd:keyword>
                    <gmd:type>
                        <gmd:MD_KeywordTypeCode codeSpace="SeaDataNet"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_KeywordTypeCode"  codeListValue="theme" >theme</gmd:MD_KeywordTypeCode>
                    </gmd:type>
                    <gmd:thesaurusName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>GEMET - INSPIRE themes, version 1.0</gco:CharacterString>
                            </gmd:title>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2008-06-01</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="publication"  codeSpace="ISOTC211/19115" >publication</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                        </gmd:CI_Citation>
                    </gmd:thesaurusName>
                </gmd:MD_Keywords>
            </gmd:descriptiveKeywords>
            <gmd:descriptiveKeywords>
                <gmd:MD_Keywords>
                    <gmd:keyword>
                        <sdn:SDN_ParameterDiscoveryCode codeSpace="SeaDataNet"  codeListValue="MBAN"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_ParameterDiscoveryCode" >Bathymetry and Elevation</sdn:SDN_ParameterDiscoveryCode>
                    </gmd:keyword>
                    <gmd:type>
                        <gmd:MD_KeywordTypeCode codeSpace="SeaDataNet"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_KeywordTypeCode"  codeListValue="parameter" >parameter</gmd:MD_KeywordTypeCode>
                    </gmd:type>
                    <gmd:thesaurusName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>SeaDataNet Parameter Discovery Vocabulary</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>P02</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2019-07-26</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                            <gmd:edition>
                                <gco:CharacterString>113</gco:CharacterString>
                            </gmd:edition>
                            <gmd:identifier>
                                <gmd:MD_Identifier>
                                    <gmd:code>
                                        <gco:CharacterString>https://www.seadatanet.org/urnurl/SDN:P02</gco:CharacterString>
                                    </gmd:code>
                                </gmd:MD_Identifier>
                            </gmd:identifier>
                        </gmd:CI_Citation>
                    </gmd:thesaurusName>
                </gmd:MD_Keywords>
            </gmd:descriptiveKeywords>
            <gmd:descriptiveKeywords>
                <gmd:MD_Keywords>
                    <gmd:keyword>
                        <sdn:SDN_DeviceCategoryCode codeSpace="SeaDataNet"  codeListValue="311"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_DeviceCategoryCode" >cameras</sdn:SDN_DeviceCategoryCode>
                    </gmd:keyword>
                    <gmd:type>
                        <gmd:MD_KeywordTypeCode codeSpace="SeaDataNet"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_KeywordTypeCode"  codeListValue="instrument" >instrument</gmd:MD_KeywordTypeCode>
                    </gmd:type>
                    <gmd:thesaurusName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>SeaDataNet device categories</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>L05</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2020-09-18</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                            <gmd:edition>
                                <gco:CharacterString>78</gco:CharacterString>
                            </gmd:edition>
                            <gmd:identifier>
                                <gmd:MD_Identifier>
                                    <gmd:code>
                                        <gco:CharacterString>https://www.seadatanet.org/urnurl/SDN:L05</gco:CharacterString>
                                    </gmd:code>
                                </gmd:MD_Identifier>
                            </gmd:identifier>
                        </gmd:CI_Citation>
                    </gmd:thesaurusName>
                </gmd:MD_Keywords>
            </gmd:descriptiveKeywords>
            <gmd:descriptiveKeywords>
                <gmd:MD_Keywords>
                    <gmd:keyword>
                        <sdn:SDN_PlatformCategoryCode codeSpace="SeaDataNet"  codeListValue="22"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_PlatformCategoryCode" >propelled unmanned submersible</sdn:SDN_PlatformCategoryCode>
                    </gmd:keyword>
                    <gmd:type>
                        <gmd:MD_KeywordTypeCode codeSpace="SeaDataNet"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_KeywordTypeCode"  codeListValue="platform_class" >platform_class</gmd:MD_KeywordTypeCode>
                    </gmd:type>
                    <gmd:thesaurusName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>SeaVoX Platform Categories</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>L06</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2020-09-11</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                            <gmd:edition>
                                <gco:CharacterString>16</gco:CharacterString>
                            </gmd:edition>
                            <gmd:identifier>
                                <gmd:MD_Identifier>
                                    <gmd:code>
                                        <gco:CharacterString>https://www.seadatanet.org/urnurl/SDN:L06</gco:CharacterString>
                                    </gmd:code>
                                </gmd:MD_Identifier>
                            </gmd:identifier>
                        </gmd:CI_Citation>
                    </gmd:thesaurusName>
                </gmd:MD_Keywords>
            </gmd:descriptiveKeywords>
            <gmd:resourceConstraints>
                <gmd:MD_Constraints>
                    <gmd:useLimitation>
                        <gco:CharacterString>Not applicable</gco:CharacterString>
                    </gmd:useLimitation>
                </gmd:MD_Constraints>
            </gmd:resourceConstraints>
            <gmd:resourceConstraints>
                <gmd:MD_LegalConstraints>
                    <gmd:accessConstraints>
                        <gmd:MD_RestrictionCode codeListValue="otherRestrictions"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_RestrictionCode" >otherRestrictions</gmd:MD_RestrictionCode>
                    </gmd:accessConstraints>
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="https://www.seadatanet.org/urnurl/SDN:L08::LS" >SeaDataNet licence</gmx:Anchor>
                    </gmd:otherConstraints>
                </gmd:MD_LegalConstraints>
            </gmd:resourceConstraints>
            <gmd:aggregationInfo>
                <gmd:MD_AggregateInformation>
                    <gmd:aggregateDataSetName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>{{$view['organization']->address}}</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>CE19001</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2019-01-10</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                        </gmd:CI_Citation>
                    </gmd:aggregateDataSetName>
                    <gmd:associationType>
                        <gmd:DS_AssociationTypeCode codeListValue="largerWorkCitation"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#DS_AssociationTypeCode"  codeSpace="ISOTC211/19115" >largerWorkCitation</gmd:DS_AssociationTypeCode>
                    </gmd:associationType>
                    <gmd:initiativeType>
                        <gmd:DS_InitiativeTypeCode codeListValue="campaign"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#DS_InitiativeTypeCode"  codeSpace="ISOTC211/19115" >campaign</gmd:DS_InitiativeTypeCode>
                    </gmd:initiativeType>
                </gmd:MD_AggregateInformation>
            </gmd:aggregationInfo>
            <gmd:aggregationInfo>
                <gmd:MD_AggregateInformation>
                    <gmd:aggregateDataSetName>
                        <gmd:CI_Citation>
                            <gmd:title>
                                <gco:CharacterString>Kinsale</gco:CharacterString>
                            </gmd:title>
                            <gmd:alternateTitle>
                                <gco:CharacterString>WellHead</gco:CharacterString>
                            </gmd:alternateTitle>
                            <gmd:date>
                                <gmd:CI_Date>
                                    <gmd:date>
                                        <gco:Date>2019-01-10</gco:Date>
                                    </gmd:date>
                                    <gmd:dateType>
                                        <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="revision"  codeSpace="ISOTC211/19115" >revision</gmd:CI_DateTypeCode>
                                    </gmd:dateType>
                                </gmd:CI_Date>
                            </gmd:date>
                        </gmd:CI_Citation>
                    </gmd:aggregateDataSetName>
                    <gmd:associationType>
                        <gmd:DS_AssociationTypeCode codeListValue="source"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#DS_AssociationTypeCode"  codeSpace="ISOTC211/19115" >source</gmd:DS_AssociationTypeCode>
                    </gmd:associationType>
                    <gmd:initiativeType>
                        <gmd:DS_InitiativeTypeCode codeListValue="operation"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#DS_InitiativeTypeCode"  codeSpace="ISOTC211/19115" >operation</gmd:DS_InitiativeTypeCode>
                    </gmd:initiativeType>
                </gmd:MD_AggregateInformation>
            </gmd:aggregationInfo>
            <gmd:spatialRepresentationType>
                <gmd:MD_SpatialRepresentationTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_SpatialRepresentationTypeCode"  codeListValue="{{$view['spatialRepresentationType']->identifier}}" >{{$view['spatialRepresentationType']->identifier}}</gmd:MD_SpatialRepresentationTypeCode>
            </gmd:spatialRepresentationType>
            <gmd:language>
                <gmd:LanguageCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#LanguageCode"  codeListValue="eng"  codeSpace="ISOTC211/19115" >English</gmd:LanguageCode>
            </gmd:language>
            <gmd:characterSet>
                <gmd:MD_CharacterSetCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_CharacterSetCode"  codeListValue="utf8"  codeSpace="ISOTC211/19115" >utf8</gmd:MD_CharacterSetCode>
            </gmd:characterSet>
            <gmd:topicCategory>
                <gmd:MD_TopicCategoryCode>{{$view['MD_TopicCategoryCode']}}</gmd:MD_TopicCategoryCode>
            </gmd:topicCategory>
            <gmd:extent>
                <gmd:EX_Extent>
                    <gmd:geographicElement>
                        <gmd:EX_GeographicBoundingBox>
                            <gmd:westBoundLongitude>
                                <gco:Decimal>{{$view['westBoundLongitude']}}</gco:Decimal>
                            </gmd:westBoundLongitude>
                            <gmd:eastBoundLongitude>
                                <gco:Decimal>{{$view['eastBoundLongitude']}}</gco:Decimal>
                            </gmd:eastBoundLongitude>
                            <gmd:southBoundLatitude>
                                <gco:Decimal>{{$view['southBoundLatitude']}}</gco:Decimal>
                            </gmd:southBoundLatitude>
                            <gmd:northBoundLatitude>
                                <gco:Decimal>{{$view['northBoundLatitude']}}</gco:Decimal>
                            </gmd:northBoundLatitude>
                        </gmd:EX_GeographicBoundingBox>
                    </gmd:geographicElement>
                    <gmd:temporalElement>
                        <gmd:EX_TemporalExtent>
                            <gmd:extent>
                                <gml:TimePeriod gml:id="mik3.6.2" >
                                    <gml:beginPosition>{{$view['endPosition']}}</gml:beginPosition>
                                    <gml:endPosition>{{$view['endPosition']}}</gml:endPosition>
                                </gml:TimePeriod>
                            </gmd:extent>
                        </gmd:EX_TemporalExtent>
                    </gmd:temporalElement>
                </gmd:EX_Extent>
            </gmd:extent>
        </sdn:SDN_DataIdentification>
    </gmd:identificationInfo>
    <gmd:distributionInfo>
        <gmd:MD_Distribution>
            <gmd:distributionFormat>
                <gmd:MD_Format>
                    <gmd:name>
                        <sdn:SDN_FormatNameCode codeSpace="SeaDataNet"  codeListValue="PNG"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_FormatNameCode" >Portable Network Graphics</sdn:SDN_FormatNameCode>
                    </gmd:name>
                    <gmd:version>
                        <gco:CharacterString>none</gco:CharacterString>
                    </gmd:version>
                </gmd:MD_Format>
            </gmd:distributionFormat>
            <gmd:distributor>
                <gmd:MD_Distributor>
                    <gmd:distributorContact>
                        <gmd:CI_ResponsibleParty>
                            <gmd:organisationName>
                                <sdn:SDN_EDMOCode codeList="https://edmo.seadatanet.org/isocodelists/edmo-edmerp-codelists.xml#SDN_EDMOCode"  codeSpace="SeaDataNet"  codeListValue="{{$view['organization']->code}}" >{{$view['organization']->organizationname}}</sdn:SDN_EDMOCode>
                            </gmd:organisationName>
                            <gmd:contactInfo>
                                <gmd:CI_Contact>
                                    <gmd:phone>
                                        <gmd:CI_Telephone>
                                            <gmd:voice>
                                                <gco:CharacterString>{{$view['organization']->voice}}</gco:CharacterString>
                                            </gmd:voice>
                                            <gmd:facsimile>
                                                <gco:CharacterString>{{$view['organization']->facsimile}}</gco:CharacterString>
                                            </gmd:facsimile>
                                        </gmd:CI_Telephone>
                                    </gmd:phone>
                                    <gmd:address>
                                        <gmd:CI_Address>
                                            <gmd:deliveryPoint>
                                                <gco:CharacterString>{{$view['organization']->address}} </gco:CharacterString>
                                            </gmd:deliveryPoint>
                                            <gmd:city>
                                                <gco:CharacterString>{{$view['organization']->city}}</gco:CharacterString>
                                            </gmd:city>
                                            <gmd:postalCode>
                                                <gco:CharacterString>{{$view['organization']->postalCode}}</gco:CharacterString>
                                            </gmd:postalCode>
                                            <gmd:country>
                                                <sdn:SDN_CountryCode codeSpace="SeaDataNet"  codeListValue="IE"  codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/cdicsrCodeList.xml#SDN_CountryCode" >{{$view['organization']->countrycode}}</sdn:SDN_CountryCode>
                                            </gmd:country>
                                            <gmd:electronicMailAddress>
                                                <gco:CharacterString>{{$view['organization']->email}}</gco:CharacterString>
                                            </gmd:electronicMailAddress>
                                        </gmd:CI_Address>
                                    </gmd:address>
                                    <gmd:onlineResource>
                                        <gmd:CI_OnlineResource>
                                            <gmd:linkage>
                                                <gmd:URL>{{$view['organization']->url}}</gmd:URL>
                                            </gmd:linkage>
                                        </gmd:CI_OnlineResource>
                                    </gmd:onlineResource>
                                </gmd:CI_Contact>
                            </gmd:contactInfo>
                            <gmd:role>
                                <gmd:CI_RoleCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_RoleCode"  codeListValue="distributor"  codeSpace="ISOTC211/19115" >distributor</gmd:CI_RoleCode>
                            </gmd:role>
                        </gmd:CI_ResponsibleParty>
                    </gmd:distributorContact>
                </gmd:MD_Distributor>
            </gmd:distributor>
            <gmd:transferOptions>
                <gmd:MD_DigitalTransferOptions>
                    <gmd:transferSize>
                        <gco:Real>6.62</gco:Real>
                    </gmd:transferSize>
                    <gmd:onLine>
                        <gmd:CI_OnlineResource>
                            <gmd:linkage>
                                <gmd:URL>http://eumarines.ul.ie/</gmd:URL>
                            </gmd:linkage>
                            <gmd:protocol>
                                <gco:CharacterString>HTTP-DOWNLOAD</gco:CharacterString>
                            </gmd:protocol>
                            <gmd:function>
                                <gmd:CI_OnLineFunctionCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_OnLineFunctionCode"  codeListValue="downloadRegistration"  codeSpace="SeaDataNet" >web data access with registration</gmd:CI_OnLineFunctionCode>
                            </gmd:function>
                        </gmd:CI_OnlineResource>
                    </gmd:onLine>
                </gmd:MD_DigitalTransferOptions>
            </gmd:transferOptions>
        </gmd:MD_Distribution>
    </gmd:distributionInfo>
    <gmd:dataQualityInfo>
        <gmd:DQ_DataQuality>
            <gmd:scope>
                <gmd:DQ_Scope>
                    <gmd:level>
                        <gmd:MD_ScopeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#MD_ScopeCode"  codeListValue="dataset"  codeSpace="ISOTC211/19115" >dataset</gmd:MD_ScopeCode>
                    </gmd:level>
                </gmd:DQ_Scope>
            </gmd:scope>
            <gmd:report>
                <gmd:DQ_DomainConsistency>
                    <gmd:result>
                        <gmd:DQ_ConformanceResult>
                            <gmd:specification>
                                <gmd:CI_Citation>
                                    <gmd:title>
                                        <gco:CharacterString>COMMISSION REGULATION (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services</gco:CharacterString>
                                    </gmd:title>
                                    <gmd:date>
                                        <gmd:CI_Date>
                                            <gmd:date>
                                                <gco:Date>2010-12-08</gco:Date>
                                            </gmd:date>
                                            <gmd:dateType>
                                                <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="publication"  codeSpace="ISOTC211/19115" >publication</gmd:CI_DateTypeCode>
                                            </gmd:dateType>
                                        </gmd:CI_Date>
                                    </gmd:date>
                                </gmd:CI_Citation>
                            </gmd:specification>
                            <gmd:explanation>
                                <gco:CharacterString>See the referenced specification</gco:CharacterString>
                            </gmd:explanation>
                            <gmd:pass>
                                <gco:Boolean>true</gco:Boolean>
                            </gmd:pass>
                        </gmd:DQ_ConformanceResult>
                    </gmd:result>
                </gmd:DQ_DomainConsistency>
            </gmd:report>
            <gmd:report>
                <gmd:DQ_DomainConsistency>
                    <gmd:result>
                        <gmd:DQ_ConformanceResult>
                            <gmd:specification>
                                <gmd:CI_Citation>
                                    <gmd:title>
                                        <gco:CharacterString>COMMISSION REGULATION (EC) No 1205/2008 of 3 December 2008 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards metadata</gco:CharacterString>
                                    </gmd:title>
                                    <gmd:date>
                                        <gmd:CI_Date>
                                            <gmd:date>
                                                <gco:Date>2008-12-04</gco:Date>
                                            </gmd:date>
                                            <gmd:dateType>
                                                <gmd:CI_DateTypeCode codeList="https://vocab.nerc.ac.uk/isoCodelists/sdnCodelists/gmxCodeLists.xml#CI_DateTypeCode"  codeListValue="publication"  codeSpace="ISOTC211/19115" >publication</gmd:CI_DateTypeCode>
                                            </gmd:dateType>
                                        </gmd:CI_Date>
                                    </gmd:date>
                                </gmd:CI_Citation>
                            </gmd:specification>
                            <gmd:explanation>
                                <gco:CharacterString>See the referenced specification</gco:CharacterString>
                            </gmd:explanation>
                            <gmd:pass>
                                <gco:Boolean>true</gco:Boolean>
                            </gmd:pass>
                        </gmd:DQ_ConformanceResult>
                    </gmd:result>
                </gmd:DQ_DomainConsistency>
            </gmd:report>
            <gmd:lineage>
                <gmd:LI_Lineage>
                    <gmd:statement>
                        <gco:CharacterString>The data centres apply standard data quality control procedures on all data that the centres manage. Ask the data centre for details.</gco:CharacterString>
                    </gmd:statement>
                </gmd:LI_Lineage>
            </gmd:lineage>
        </gmd:DQ_DataQuality>
    </gmd:dataQualityInfo>
{!! '</'.'gmd:MD_Metadata>' !!}
