<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="" media="all" />
</head>
<body>

<div style="width:670px; padding:0 0 0 20px; margin:50px auto 12px auto" id="email_header">
				<span style="color:#000; font-family:trebuchet ms; letter-spacing:1px;">
					<center>Reset</center>
                            </span>
</div>


<div style="width:670px; padding:0 20px 20px 20px; background:#fff; margin:0 auto; border:2px #f17c37 solid;
				moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; color:#454545;line-height:1.5em; "
     id="email_content">

    <h1 style="padding:5px 0 0 0; font-family:georgia;font-weight:500;font-size:24px;color:#000;border-bottom:1px solid #f17c37">
        Reset Password
    </h1>


    <p>
        Hello <strong>{{ $user->first_name }} {{ $user->last_name }}</strong>,
        You have requested a password reset.

        <br>
        Please click on this link or copy it in the browser in order to create a new password:
        <a href="https://eumarines.ul.ie/auth/reset/{{$user->token}}">https:\\eumarines.ul.ie\auth\reset\{{$user->token}}</a>

    </p>

    <br>
    <p style="">
        Thank you,<br>
    </p>




    <div style="text-align:center; border-top:1px solid #f17c37;padding:5px 0 0 0;" id="email_footer">
        <small style="font-size:11px; color:#999; line-height:14px;">
            <table>
                <tr>
                    <td>
                    </td>
                </tr>

                <tr>

                </tr>
                <tr>
                    <td>
                        <p align=center style='text-align:center'>
                            This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed.
                            If you have received this email in error please notify the system manager.
                            This message is confidential. It may also be privileged or otherwise protected by work product immunity or other legal rules.
                            If you have received it by mistake, please let us know by e-mail reply and delete it from your system; you may not copy this message or disclose its contents to anyone.
                            Please send us by fax any message containing deadlines as incoming e-mails are not screened for response deadlines.
                            The integrity and security of this message cannot be guaranteed on the Internet.      </p>
                    </td>
                </tr>
            </table>

        </small>
    </div>

</div>
</body>
</html>
