<?php


use App\Modules\Accountancy\Models\Expense;
use App\Modules\Cdis\Models\GmxCode;
use App\Modules\Files\Models\File;
use App\Modules\Files\Models\FileType;

use App\Modules\Files\Models\UserFile;
use App\Modules\Groups\Models\Group;
use App\Modules\Groups\Models\GroupRights;
use App\Modules\Groups\Models\UserGroup;
use App\Modules\Internationalization\Models\Country;
use App\Modules\Navigation\Models\Navigation;
use App\Modules\Internationalization\Models\City;
use App\Modules\Nomenclatoare\Models\ExpensesType;
use App\Modules\Organizations\Models\Organization;
use App\Modules\Users\Models\Right;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserDetail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->seedCountries();
        $this->seedCities();
        $this->seedUsers();
        $this->seedUserDetail();
        $this->seedRights();
        $this->seedGroupRights();
        $this->seedUserGroups();
        $this->seedNav();
        $this->seedFiles();
        $this->seedOrganizations();
        $this->seedNomenExpenseType();

    }

    // Users Countries
    private function seedCountries()
    {
        $countries = [
            array(1, 'Afghanistan', 'AF', '+93', 'Afghan afghani', '؋', 'AFN'),
            array(2, 'Aland Islands', 'AX', '+358', '', '', ''),
            array(3, 'Albania', 'AL', '+355', 'Albanian lek', 'L', 'ALL'),
            array(4, 'Algeria', 'DZ', '+213', 'Algerian dinar', 'د.ج', 'DZD'),
            array(5, 'AmericanSamoa', 'AS', '+1684', '', '', ''),
            array(6, 'Andorra', 'AD', '+376', 'Euro', '€', 'EUR'),
            array(7, 'Angola', 'AO', '+244', 'Angolan kwanza', 'Kz', 'AOA'),
            array(8, 'Anguilla', 'AI', '+1264', 'East Caribbean dolla', '$', 'XCD'),
            array(9, 'Antarctica', 'AQ', '+672', '', '', ''),
            array(10, 'Antigua and Barbuda', 'AG', '+1268', 'East Caribbean dolla', '$', 'XCD'),
            array(11, 'Argentina', 'AR', '+54', 'Argentine peso', '$', 'ARS'),
            array(12, 'Armenia', 'AM', '+374', 'Armenian dram', '', 'AMD'),
            array(13, 'Aruba', 'AW', '+297', 'Aruban florin', 'ƒ', 'AWG'),
            array(14, 'Australia', 'AU', '+61', 'Australian dollar', '$', 'AUD'),
            array(15, 'Austria', 'AT', '+43', 'Euro', '€', 'EUR'),
            array(16, 'Azerbaijan', 'AZ', '+994', 'Azerbaijani manat', '', 'AZN'),
            array(17, 'Bahamas', 'BS', '+1242', '', '', ''),
            array(18, 'Bahrain', 'BH', '+973', 'Bahraini dinar', '.د.ب', 'BHD'),
            array(19, 'Bangladesh', 'BD', '+880', 'Bangladeshi taka', '৳', 'BDT'),
            array(20, 'Barbados', 'BB', '+1246', 'Barbadian dollar', '$', 'BBD'),
            array(21, 'Belarus', 'BY', '+375', 'Belarusian ruble', 'Br', 'BYR'),
            array(22, 'Belgium', 'BE', '+32', 'Euro', '€', 'EUR'),
            array(23, 'Belize', 'BZ', '+501', 'Belize dollar', '$', 'BZD'),
            array(24, 'Benin', 'BJ', '+229', 'West African CFA fra', 'Fr', 'XOF'),
            array(25, 'Bermuda', 'BM', '+1441', 'Bermudian dollar', '$', 'BMD'),
            array(26, 'Bhutan', 'BT', '+975', 'Bhutanese ngultrum', 'Nu.', 'BTN'),
            array(27, 'Bolivia, Plurination', 'BO', '+591', '', '', ''),
            array(28, 'Bosnia and Herzegovi', 'BA', '+387', '', '', ''),
            array(29, 'Botswana', 'BW', '+267', 'Botswana pula', 'P', 'BWP'),
            array(30, 'Brazil', 'BR', '+55', 'Brazilian real', 'R$', 'BRL'),
            array(31, 'British Indian Ocean', 'IO', '+246', '', '', ''),
            array(32, 'Brunei Darussalam', 'BN', '+673', '', '', ''),
            array(33, 'Bulgaria', 'BG', '+359', 'Bulgarian lev', 'лв', 'BGN'),
            array(34, 'Burkina Faso', 'BF', '+226', 'West African CFA fra', 'Fr', 'XOF'),
            array(35, 'Burundi', 'BI', '+257', 'Burundian franc', 'Fr', 'BIF'),
            array(36, 'Cambodia', 'KH', '+855', 'Cambodian riel', '៛', 'KHR'),
            array(37, 'Cameroon', 'CM', '+237', 'Central African CFA ', 'Fr', 'XAF'),
            array(38, 'Canada', 'CA', '+1', 'Canadian dollar', '$', 'CAD'),
            array(39, 'Cape Verde', 'CV', '+238', 'Cape Verdean escudo', 'Esc or $', 'CVE'),
            array(40, 'Cayman Islands', 'KY', '+ 345', 'Cayman Islands dolla', '$', 'KYD'),
            array(41, 'Central African Repu', 'CF', '+236', '', '', ''),
            array(42, 'Chad', 'TD', '+235', 'Central African CFA ', 'Fr', 'XAF'),
            array(43, 'Chile', 'CL', '+56', 'Chilean peso', '$', 'CLP'),
            array(44, 'China', 'CN', '+86', 'Chinese yuan', '¥ or 元', 'CNY'),
            array(45, 'Christmas Island', 'CX', '+61', '', '', ''),
            array(46, 'Cocos array(Keeling) Isla', 'CC', '+61', '', '', ''),
            array(47, 'Colombia', 'CO', '+57', 'Colombian peso', '$', 'COP'),
            array(48, 'Comoros', 'KM', '+269', 'Comorian franc', 'Fr', 'KMF'),
            array(49, 'Congo', 'CG', '+242', '', '', ''),
            array(50, 'Congo, The Democrati', 'CD', '+243', '', '', ''),
            array(51, 'Cook Islands', 'CK', '+682', 'New Zealand dollar', '$', 'NZD'),
            array(52, 'Costa Rica', 'CR', '+506', 'Costa Rican colón', '₡', 'CRC'),
            array(53, 'Cote d\'Ivoire', 'CI', '+225', 'West African CFA fra', 'Fr', 'XOF'),
            array(54, 'Croatia', 'HR', '+385', 'Croatian kuna', 'kn', 'HRK'),
            array(55, 'Cuba', 'CU', '+53', 'Cuban convertible pe', '$', 'CUC'),
            array(56, 'Cyprus', 'CY', '+357', 'Euro', '€', 'EUR'),
            array(57, 'Czech Republic', 'CZ', '+420', 'Czech koruna', 'Kč', 'CZK'),
            array(58, 'Denmark', 'DK', '+45', 'Danish krone', 'kr', 'DKK'),
            array(59, 'Djibouti', 'DJ', '+253', 'Djiboutian franc', 'Fr', 'DJF'),
            array(60, 'Dominica', 'DM', '+1767', 'East Caribbean dolla', '$', 'XCD'),
            array(61, 'Dominican Republic', 'DO', '+1849', 'Dominican peso', '$', 'DOP'),
            array(62, 'Ecuador', 'EC', '+593', 'United States dollar', '$', 'USD'),
            array(63, 'Egypt', 'EG', '+20', 'Egyptian pound', '£ or ج.م', 'EGP'),
            array(64, 'El Salvador', 'SV', '+503', 'United States dollar', '$', 'USD'),
            array(65, 'Equatorial Guinea', 'GQ', '+240', 'Central African CFA ', 'Fr', 'XAF'),
            array(66, 'Eritrea', 'ER', '+291', 'Eritrean nakfa', 'Nfk', 'ERN'),
            array(67, 'Estonia', 'EE', '+372', 'Euro', '€', 'EUR'),
            array(68, 'Ethiopia', 'ET', '+251', 'Ethiopian birr', 'Br', 'ETB'),
            array(69, 'Falkland Islands array(Ma', 'FK', '+500', '', '', ''),
            array(70, 'Faroe Islands', 'FO', '+298', 'Danish krone', 'kr', 'DKK'),
            array(71, 'Fiji', 'FJ', '+679', 'Fijian dollar', '$', 'FJD'),
            array(72, 'Finland', 'FI', '+358', 'Euro', '€', 'EUR'),
            array(73, 'France', 'FR', '+33', 'Euro', '€', 'EUR'),
            array(74, 'French Guiana', 'GF', '+594', '', '', ''),
            array(75, 'French Polynesia', 'PF', '+689', 'CFP franc', 'Fr', 'XPF'),
            array(76, 'Gabon', 'GA', '+241', 'Central African CFA ', 'Fr', 'XAF'),
            array(77, 'Gambia', 'GM', '+220', '', '', ''),
            array(78, 'Georgia', 'GE', '+995', 'Georgian lari', 'ლ', 'GEL'),
            array(79, 'Germany', 'DE', '+49', 'Euro', '€', 'EUR'),
            array(80, 'Ghana', 'GH', '+233', 'Ghana cedi', '₵', 'GHS'),
            array(81, 'Gibraltar', 'GI', '+350', 'Gibraltar pound', '£', 'GIP'),
            array(82, 'Greece', 'GR', '+30', 'Euro', '€', 'EUR'),
            array(83, 'Greenland', 'GL', '+299', '', '', ''),
            array(84, 'Grenada', 'GD', '+1473', 'East Caribbean dolla', '$', 'XCD'),
            array(85, 'Guadeloupe', 'GP', '+590', '', '', ''),
            array(86, 'Guam', 'GU', '+1671', '', '', ''),
            array(87, 'Guatemala', 'GT', '+502', 'Guatemalan quetzal', 'Q', 'GTQ'),
            array(88, 'Guernsey', 'GG', '+44', 'British pound', '£', 'GBP'),
            array(89, 'Guinea', 'GN', '+224', 'Guinean franc', 'Fr', 'GNF'),
            array(90, 'Guinea-Bissau', 'GW', '+245', 'West African CFA fra', 'Fr', 'XOF'),
            array(91, 'Guyana', 'GY', '+595', 'Guyanese dollar', '$', 'GYD'),
            array(92, 'Haiti', 'HT', '+509', 'Haitian gourde', 'G', 'HTG'),
            array(93, 'Holy See array(Vatican Ci', 'VA', '+379', '', '', ''),
            array(94, 'Honduras', 'HN', '+504', 'Honduran lempira', 'L', 'HNL'),
            array(95, 'Hong Kong', 'HK', '+852', 'Hong Kong dollar', '$', 'HKD'),
            array(96, 'Hungary', 'HU', '+36', 'Hungarian forint', 'Ft', 'HUF'),
            array(97, 'Iceland', 'IS', '+354', 'Icelandic króna', 'kr', 'ISK'),
            array(98, 'India', 'IN', '+91', 'Indian rupee', '₹', 'INR'),
            array(99, 'Indonesia', 'ID', '+62', 'Indonesian rupiah', 'Rp', 'IDR'),
            array(100, 'Iran, Islamic Republ', 'IR', '+98', '', '', ''),
            array(101, 'Iraq', 'IQ', '+964', 'Iraqi dinar', 'ع.د', 'IQD'),
            array(102, 'Ireland', 'IE', '+353', 'Euro', '€', 'EUR'),
            array(103, 'Isle of Man', 'IM', '+44', 'British pound', '£', 'GBP'),
            array(104, 'Israel', 'IL', '+972', 'Israeli new shekel', '₪', 'ILS'),
            array(105, 'Italy', 'IT', '+39', 'Euro', '€', 'EUR'),
            array(106, 'Jamaica', 'JM', '+1876', 'Jamaican dollar', '$', 'JMD'),
            array(107, 'Japan', 'JP', '+81', 'Japanese yen', '¥', 'JPY'),
            array(108, 'Jersey', 'JE', '+44', 'British pound', '£', 'GBP'),
            array(109, 'Jordan', 'JO', '+962', 'Jordanian dinar', 'د.ا', 'JOD'),
            array(110, 'Kazakhstan', 'KZ', '+7 7', 'Kazakhstani tenge', '', 'KZT'),
            array(111, 'Kenya', 'KE', '+254', 'Kenyan shilling', 'Sh', 'KES'),
            array(112, 'Kiribati', 'KI', '+686', 'Australian dollar', '$', 'AUD'),
            array(113, 'Korea, Democratic Pe', 'KP', '+850', '', '', ''),
            array(114, 'Korea, Republic of S', 'KR', '+82', '', '', ''),
            array(115, 'Kuwait', 'KW', '+965', 'Kuwaiti dinar', 'د.ك', 'KWD'),
            array(116, 'Kyrgyzstan', 'KG', '+996', 'Kyrgyzstani som', 'лв', 'KGS'),
            array(117, 'Laos', 'LA', '+856', 'Lao kip', '₭', 'LAK'),
            array(118, 'Latvia', 'LV', '+371', 'Euro', '€', 'EUR'),
            array(119, 'Lebanon', 'LB', '+961', 'Lebanese pound', 'ل.ل', 'LBP'),
            array(120, 'Lesotho', 'LS', '+266', 'Lesotho loti', 'L', 'LSL'),
            array(121, 'Liberia', 'LR', '+231', 'Liberian dollar', '$', 'LRD'),
            array(122, 'Libyan Arab Jamahiri', 'LY', '+218', '', '', ''),
            array(123, 'Liechtenstein', 'LI', '+423', 'Swiss franc', 'Fr', 'CHF'),
            array(124, 'Lithuania', 'LT', '+370', 'Euro', '€', 'EUR'),
            array(125, 'Luxembourg', 'LU', '+352', 'Euro', '€', 'EUR'),
            array(126, 'Macao', 'MO', '+853', '', '', ''),
            array(127, 'Macedonia', 'MK', '+389', '', '', ''),
            array(128, 'Madagascar', 'MG', '+261', 'Malagasy ariary', 'Ar', 'MGA'),
            array(129, 'Malawi', 'MW', '+265', 'Malawian kwacha', 'MK', 'MWK'),
            array(130, 'Malaysia', 'MY', '+60', 'Malaysian ringgit', 'RM', 'MYR'),
            array(131, 'Maldives', 'MV', '+960', 'Maldivian rufiyaa', '.ރ', 'MVR'),
            array(132, 'Mali', 'ML', '+223', 'West African CFA fra', 'Fr', 'XOF'),
            array(133, 'Malta', 'MT', '+356', 'Euro', '€', 'EUR'),
            array(134, 'Marshall Islands', 'MH', '+692', 'United States dollar', '$', 'USD'),
            array(135, 'Martinique', 'MQ', '+596', '', '', ''),
            array(136, 'Mauritania', 'MR', '+222', 'Mauritanian ouguiya', 'UM', 'MRO'),
            array(137, 'Mauritius', 'MU', '+230', 'Mauritian rupee', '₨', 'MUR'),
            array(138, 'Mayotte', 'YT', '+262', '', '', ''),
            array(139, 'Mexico', 'MX', '+52', 'Mexican peso', '$', 'MXN'),
            array(140, 'Micronesia, Federate', 'FM', '+691', '', '', ''),
            array(141, 'Moldova', 'MD', '+373', 'Moldovan leu', 'L', 'MDL'),
            array(142, 'Monaco', 'MC', '+377', 'Euro', '€', 'EUR'),
            array(143, 'Mongolia', 'MN', '+976', 'Mongolian tögrög', '₮', 'MNT'),
            array(144, 'Montenegro', 'ME', '+382', 'Euro', '€', 'EUR'),
            array(145, 'Montserrat', 'MS', '+1664', 'East Caribbean dolla', '$', 'XCD'),
            array(146, 'Morocco', 'MA', '+212', 'Moroccan dirham', 'د.م.', 'MAD'),
            array(147, 'Mozambique', 'MZ', '+258', 'Mozambican metical', 'MT', 'MZN'),
            array(148, 'Myanmar', 'MM', '+95', 'Burmese kyat', 'Ks', 'MMK'),
            array(149, 'Namibia', 'NA', '+264', 'Namibian dollar', '$', 'NAD'),
            array(150, 'Nauru', 'NR', '+674', 'Australian dollar', '$', 'AUD'),
            array(151, 'Nepal', 'NP', '+977', 'Nepalese rupee', '₨', 'NPR'),
            array(152, 'Netherlands', 'NL', '+31', 'Euro', '€', 'EUR'),
            array(153, 'Netherlands Antilles', 'AN', '+599', '', '', ''),
            array(154, 'New Caledonia', 'NC', '+687', 'CFP franc', 'Fr', 'XPF'),
            array(155, 'New Zealand', 'NZ', '+64', 'New Zealand dollar', '$', 'NZD'),
            array(156, 'Nicaragua', 'NI', '+505', 'Nicaraguan córdoba', 'C$', 'NIO'),
            array(157, 'Niger', 'NE', '+227', 'West African CFA fra', 'Fr', 'XOF'),
            array(158, 'Nigeria', 'NG', '+234', 'Nigerian naira', '₦', 'NGN'),
            array(159, 'Niue', 'NU', '+683', 'New Zealand dollar', '$', 'NZD'),
            array(160, 'Norfolk Island', 'NF', '+672', '', '', ''),
            array(161, 'Northern Mariana Isl', 'MP', '+1670', '', '', ''),
            array(162, 'Norway', 'NO', '+47', 'Norwegian krone', 'kr', 'NOK'),
            array(163, 'Oman', 'OM', '+968', 'Omani rial', 'ر.ع.', 'OMR'),
            array(164, 'Pakistan', 'PK', '+92', 'Pakistani rupee', '₨', 'PKR'),
            array(165, 'Palau', 'PW', '+680', 'Palauan dollar', '$', 'USD'),
            array(166, 'Palestinian Territor', 'PS', '+970', '', '', ''),
            array(167, 'Panama', 'PA', '+507', 'Panamanian balboa', 'B/.', 'PAB'),
            array(168, 'Papua New Guinea', 'PG', '+675', 'Papua New Guinean ki', 'K', 'PGK'),
            array(169, 'Paraguay', 'PY', '+595', 'Paraguayan guaraní', '₲', 'PYG'),
            array(170, 'Peru', 'PE', '+51', 'Peruvian nuevo sol', 'S/.', 'PEN'),
            array(171, 'Philippines', 'PH', '+63', 'Philippine peso', '₱', 'PHP'),
            array(172, 'Pitcairn', 'PN', '+872', '', '', ''),
            array(173, 'Poland', 'PL', '+48', 'Polish z?oty', 'zł', 'PLN'),
            array(174, 'Portugal', 'PT', '+351', 'Euro', '€', 'EUR'),
            array(175, 'Puerto Rico', 'PR', '+1939', '', '', ''),
            array(176, 'Qatar', 'QA', '+974', 'Qatari riyal', 'ر.ق', 'QAR'),
            array(177, 'Romania', 'RO', '+40', 'Romanian leu', 'lei', 'RON'),
            array(178, 'Russia', 'RU', '+7', 'Russian ruble', '', 'RUB'),
            array(179, 'Rwanda', 'RW', '+250', 'Rwandan franc', 'Fr', 'RWF'),
            array(180, 'Reunion', 'RE', '+262', '', '', ''),
            array(181, 'Saint Barthelemy', 'BL', '+590', '', '', ''),
            array(182, 'Saint Helena, Ascens', 'SH', '+290', '', '', ''),
            array(183, 'Saint Kitts and Nevi', 'KN', '+1869', '', '', ''),
            array(184, 'Saint Lucia', 'LC', '+1758', 'East Caribbean dolla', '$', 'XCD'),
            array(185, 'Saint Martin', 'MF', '+590', '', '', ''),
            array(186, 'Saint Pierre and Miq', 'PM', '+508', '', '', ''),
            array(187, 'Saint Vincent and th', 'VC', '+1784', '', '', ''),
            array(188, 'Samoa', 'WS', '+685', 'Samoan t?l?', 'T', 'WST'),
            array(189, 'San Marino', 'SM', '+378', 'Euro', '€', 'EUR'),
            array(190, 'Sao Tome and Princip', 'ST', '+239', '', '', ''),
            array(191, 'Saudi Arabia', 'SA', '+966', 'Saudi riyal', 'ر.س', 'SAR'),
            array(192, 'Senegal', 'SN', '+221', 'West African CFA fra', 'Fr', 'XOF'),
            array(193, 'Serbia', 'RS', '+381', 'Serbian dinar', 'дин. or din.', 'RSD'),
            array(194, 'Seychelles', 'SC', '+248', 'Seychellois rupee', '₨', 'SCR'),
            array(195, 'Sierra Leone', 'SL', '+232', 'Sierra Leonean leone', 'Le', 'SLL'),
            array(196, 'Singapore', 'SG', '+65', 'Brunei dollar', '$', 'BND'),
            array(197, 'Slovakia', 'SK', '+421', 'Euro', '€', 'EUR'),
            array(198, 'Slovenia', 'SI', '+386', 'Euro', '€', 'EUR'),
            array(199, 'Solomon Islands', 'SB', '+677', 'Solomon Islands doll', '$', 'SBD'),
            array(200, 'Somalia', 'SO', '+252', 'Somali shilling', 'Sh', 'SOS'),
            array(201, 'South Africa', 'ZA', '+27', 'South African rand', 'R', 'ZAR'),
            array(202, 'South Georgia and th', 'GS', '+500', '', '', ''),
            array(203, 'Spain', 'ES', '+34', 'Euro', '€', 'EUR'),
            array(204, 'Sri Lanka', 'LK', '+94', 'Sri Lankan rupee', 'Rs or රු', 'LKR'),
            array(205, 'Sudan', 'SD', '+249', 'Sudanese pound', 'ج.س.', 'SDG'),
            array(206, 'Suriname', 'SR', '+597', 'Surinamese dollar', '$', 'SRD'),
            array(207, 'Svalbard and Jan May', 'SJ', '+47', '', '', ''),
            array(208, 'Swaziland', 'SZ', '+268', 'Swazi lilangeni', 'L', 'SZL'),
            array(209, 'Sweden', 'SE', '+46', 'Swedish krona', 'kr', 'SEK'),
            array(210, 'Switzerland', 'CH', '+41', 'Swiss franc', 'Fr', 'CHF'),
            array(211, 'Syrian Arab Republic', 'SY', '+963', '', '', ''),
            array(212, 'Taiwan', 'TW', '+886', 'New Taiwan dollar', '$', 'TWD'),
            array(213, 'Tajikistan', 'TJ', '+992', 'Tajikistani somoni', 'ЅМ', 'TJS'),
            array(214, 'Tanzania, United Rep', 'TZ', '+255', '', '', ''),
            array(215, 'Thailand', 'TH', '+66', 'Thai baht', '฿', 'THB'),
            array(216, 'Timor-Leste', 'TL', '+670', '', '', ''),
            array(217, 'Togo', 'TG', '+228', 'West African CFA fra', 'Fr', 'XOF'),
            array(218, 'Tokelau', 'TK', '+690', '', '', ''),
            array(219, 'Tonga', 'TO', '+676', 'Tongan pa?anga', 'T$', 'TOP'),
            array(220, 'Trinidad and Tobago', 'TT', '+1868', 'Trinidad and Tobago ', '$', 'TTD'),
            array(221, 'Tunisia', 'TN', '+216', 'Tunisian dinar', 'د.ت', 'TND'),
            array(222, 'Turkey', 'TR', '+90', 'Turkish lira', '', 'TRY'),
            array(223, 'Turkmenistan', 'TM', '+993', 'Turkmenistan manat', 'm', 'TMT'),
            array(224, 'Turks and Caicos Isl', 'TC', '+1649', '', '', ''),
            array(225, 'Tuvalu', 'TV', '+688', 'Australian dollar', '$', 'AUD'),
            array(226, 'Uganda', 'UG', '+256', 'Ugandan shilling', 'Sh', 'UGX'),
            array(227, 'Ukraine', 'UA', '+380', 'Ukrainian hryvnia', '₴', 'UAH'),
            array(228, 'United Arab Emirates', 'AE', '+971', 'United Arab Emirates', 'د.إ', 'AED'),
            array(229, 'United Kingdom', 'GB', '+44', 'British pound', '£', 'GBP'),
            array(230, 'United States', 'US', '+1', 'United States dollar', '$', 'USD'),
            array(231, 'Uruguay', 'UY', '+598', 'Uruguayan peso', '$', 'UYU'),
            array(232, 'Uzbekistan', 'UZ', '+998', 'Uzbekistani som', '', 'UZS'),
            array(233, 'Vanuatu', 'VU', '+678', 'Vanuatu vatu', 'Vt', 'VUV'),
            array(234, 'Venezuela, Bolivaria', 'VE', '+58', '', '', ''),
            array(235, 'Vietnam', 'VN', '+84', 'Vietnamese ??ng', '₫', 'VND'),
            array(236, 'Virgin Islands, Brit', 'VG', '+1284', '', '', ''),
            array(237, 'Virgin Islands, U.S.', 'VI', '+1340', '', '', ''),
            array(238, 'Wallis and Futuna', 'WF', '+681', 'CFP franc', 'Fr', 'XPF'),
            array(239, 'Yemen', 'YE', '+967', 'Yemeni rial', '﷼', 'YER'),
            array(240, 'Zambia', 'ZM', '+260', 'Zambian kwacha', 'ZK', 'ZMW'),
            array(241, 'Zimbabwe', 'ZW', '+263', 'Botswana pula', 'P', 'BWP')
        ];

        foreach ($countries as $country) {
            $countryData = [
                'id' => $country[0],
                'name' => $country[1],
                'code' => $country[2],
                'dial_code' => $country[3],

            ];

            if ($country[2] == 'US') {
                $countryData['available'] = 1;
            }


            Country::create($countryData);


        }
    }

    // Users Cities
    private function seedCities()
    {
        City::create([
            'name' => 'London',
            'code' => 'LON',
            'available' => true,
            'country_id' => '229'
        ]);
        City::create([
            'name' => 'Dingle',
            'code' => 'DNG',
            'available' => true,
            'country_id' => '102'
        ]);
        City::create([
            'name' => 'Limerick',
            'code' => 'LMR',
            'available' => true,
            'country_id' => '102'
        ]);
        City::create([
            'name' => 'Dublin',
            'code' => 'DBL',
            'available' => true,
            'country_id' => '102'
        ]);

    }


    // Users Seed
    private function seedUsers()
    {
        $user = User::create([
            'id' => 1,
            'email' => 'admin@ai-tech.ltd',
            'password' => bcrypt('password'),
            'status' => 1,
            'owner_id' => 1

        ]);


        $user = User::create([
            'id' => 2,
            'email' => 'mihai.penica@gmail.com',
            'password' => bcrypt('password'),
            'status' => 1,
            'owner_id' => 1

        ]);


        $caGroup = Group::create([
            'id' => 1,
            'name' => 'System Administrators',
            'code' => 'ADM',
            'system' => 1,
            'owner_id' => 1
        ]);

        $caGroup = Group::create([
            'name' => 'Company Directors',
            'code' => 'STF',
            'owner_id' => 1
        ]);


        $caGroup = Group::create([
            'name' => 'Accountants',
            'code' => 'ACT',
            'owner_id' => 1
        ]);


    }


    function seedUserDetail()
    {

        UserDetail::create([

            'user_id' => 1,
            'name' => 'Administrator User',
            'country_id' => 201, //romania
            'organization' => 'AIEDGETECH',
            'organization_id' => 1,
            'avatarName' => '',
            'gdpr_accepted' => 1,
            'owner_id' => 1
        ]);
        UserDetail::create([
            'user_id' => 2,
            'name' => ' User',
            'organization_id' => 2,
            'country_id' => 201, //romania
            'avatarName' => '',
            'gdpr_accepted' => 1,
            'owner_id' => 1
        ]);

    }


    private function seedFiles()
    {

        $this->seedFileTypes();
    }

    private function seedFileTypes()
    {
        $folders=[
            [
            'id' => 1,
            'name' => 'ROOT',
            'type' => 'folder',
            'user_id' => 1,
            'organization_id' => 1
        ],
            [
                'name' => 'invoices',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'expenses',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'company_docs',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'clients_docs',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'providers_docs',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'payroll',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'bank_statements',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ],
            [
                'name' => 'projects_docs',
                'type' => 'folder',
                'user_id' => 1,
                'organization_id' => 1,
                'parent_id' => 1,
                'icon' => 'folder',
                'level' => 'private'
            ]
        ];

        foreach ($folders as $folder) {
            $file_id = File::create($folder);
//            UserFile::create([
//                'file_id' => $file_id['id'],
//                'user_id' => 1,
//                'owner_id' => 1,
//                'level' => 'private'
//            ]);
        }
    }


    //Rights Seed
    private function seedRights()
    {
        $rights = [

            ['name' => 'Manage User Detail',
                'class_namespace' => ['UserDetail', 'Rights'],
                'C' => 1,
                'R' => 1,
                'U' => 1,
                'D' => 1,
                'owner_id' => 1
            ],
            ['name' => 'Manage Users',
                'class_namespace' => ['User'],
                'C' => 1,
                'R' => 1,
                'U' => 1,
                'D' => 1,
                'owner_id' => 1
            ]


        ];
        foreach ($rights as $right) {
            Right::create($right);
        }
    }


    //Navigation Seed
    private function seedNav()
    {
        $navs = [

            [
                'id' => 1,
                'title' => 'Root',
                'type' => 'system',
                'icon' => null,
                'url' => null,
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 1

            ],
            [
                'id' => 2,
                'title' => 'Team',
                'type' => 'group',
                'icon' => null,
                'url' => null,
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 1

            ],
            [
                'id' => 3,
                'title' => 'Company',
                'type' => 'group',
                'icon' => 'settings',
                'url' => null,
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 2
            ],

            [
                'id' => 4,
                'title' => 'Accountancy',
                'type' => 'group',
                'icon' => 'settings',
                'url' => null,
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 2
            ],
            [
                'id' => 5,
                'title' => 'Settings',
                'type' => 'group',
                'icon' => 'settings',
                'url' => null,
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 2
            ],
            [
                'id' => 6,
                'title' => 'Management',
                'type' => 'group',
                'icon' => 'group',
                'url' => '',
                'translate' => null,
                'parent_id' => null,
                'owner_id' => 1,
                'position' => 3
            ],
            [
                'title' => 'Dashboard',
                'type' => 'item',
                'icon' => 'settings',
                'url' => '/private/dashboard',
                'translate' => null,
                'parent_id' => 2,
                'owner_id' => 1,
                'position' => 1
            ],
            [
                'title' => 'Notifications',
                'type' => 'item',
                'icon' => 'notification_important',
                'url' => '/private/notifications',
                'translate' => null,
                'parent_id' => 2,
                'owner_id' => 1,
                'position' => 2
            ],
            [
                'title' => 'Projects',
                'type' => 'item',
                'icon' => 'folder',
                'url' => '/private/projects',
                'translate' => null,
                'parent_id' => 2,
                'owner_id' => 1,
                'position' => 3
            ],
            [
                'title' => 'Tasks',
                'type' => 'item',
                'icon' => 'menu',
                'url' => '/private/tasks',
                'translate' => null,
                'parent_id' => 2,
                'owner_id' => 1,
                'position' => 4
            ],

            [
                'title' => 'Company Files',
                'type' => 'item',
                'icon' => 'folder',
                'url' => '/private/files',
                'translate' => null,
                'parent_id' => 3,
                'owner_id' => 1,
                'position' => 1
            ],
            [
                'title' => 'Clients',
                'type' => 'item',
                'icon' => 'folder',
                'url' => '/private/clients',
                'translate' => null,
                'parent_id' => 3,
                'owner_id' => 1,
                'position' => 1
            ],
            [
                'title' => 'Providers',
                'type' => 'item',
                'icon' => 'folder',
                'url' => '/private/providers',
                'translate' => null,
                'parent_id' => 3,
                'owner_id' => 1,
                'position' => 1
            ],

            [
                'title' => 'Invoices',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/invoices',
                'translate' => null,
                'parent_id' => 4,
                'owner_id' => 1,
                'position' => 1
            ],

            [
                'title' => 'Expenses',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/expenses',
                'translate' => null,
                'parent_id' => 4,
                'owner_id' => 1,
                'position' => 2
            ],

            [
                'title' => 'Payroll',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/payroll',
                'translate' => null,
                'parent_id' => 4,
                'owner_id' => 1,
                'position' => 3
            ],

            [
                'title' => 'Bank',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/financial',
                'translate' => null,
                'parent_id' => 4,
                'owner_id' => 1,
                'position' => 4
            ],

            [
                'title' => 'Permissions',
                'type' => 'item',
                'icon' => 'lock',
                'url' => '/private/settings/permissions',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 4
            ], [
                'title' => 'Groups',
                'type' => 'item',
                'icon' => 'menu',
                'url' => '/private/settings/groups',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 3
            ],
            [
                'title' => 'Organizations',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/settings/organizations',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 1
            ],
            [
                'title' => 'Users',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/settings/users',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 2
            ], [
                'title' => 'Navigation',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/settings/navigations',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 8
            ],
            [
                'title' => 'Settings',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/settings/settings',
                'translate' => null,
                'parent_id' => 5,
                'owner_id' => 1,
                'position' => 10
            ],
            [
                'title' => 'Reporting',
                'type' => 'item',
                'icon' => 'group',
                'url' => '/private/reporting',
                'translate' => null,
                'parent_id' => 6,
                'owner_id' => 1,
                'position' => 5
            ]


        ];
        foreach ($navs as $nav) {
            Navigation::create($nav);
        }
    }


    //GroupsRight Seed
    private function seedGroupRights()
    {
        $groupRights = [

            [
                'id' => 1,
                'group_id' => 1,
                'right_id' => 1,
                'owner_id' => 1
            ]

        ];
        foreach ($groupRights as $groupRight) {
            GroupRights::create($groupRight);
        }
    }


    //Groups Seed
    private function seedUserGroups()
    {
        $userGroups = [

            [
                'id' => 1,
                'group_id' => 1,
                'user_id' => 1,
                'owner_id' => 1

            ]

        ];
        foreach ($userGroups as $userGroup) {
            UserGroup::create($userGroup);
        }
    }


    //Groups Seed
    private function seedOrganizations()
    {
        $organizations = [

            [
                'id' => 1,
                'organisation_name' => 'APPLIED AI EDGETECH LIMITED',
                'address' => '98 Bloomfield ',
                'city' => 'Annacotty',
                'county' => 'Limerick',
                'country' => 'Ireland',
                'postal_code' => 'V94 T9PX',

                'phone' => '+353-61-202700',
                'vat_no' => '3690359GH',

                'director_name' => 'Mihai Penica',
                'secretary_name' => 'Olivia Penica',

                'iban' => 'RO07BOFI90430923178118',
                'date_inclusion' => '08.06.2019',
                'owner_id' => 1,

            ]
        ];
        foreach ($organizations as $organization) {
            Organization::create($organization);
        }
    }

    //Groups Seed
    private function seedNomenExpenseType()
    {
        $expanseType = [

            [

                'name' => 'Advertising',
                'description' => 'Advertising',
                'owner_id' => 1,

            ],
            [
                'name' => 'Bank Charges',
                'description' => 'Bank Charges',
                'owner_id' => 1,

            ], [

                'name' => 'Business meetings and networking',
                'description' => 'Business meetings and networking',
                'owner_id' => 1,

            ], [

                'name' => 'Company Pension',
                'description' => 'Company Pension',
                'owner_id' => 1,

            ], [

                'name' => 'Computer Equipment',
                'description' => 'Computer Equipment',
                'owner_id' => 1,

            ], [

                'name' => 'Course/Seminar/Conference',
                'description' => 'Course/Seminar/Conference',
                'owner_id' => 1,

            ], [

                'name' => 'CRO',
                'description' => 'CRO',
                'owner_id' => 1,

            ], [

                'name' => 'Cycle to work scheme Electric',
                'description' => 'Cycle to work scheme Electric',
                'owner_id' => 1,

            ], [

                'name' => 'Cycle to work scheme Non Electric',
                'description' => 'Cycle to work scheme Non Electric',
                'owner_id' => 1,

            ], [

                'name' => 'Directors Current Account',
                'description' => 'Directors Current Account',
                'owner_id' => 1,

            ], [

                'name' => 'Home Office',
                'description' => 'Home Office',
                'owner_id' => 1,

            ], [

                'name' => 'Internet',
                'description' => 'Internet',
                'owner_id' => 1,

            ], [

                'name' => 'Landline',
                'description' => 'Landline',
                'owner_id' => 1,

            ], [

                'name' => 'Medical Checkup',
                'description' => 'Medical Checkup',
                'owner_id' => 1,

            ], [

                'name' => 'Mobile Phone',
                'description' => 'Mobile Phone',
                'owner_id' => 1,

            ], [

                'name' => 'Mobile Phone Handset',
                'description' => 'Mobile Phone Handset',
                'owner_id' => 1,

            ], [

                'name' => 'Office Rent',
                'description' => 'Office Rent',
                'owner_id' => 1,

            ], [

                'name' => 'Other Business Expenses',
                'description' => 'Other Business Expenses',
                'owner_id' => 1,

            ], [

                'name' => 'Postage Stamps,Printing,Stationary',
                'description' => 'Postage Stamps,Printing,Stationary',
                'owner_id' => 1,

            ], [

                'name' => 'Professional Consultancy Fees',
                'description' => 'Professional Consultancy Fees',
                'owner_id' => 1,

            ], [

                'name' => 'Professional Subscription',
                'description' => 'Professional Subscription',
                'owner_id' => 1,

            ], [

                'name' => 'Purchases',
                'description' => 'Purchases',
                'owner_id' => 1,

            ], [

                'name' => 'Safety Equipment',
                'description' => 'Safety Equipment',
                'owner_id' => 1,

            ], [

                'name' => 'Small Gift Exemption Voucher',
                'description' => 'Small Gift Exemption Voucher',
                'owner_id' => 1,

            ], [

                'name' => 'Technical Books',
                'description' => 'Technical Books',
                'owner_id' => 1,

            ], [

                'name' => 'Tools &amp; Equipment',
                'description' => 'Tools &amp; Equipment',
                'owner_id' => 1,

            ], [

                'name' => 'Uniform/work clothes',
                'description' => 'Uniform/work clothes',
                'owner_id' => 1,

            ], [

                'name' => 'Work Books',
                'description' => 'Work Books',
                'owner_id' => 1,

            ], [

                'name' => 'Other services',
                'description' => 'Other services',
                'owner_id' => 1,

            ],
        ];
        foreach ($expanseType as $expense) {
           ExpensesType::create($expense);
        }
    }




}
