<?php

return [
	'modules' => [
        'Users',
        'Files',
        'Groups',
		'Internationalization',
        'Notifications',
        'Organizations',
        'Persons',
        'System',
        'Navigation',
        'Unsecured',
        'Nomenclatoare',
        'Accountancy',


	]
];
